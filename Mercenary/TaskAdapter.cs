﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Mercenary
{
    // Token: 0x0200000D RID: 13
    public static class TaskAdapter
    {
        // Token: 0x06000053 RID: 83 RVA: 0x00005234 File Offset: 0x00003434
        public static void SetTask(int taskId, int mercenaryId, string title, string desc, List<Task> tasks, string progressMessage)
        {
            // 对于优先使用多个英雄配合的，优先级设为0 /1，保证一起出场
            // 对于需要6个英雄配合的，优先级为0, 3个以下的，优先级为1
            // 未满30级的佣兵 优先级设为10，放后备箱里跟着升级
            // 部分要求地图的基本优先级都是5（正常）挨个排着
            // 英雄悬赏优先度为2，为了能早日排进H1-1的地图
            Out.Log(string.Format("[TID:{0}][MID:{1}] {2} {3} {4}",
                taskId, mercenaryId, title, desc, progressMessage));
            
            //**********施法者任务***********//
            //卡德加 任务10 
            if (title.Contains("然后降温"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {

                    TaskAdapter.GetMercenary(mercenaryId,"暴风雪",1)
                }));
                return;
            }
            //卡德加 任务3 
            if (title.Contains("燃烧之魂"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {

                    TaskAdapter.GetMercenary(mercenaryId,null,1),
                    TaskAdapter.GetMercenary(MercConst.安东尼达斯,null,0)
                }));
                return;
            }
            //卡德加 任务14 9
            if (title.Contains("臻于完美") || title.Contains("搅热事态"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {

                    TaskAdapter.GetMercenary(mercenaryId,"炉火",2)
                }));
                return;
            }
            //任务12 
            if (title.Contains("嗞，啪，嘭！"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-5", new MercenaryEntity[]
                {

                    TaskAdapter.GetMercenary(mercenaryId,"魔爆术",0),
                    TaskAdapter.GetMercenary(MercConst.厨师曲奇,"小鱼快冲",2),
                }));
                return;
            }
            //豪斯任务17 嗞，啪，嘭！
            if (title.Contains("大箭就是好箭"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-5", new MercenaryEntity[]
                {

                    TaskAdapter.GetMercenary(mercenaryId,"奥术箭",0),
                    TaskAdapter.GetMercenary(MercConst.伊莉斯_逐星,"黄金猿",EquipConst.猴爪),
                    TaskAdapter.GetMercenary(MercConst.玉珑, "玉火打击", 0),
                }));
                return;
            }
            //德雷克 任务14
            if (title.Contains("邪能的力量"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage,  taskId, 0, new MercenaryEntity[]
                {

                    TaskAdapter.GetMercenary(mercenaryId,"邪能腐蚀",0),
                    TaskAdapter.GetMercenary(MercConst.加拉克苏斯大王,"邪能地狱火",EquipConst.邪能核心)
                }));
                return;
            }
            //古夫 任务14
            if (title.Contains("带刺的自然"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {

                    TaskAdapter.GetMercenary(mercenaryId,"活体荆棘",EquipConst.木棘图腾),
                    TaskAdapter.GetMercenary(MercConst.布鲁坎,"陷足泥泞",EquipConst.霜木图腾),

                }));
                return;
            }
            //塔姆辛 任务10
            if (title.Contains("证明我的价值"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-6", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.伊莉斯_逐星,"黄金猿",EquipConst.猴爪),
                    TaskAdapter.GetMercenary(MercConst.玉珑, "玉火打击", 0),
                    TaskAdapter.GetMercenary(mercenaryId,"暗影之幕",EquipConst.暗影符文)
                }));
                return;
            }
            //沃金 任务9 任务14
            if (title.Contains("暗影秘法") || title.Contains("沃金的意志"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-6", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.魔像师卡扎库斯,"构筑魔像"),
                    TaskAdapter.GetMercenary(mercenaryId,"暗影涌动",EquipConst.沃金战刃),

                }));
                return;
            }
            //神谕者摩戈尔 任务12 
            if (title.Contains("鲜血淋漓"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-6", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,"鱼人弹幕", EquipConst.尖刺泡泡鱼),
                    TaskAdapter.GetMercenary(MercConst.老瞎眼,"邪鳍导航员", EquipConst.导航员的护符),
                    TaskAdapter.GetMercenary(MercConst.厨师曲奇,"小鱼快冲", EquipConst.开胃前菜)

                }));
                return;
            }
            //乌瑟尔 任务12
            if (title.Contains("光明使者"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-5", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "复仇之怒", EquipConst.微光肩甲),
                    TaskAdapter.GetMercenary(MercConst.考内留斯_罗姆, "牺牲祝福", EquipConst.黎明之盾),
                    TaskAdapter.GetMercenary(MercConst.先知维伦, "维伦的祝福", 0),
                }));
                return;
            }
            //吉安娜 任务12
            if (title.Contains("千钧一发"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-6", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.伊莉斯_逐星,"黄金猿",EquipConst.猴爪),
                    TaskAdapter.GetMercenary(MercConst.玉珑, "玉火打击", 0),
                    TaskAdapter.GetMercenary(mercenaryId,"浮冰术",EquipConst.寒冰屏障护身符)

                }));
                return;
            }
            //安东尼 任务
            if (title.Contains("火焰滚滚"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-5", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.玉珑,"玉火打击",0),
                    TaskAdapter.GetMercenary(MercConst.赤精,"振奋之歌",EquipConst.赤精之杖),
                    TaskAdapter.GetMercenary(mercenaryId,"火球风暴",EquipConst.灼热吊坠)

                }));
                return;
            }
            //安东尼 任务
            if (title.Contains("保护肯瑞托"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-5", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.拉格纳罗斯,"熔岩冲击",EquipConst.炽烧符文),
                    TaskAdapter.GetMercenary(mercenaryId,"烈焰风暴",EquipConst.烈焰饰环)

                }));
                return;
            }
            //闪狐 任务3
            if (title.Contains("现在归我了"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 5, "2-5", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,"群体缠绕",EquipConst.第十条尾巴)

                }));
                return;
            }
            //闪狐 任务
            if (title.Contains("唰！"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-5", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.玉珑, "玉火打击",0),
                    TaskAdapter.GetMercenary(mercenaryId,"法力闪现",EquipConst.法力符文),
                    TaskAdapter.GetMercenary(MercConst.贝恩_血蹄,"大地母亲之怒",EquipConst.酋长的羽毛)

                }));
                return;
            }
            //闪狐 任务9 任务14 泰兰德 任务9
            if (desc.Contains("加快友方技能的速度值总") || desc.Contains("使一个友方技能的速度值加快"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 5, "2-5", TaskAdapter.GetQuickMercenary(mercenaryId)));
                return;
            }
            //空军上将 任务2
            if (title.Contains("列队飞行"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "战术大师", 0, -1, HsMercenaryStrategy.TARGETTYPE.FRIENDLY)
                }));
                return;
            }
            //珑心 任务3 任务10
            if (title.Contains("远古洞窟") || title.Contains("修习飞举"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "群星簇拥", EquipConst.昆莱之晶, -1, HsMercenaryStrategy.TARGETTYPE.FRIENDLY)
                }));
                return;
            }
            //奥妮克希亚 任务2
            if (title.Contains("吸气"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 5, "H2-6", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "深呼吸", EquipConst.更深的呼吸)
                }));
                return;
            }
            //奥妮克希亚 任务10 任务16
            if (title.Contains("喷吐火焰") || title.Contains("团本首领下场"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.伊莉斯_逐星,"黄金猿",EquipConst.猴爪),
                    TaskAdapter.GetMercenary(MercConst.玉珑, "玉火打击", 0),
                    TaskAdapter.GetMercenary(mercenaryId, "清理巢穴", 0)
                }));
                return;
            }
            //德雷克塔尔 任务17
            if (title.Contains("淬霜之刃"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "H1-2", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,null, EquipConst.冰川之刃),
                    TaskAdapter.GetMercenary(MercConst.卡德加,"炉火",EquipConst.焙烤培根),
                    TaskAdapter.GetMercenary(MercConst.魔像师卡扎库斯,"暗影之爪",EquipConst.野葡萄藤)
                }));
                return;
            }
            //瓦尔登·晨拥 任务9 
            if (title.Contains("学术道德"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-6", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "冰枪术", EquipConst.寒冰药水),
                    TaskAdapter.GetMercenary(MercConst.布莱恩_铜须, "铜须风范", 0)
                    
                }));
                return;
            }
            //瓦尔登·晨拥 任务10
            if (title.Contains("调节温度"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-6", new MercenaryEntity[]
                {

                    TaskAdapter.GetMercenary(MercConst.玉珑,"玉火打击" ,0),
                    TaskAdapter.GetMercenary(MercConst.赤精,"振奋之歌", EquipConst.赤精之杖),
                    TaskAdapter.GetMercenary(mercenaryId, "冰枪术", EquipConst.霜冻之戒)
                }));
                return;
            }
            //瓦尔登·晨拥 任务12
            if (title.Contains("附加影响"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "H1-2", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "冰风暴", EquipConst.寒冰药水),
                    TaskAdapter.GetMercenary(MercConst.冰雪之王洛克霍拉, null, EquipConst.刺骨寒风)
                }));
                return;
            }
            //先知维伦 任务12
            if (title.Contains("圣光扫荡一切"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-6", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "裂解之光", EquipConst.受祝福的碎片),
                    TaskAdapter.GetMercenary(MercConst.安度因_乌瑞恩, "神圣新星", EquipConst.祥和钟杵)
                }));
                return;
            }
            //先知维伦 任务17
            if (title.Contains("完美圣光"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "裂解之光", EquipConst.受祝福的碎片),
                    TaskAdapter.GetMercenary(MercConst.泽瑞拉, null, 0)
                }));
                return;
            }
            //布鲁坎 任务17
            if (title.Contains("一点点电"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.伊莉斯_逐星,"黄金猿",EquipConst.猴爪),
                    TaskAdapter.GetMercenary(MercConst.玉珑, "玉火打击", 0),
                    TaskAdapter.GetMercenary(mercenaryId, "陷足泥泞", EquipConst.霜木图腾)

                }));
                return;
            }
            //娜塔莉·赛琳 任务4
            if (title.Contains("圣光治愈一切"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 5, "2-5", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "咒逐", 0),
                    TaskAdapter.GetMercenary(mercenaryId, "祈福", 0)
                }));
                return;
            }
            //迦顿 任务12
            if (title.Contains("烧烤弱鸡"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-6", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.厨师曲奇, "小鱼快冲", 0),
                    TaskAdapter.GetMercenary(mercenaryId, "活体炸弹",EquipConst.焚火印记),
                    TaskAdapter.GetMercenary(MercConst.安度因_乌瑞恩,"神圣新星",EquipConst.祥和钟杵),

                }));
                return;
            }
            //迦顿 任务10 任务14
            if (title.Contains("侵略如火") || title.Contains("翻腾火流"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-5", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "热力迸发", 0),
                    TaskAdapter.GetMercenary(MercConst.安东尼达斯, "火球术", 0),
                    TaskAdapter.GetMercenary(MercConst.拉格纳罗斯, "熔岩冲击", 0)
                }));
                return;
            }

            //**********斗士任务***********//
            //伊瑞尔
            if (title.Contains("惩戒不信之徒") || title.Contains("净化邪恶"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-5", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,"光缚之怒",0),
                    TaskAdapter.GetMercenary(MercConst.玉珑,"玉火打击",0),
                    TaskAdapter.GetMercenary(MercConst.赤精,"振奋之歌",EquipConst.赤精之杖)
                }));
                return;
            }
            //拉希奥
            if (title.Contains("展开双翼") || title.Contains("黑王子之站"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-5", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,"真正形态",EquipConst.黑龙鳞片),
                    TaskAdapter.GetMercenary(MercConst.玉珑,"玉火打击",0),
                    TaskAdapter.GetMercenary(MercConst.赤精,"振奋之歌",EquipConst.赤精之杖)
                }));
                return;
            }
            //滑矛领主
            if (title.Contains("丢进垃圾箱") || title.Contains("那就是垃圾"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "H1-1", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,"处理垃圾",EquipConst.锐利思维),
                    TaskAdapter.GetMercenary(MercConst.玉珑,"玉火打击",0),
                    TaskAdapter.GetMercenary(MercConst.厨师曲奇,"小鱼快冲",0)
                }));
                return;
            }
            //尤朵拉 任务9 任务16
            if (title.Contains("炮灰") || title.Contains("火炮齐射"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 5, "H1-1", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,"准备火炮",EquipConst.装填武器)

                }));
                return;
            }
            //尤朵拉 任务3 任务14
            if (title.Contains("远洋助手") || title.Contains("飓风营救"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 5, "2-5", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.玉珑,"玉火打击",0),
                    TaskAdapter.GetMercenary(mercenaryId,"掩护射击",EquipConst.装填武器,-1,HsMercenaryStrategy.TARGETTYPE.FRIENDLY)

                }));
                return;
            }
            //雷克萨
            if (title.Contains("唯一的朋友"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,"动物伙伴",EquipConst.熊妈妈之爪,0,HsMercenaryStrategy.TARGETTYPE.FRIENDLY),
                    TaskAdapter.GetMercenary(MercConst.穆克拉,null,EquipConst.穆克拉的大表哥)

                }));
                return;
            }
            //弗丁 任务2 任务14
            if (title.Contains("王者祝福") || title.Contains("银色北伐军"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "王者祝福", EquipConst.王者之盔, -1, HsMercenaryStrategy.TARGETTYPE.FRIENDLY)
                }));
                return;
            }
            //斯卡布斯·刀油 任务1 任务9
            if (title.Contains("秘密探员") || title.Contains("二加二等于五"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "战术打击", EquipConst.军情七处合约),
                    TaskAdapter.GetMercenary(MercConst.玉珑, "玉火打击", 0)

                }));
                return;
            }
            //泰瑞尔 任务2
            if (title.Contains("巨大的谎言"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "1-10", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.巴琳达_斯通赫尔斯,null,EquipConst.次级水元素),
                    TaskAdapter.GetMercenary(MercConst.迦顿男爵,"地狱火",EquipConst.焚火印记),
                    TaskAdapter.GetMercenary(MercConst.拉格纳罗斯,"死吧，虫子",EquipConst.炽烧符文),
                    TaskAdapter.GetMercenary(MercConst.冰雪之王洛克霍拉,null,EquipConst.刺骨寒风),
                    TaskAdapter.GetMercenary(MercConst.吉安娜_普罗德摩尔,null,EquipConst.寒冰屏障护身符),
                    TaskAdapter.GetMercenary(mercenaryId,"圣剑挺击",EquipConst.圣羽之辉)

                }));
                return;
            }
            //泰瑞尔 任务10
            if (title.Contains("黑暗流亡者"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "H1-9", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.巴琳达_斯通赫尔斯,null,EquipConst.次级水元素),
                    TaskAdapter.GetMercenary(MercConst.迦顿男爵,"地狱火",EquipConst.焚火印记),
                    TaskAdapter.GetMercenary(MercConst.拉格纳罗斯,"死吧，虫子",EquipConst.炽烧符文),
                    TaskAdapter.GetMercenary(MercConst.冰雪之王洛克霍拉,null,EquipConst.凝聚冰凌),
                    TaskAdapter.GetMercenary(MercConst.吉安娜_普罗德摩尔,null,EquipConst.寒冰屏障护身符),
                    TaskAdapter.GetMercenary(mercenaryId,"圣剑挺击",EquipConst.圣羽之辉)

                }));
                return;
            }
            //泰瑞尔 任务12
            if (title.Contains("恶魔再临"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "H2-6", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.巴琳达_斯通赫尔斯,null,EquipConst.次级水元素),
                    TaskAdapter.GetMercenary(MercConst.迦顿男爵,"地狱火",EquipConst.焚火印记),
                    TaskAdapter.GetMercenary(MercConst.拉格纳罗斯,"死吧，虫子",EquipConst.炽烧符文),
                    TaskAdapter.GetMercenary(MercConst.冰雪之王洛克霍拉,null,EquipConst.凝聚冰凌),
                    TaskAdapter.GetMercenary(MercConst.吉安娜_普罗德摩尔,null,EquipConst.寒冰屏障护身符),
                    TaskAdapter.GetMercenary(mercenaryId,"圣剑挺击",EquipConst.圣羽之辉)

                }));
                return;
            }
            //泰瑞尔 任务14
            if (title.Contains("夺魂之镰"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "H5-2", new MercenaryEntity[]
                {

                    TaskAdapter.GetMercenary(MercConst.巴琳达_斯通赫尔斯,null,EquipConst.次级水元素),
                    TaskAdapter.GetMercenary(MercConst.迦顿男爵,"地狱火",EquipConst.焚火印记),
                    TaskAdapter.GetMercenary(MercConst.拉格纳罗斯,"死吧，虫子",EquipConst.炽烧符文),
                    TaskAdapter.GetMercenary(MercConst.冰雪之王洛克霍拉,null,EquipConst.凝聚冰凌),
                    TaskAdapter.GetMercenary(MercConst.吉安娜_普罗德摩尔,null,EquipConst.寒冰屏障护身符),
                    TaskAdapter.GetMercenary(mercenaryId,"圣剑挺击",EquipConst.圣羽之辉)

                }));
                return;
            }
            //风行者 任务 2 任务17
            if (title.Contains("重拾灵魂") || title.Contains("女妖之怒"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-5", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "重拾灵魂", EquipConst.灵魂缶),
                    TaskAdapter.GetMercenary(MercConst.魔像师卡扎库斯, "构筑魔像", EquipConst.野葡萄藤),
                    TaskAdapter.GetMercenary(MercConst.海盗帕奇斯, "眼魔船工", EquipConst.武器柜)
                }));
                return;
            }
            //风行者 任务 12
            if (title.Contains("无论死活"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "H2-5", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "重拾灵魂", EquipConst.死亡箭雨),
                    TaskAdapter.GetMercenary(MercConst.魔像师卡扎库斯, "构筑魔像", EquipConst.野葡萄藤),
                    TaskAdapter.GetMercenary(MercConst.海盗帕奇斯, "眼魔船工", EquipConst.武器柜)
                }));
                return;
            }
            //风行者 任务3 任务9 
            if (title.Contains("瞄准叛徒") || title.Contains("如此狡诈"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 5, "2-5", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.玉珑, "玉火打击", 0),
                    TaskAdapter.GetMercenary(mercenaryId, "为了女王", EquipConst.哀伤之剑)

                }));
                return;
            }
            //老瞎眼 任务9
            if (title.Contains("鱼类学"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 5, "2-6", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,"老蓟皮",EquipConst.始生鱼人)
                }));
                return;
            }
            //老瞎眼 任务12
            if (title.Contains("鱼多势众"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,"鱼人入侵",0),
                    TaskAdapter.GetMercenary(MercConst.厨师曲奇,null,0),
                    TaskAdapter.GetMercenary(MercConst.神谕者摩戈尔,null,0)
                }));
                return;
            }
            //塔维什·雷矛 任务12
            if (title.Contains("当心脚下"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-6", new MercenaryEntity[]
                {

                    TaskAdapter.GetMercenary(mercenaryId,"爆炸陷阱",EquipConst.拉线操纵),
                    TaskAdapter.GetMercenary(MercConst.瓦罗克_萨鲁法尔,"旋风斩",EquipConst.狂战士之刃),
                    TaskAdapter.GetMercenary(MercConst.伊莉斯_逐星,"黄金猿",EquipConst.猴爪),
                }));
                return;
            }
            //萨鲁法尔 任务14
            if (title.Contains("全力一击"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-6", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,"动员打击",EquipConst.萨隆踏靴),
                    TaskAdapter.GetMercenary(MercConst.洛卡拉,"进攻集结",EquipConst.激励头盔)
                }));
                return;
            }
            //拉索利安 任务1 任务12
            if (title.Contains("势均力敌？") || title.Contains("无坚不摧"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "闪电军团", 0),
                    TaskAdapter.GetMercenary(MercConst.玛诺洛斯, null, 0)
                }));
                return;
            }
            //拉索利安 任务14
            if (title.Contains("战场主宰"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 5, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,"巨型大恶魔",EquipConst.恶魔灰烬),
                }));
                return;
            }
            //拉索利安 任务17
            if (title.Contains("巨型邪魔"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-5", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "巨型大恶魔", EquipConst.恶魔灰烬),
                    TaskAdapter.GetMercenary(MercConst.洛卡拉, "进攻集结", EquipConst.激励头盔)
                }));
                return;
            }
            //吃手手 任务3 
            if (title.Contains("鸡肉味") || title.Contains("饥不可耐") || title.Contains("每日一鱼人"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,"吞噬",EquipConst.土灵护甲),
                    TaskAdapter.GetMercenary(MercConst.老瞎眼, "老蓟皮", EquipConst.始生鱼人)
                }));
                return;
            }
            //指挥官沃恩 任务9
            if (title.Contains("燃棘必胜"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.玉珑, "玉火打击", 0),
                    TaskAdapter.GetMercenary(mercenaryId,"火焰吐息",0)
                }));
                return;
            }
            //指挥官沃恩 任务12 任务17
            if (title.Contains("他们想挨斧头了") || title.Contains("猛斧与盟友"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-6", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.伊莉斯_逐星,"黄金猿",EquipConst.猴爪),
                    TaskAdapter.GetMercenary(MercConst.玉珑, "玉火打击", 0),
                    TaskAdapter.GetMercenary(mercenaryId,"飞斧投掷",0)
                }));
                return;
            }
            //迪亚波罗
            if (title.Contains("死亡降临"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-6", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "火焰践踏", 0),

                    TaskAdapter.GetMercenary(MercConst.厨师曲奇, "小鱼快冲", EquipConst.开胃前菜)
                }));
                return;
            }
            //艾德温·迪菲亚首脑 任务11 任务17
            if (title.Contains("致命一击") && mercenaryId == MercConst.艾德温_迪菲亚首脑 || title.Contains("身先士卒"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "H1-2", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "首脑的悬赏", EquipConst.公平分配),
                    TaskAdapter.GetMercenary(MercConst.海盗帕奇斯, null, 0),
                    TaskAdapter.GetMercenary(MercConst.尤朵拉, null, 0)
                }));
                return;
            }
            //加尔范 任务9
            if (title.Contains("贯穿伤口"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,"致命打击",EquipConst.德雷克塔尔的恩惠),
                    TaskAdapter.GetMercenary(MercConst.泽瑞拉,"快速治疗",0),
                    TaskAdapter.GetMercenary(MercConst.安娜科德拉,"野兽回复",EquipConst.狂野的徽记)
                }));
                return;
            }
            //希奈斯特拉 任务2
            if (title.Contains("何必忧伤"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 5, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "法力壁垒", EquipConst.法力胸针, -1, HsMercenaryStrategy.TARGETTYPE.FRIENDLY)
                }));
                return;
            }
            //雪怒 任务12
            if (title.Contains("往日旧梦"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-5", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,"伏虎闪雷", EquipConst.雪怒之矛),
                    TaskAdapter.GetMercenary(MercConst.卡德加,"炉火",EquipConst.焙烤培根),
                    TaskAdapter.GetMercenary(MercConst.穆克拉,"原始之力",EquipConst.新鲜香蕉)
                }));
                return;
            }
            //雪怒 任务16
            if (title.Contains("战斗热血"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "H1-2", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,"白虎飞扑", EquipConst.天神胸甲),
                    TaskAdapter.GetMercenary(MercConst.卡德加,"炉火",EquipConst.焙烤培根),
                    TaskAdapter.GetMercenary(MercConst.穆克拉,"原始之力",EquipConst.新鲜香蕉)
                }));
                return;
            }
            //奈法利安 任务2 
            if (title.Contains("完美的龙"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "多彩能量", EquipConst.实验对象, -1, HsMercenaryStrategy.TARGETTYPE.FRIENDLY)
                }));
                return;
            }
            //奈法利安 任务12 
            if (title.Contains("暗影烈焰"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "暗影烈焰", EquipConst.多彩龙军团),
                    TaskAdapter.GetMercenary(MercConst.希奈丝特拉, "法力壁垒", EquipConst.法力胸针, -1, HsMercenaryStrategy.TARGETTYPE.FRIENDLY),
                    TaskAdapter.GetMercenary(MercConst.玉珑, "玉火打击", EquipConst.玉火之矛)
                }));
                return;
            }
            //奈法利安 任务17
            if (title.Contains("还算有用"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "H1-2", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,"多彩能量", EquipConst.多彩龙军团),
                    TaskAdapter.GetMercenary(MercConst.卡德加,"炉火",EquipConst.焙烤培根),
                    TaskAdapter.GetMercenary(MercConst.魔像师卡扎库斯,"暗影之爪",EquipConst.野葡萄藤)
                }));
                return;
            }
            //暴龙王克鲁什  任务12
            if (title.Contains("暴龙之王"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-6", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,"顶级捕食者", EquipConst.烈焰利爪),
                    TaskAdapter.GetMercenary(MercConst.洛卡拉,"进攻集结",EquipConst.激励头盔),
                    TaskAdapter.GetMercenary(MercConst.魔像师卡扎库斯,"暗影之爪",EquipConst.野葡萄藤)
                }));
                return;
            }
            //**********护卫任务***********//
            if(title.Contains("愤怒即时力量") || title.Contains("饱受折磨") || title.Contains("萨格拉斯的腐蚀"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-5", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,"无羁之怒", 2),
                    TaskAdapter.GetMercenary(MercConst.玉珑,"玉火打击",0),
                    TaskAdapter.GetMercenary(MercConst.赤精,"振奋之歌", EquipConst.赤精之杖),

                }));
            }
            if (title.Contains("我们必胜"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-5", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,"天神下凡", EquipConst.无法撼动之物),
                    TaskAdapter.GetMercenary(MercConst.卡德加,"炉火",EquipConst.焙烤培根),
                    TaskAdapter.GetMercenary(MercConst.魔像师卡扎库斯,"暗影之爪", EquipConst.野葡萄藤),

                }));
            }
            //玛法里奥
            if (title.Contains("自然力量"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "大德鲁伊的召唤", EquipConst.石南草, 0, HsMercenaryStrategy.TARGETTYPE.UNSPECIFIED)
                }));
                return;
            }
            if (title.Contains("以艾萨拉之名") || title.Contains("水流之力"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-5", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,"俘获之潮",EquipConst.蔑视),
                    TaskAdapter.GetMercenary(MercConst.玉珑,"玉火打击",0),
                    TaskAdapter.GetMercenary(MercConst.赤精,"振奋之歌", EquipConst.赤精之杖),
                }));
                return;
            }
            //加拉克苏斯大王 任务11 任务10
            if (title.Contains("地狱火顺劈") || title.Contains("邪能烈焰"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-5", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,"加拉克苏斯之拳",EquipConst.实击护手),
                    TaskAdapter.GetMercenary(MercConst.玛诺洛斯,"恐惧嚎叫",EquipConst.深渊领主之杖),
                    TaskAdapter.GetMercenary(MercConst.拉索利安,"巨型大恶魔",EquipConst.恶魔印记)
                }));
                return;
            }
            //库尔特鲁斯·殒烬 任务10
            if (title.Contains("独自训练"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 1, "H1-2", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,"屠魔者", EquipConst.恶魔卫士),
                    TaskAdapter.GetMercenary(MercConst.卡德加,"炉火",EquipConst.焙烤培根),
                    TaskAdapter.GetMercenary(MercConst.魔像师卡扎库斯,"暗影之爪",EquipConst.野葡萄藤)
                }));
                return;
            }
            //格罗玛什·地狱咆哮  任务10
            if (title.Contains("暂缓苦痛"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-5", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,"战斗怒火", EquipConst.饮血坠饰),
                    TaskAdapter.GetMercenary(MercConst.卡德加,"炉火",EquipConst.焙烤培根),
                    TaskAdapter.GetMercenary(MercConst.魔像师卡扎库斯,"暗影之爪",EquipConst.野葡萄藤)
                }));
                return;
            }
            //格罗玛什·地狱咆哮  任务14
            if (title.Contains("玛诺洛斯的许诺"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "H1-1", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,"战斗怒火", EquipConst.饮血坠饰),
                    TaskAdapter.GetMercenary(MercConst.厨师曲奇 ,"小鱼快冲",0)
                }));
                return;
            }
            //瓦里安·乌瑞恩 任务3 任务17
            if (title.Contains("为了暴风城") || title.Contains("屹立不倒"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 5, "2-5", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "反击", EquipConst.战争旗帜)
                }));
                return;
            }
            //拉格纳罗斯 任务10 
            if (title.Contains("全部烧光"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "熔岩冲击", EquipConst.熔火之心),
                    TaskAdapter.GetMercenary(MercConst.安东尼达斯, null, 0)
                }));
                return;
            }
            //阿莱克斯塔萨 任务17
            if (title.Contains("他们的力量属于我"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-5", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.伊莉斯_逐星,"黄金猿",EquipConst.猴爪),
                    TaskAdapter.GetMercenary(MercConst.玉珑, "玉火打击", 0),
                    TaskAdapter.GetMercenary(mercenaryId, "红龙女王的计策", EquipConst.巨龙军团护身符)
                }));
                return;
            }
            //阿莱克斯塔萨 任务12 任务14
            if (title.Contains("一举两得") || title.Contains("蹈踏火焰"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "烈焰猛击", 0),
                    TaskAdapter.GetMercenary(MercConst.玉珑, "玉火打击", 0)
                }));
                return;
            }
            //穆克拉 任务17
            if (title.Contains("家族纽带"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 5, "H1-1", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,"原始之力",EquipConst.穆克拉的大表哥)
                }));
                return;
            }
            //玛诺洛斯 任务12
            if (title.Contains("愚蠢的弱者"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,"邪能抽笞",EquipConst.邪能尖刺),
                    TaskAdapter.GetMercenary(MercConst.泽瑞拉,"快速治疗",0)
                }));
                return;
            }
            //蛇 任务2
            if (title.Contains("无情抽笞"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-5", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.考内留斯_罗姆,"坚守前线",0),
                    TaskAdapter.GetMercenary(mercenaryId,"反冲",EquipConst.锋锐利爪),
                    TaskAdapter.GetMercenary(MercConst.凯瑞尔_罗姆,"嘲讽",0)
                }));
                return;
            }
            if (title.Contains("侧面横扫"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-5", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,null,0),
                    TaskAdapter.GetMercenary(MercConst.瓦罗克_萨鲁法尔, "旋风斩", EquipConst.狂战士之刃)

                }));
                return;
            }
            //巫妖王 任务9
            if (title.Contains("你们都将臣服"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "凋零缠绕", EquipConst.虚空之踏, -1, HsMercenaryStrategy.TARGETTYPE.FRIENDLY)
                }));
                return;
            }
            //梵妮莎 任务9
            if (title.Contains("夺刀斩将"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "H1-1", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "剽窃轰击", 0),
                    TaskAdapter.GetMercenary(MercConst.提里奥_弗丁,"谦逊制裁",0),
                    TaskAdapter.GetMercenary(MercConst.玉珑, "玉火打击", 0)
                }));
                return;
            }
            //梵妮莎 任务14
            if (title.Contains("一刃双雕"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-5", new MercenaryEntity[]
                {

                    TaskAdapter.GetMercenary(MercConst.厨师曲奇,"小鱼快冲",2),
                    TaskAdapter.GetMercenary(mercenaryId, "临时武器", EquipConst.加装口袋),
                    TaskAdapter.GetMercenary(MercConst.瓦罗克_萨鲁法尔, "旋风斩", EquipConst.狂战士之刃)
                }));
                return;
            }
            //斯尼德 任务9
            if (title.Contains("手脚并用"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "H1-1", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "缴械", 0),
                    TaskAdapter.GetMercenary(MercConst.提里奥_弗丁,"谦逊制裁",0),
                    TaskAdapter.GetMercenary(MercConst.玉珑, "玉火打击", 0)
                }));
                return;
            }
            //斯尼德 任务12 任务14
            if (title.Contains("一刀两断") || title.Contains("乌合之众"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "嗞拉", EquipConst.加装锯刃)
                }));
                return;
            }
            //重拳先生 任务14
            if (title.Contains("铁脚无情"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-6", new MercenaryEntity[]
                {

                    TaskAdapter.GetMercenary(MercConst.洛卡拉, "进攻集结", EquipConst.激励头盔),
                    TaskAdapter.GetMercenary(mercenaryId, "落水追击", 0),
                    TaskAdapter.GetMercenary(MercConst.海盗帕奇斯, "眼魔船长", EquipConst.武器柜)
                }));
                return;
            }
            //雪王 任务12
            if (title.Contains("雪球滚滚"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-5", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.伊莉斯_逐星,"黄金猿",EquipConst.猴爪),
                    TaskAdapter.GetMercenary(MercConst.玉珑, "玉火打击", 0),
                    TaskAdapter.GetMercenary(mercenaryId, "雪球", EquipConst.冰雪之王的遮蔽)
                }));
                return;
            }
            //玉珑 任务12
            if (title.Contains("成长与新生"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 5, "2-6", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, "青玉劲风", 0),
                }));
                return;
            }
            //范达尔·雷矛 任务17 
            if (title.Contains("拼尽全力"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "H1-2", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,"向前推进", EquipConst.无法撼动之物),
                    TaskAdapter.GetMercenary(MercConst.卡德加,"炉火",EquipConst.焙烤培根),
                    TaskAdapter.GetMercenary(MercConst.魔像师卡扎库斯,"暗影之爪",EquipConst.野葡萄藤)
                }));
                return;
            }
            //考内留斯
            if (title.Contains("我来承受伤害") || title.Contains("进攻与收获") || title.Contains("向我集结"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-5", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.奈姆希_灵沼,"沼泽工程学",0),
                    TaskAdapter.GetMercenary(mercenaryId,"坚守前线", EquipConst.启迪环带),
                    TaskAdapter.GetMercenary(MercConst.穆克拉,"大餐时间",0)
                }));
                return;
            }

            //玛诺洛斯
            if (title.Contains("让他们学会畏惧"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "2-6", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId,"恐惧嚎叫", EquipConst.邪能魔肺)
                }));
                return;
            }
            //--------------------------------------------------------------------------------//
            //--------------------------------通用任务----------------------------------------//
            //--------------------------------------------------------------------------------//
            //回复生命类任务
            //赛琳 任务17  维伦 任务14 光明之翼 任务17 任务11 希奈斯特拉 任务12 
            if (title.Contains("平衡倾斜") || title.Contains("纯净圣光") ||
                title.Contains("治疗朋友") || title.Contains("我们交朋友吧") ||
                title.Contains("低语显现") || title.Contains("包扎伤口") ||
                title.Contains("生命守护者") || title.Contains("耐心取胜"))
            {

                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, "H2-5", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.光明之翼,"妖精之尘", EquipConst.妖精口袋),
                    TaskAdapter.GetMercenary(MercConst.赤精,"振奋之歌", EquipConst.赤精之杖),
                    TaskAdapter.GetMercenary(MercConst.泽瑞拉,"快速治疗", EquipConst.强光魔杖,-1,HsMercenaryStrategy.TARGETTYPE.FRIENDLY)
                }));
                return;
            }
            //杀恶魔
            if (desc.Contains("击败") && desc.Contains("个恶魔"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 5, "2-6", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, null, 0)
                }));
                return;
            }
            //杀龙
            if ((desc.Contains("击败") || desc.Contains("消灭")) && desc.Contains("条龙"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 5, "4-1", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, null, 0)
                }));
                return;
            }
            //杀野兽
            if (desc.Contains("击败") && desc.Contains("只野兽"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 5, "H1-1", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, null, 0)
                }));
                return;
            }
            //杀鱼人
            if (desc.Contains("消灭") && desc.Contains("鱼人"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 5, "7-1", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, null, 0)
                }));
                return;
            }
            //杀元素
            if (desc.Contains("击败") && desc.Contains("元素"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 5, "H1-2", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, null, 0)
                }));
                return;
            }
            //巨魔造成伤害
            if (desc.Contains("使用包含此佣兵的队伍") && desc.Contains("使用巨魔造成"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.沃金, "暗影涌动", EquipConst.沃金战刃),
                    TaskAdapter.GetMercenary(mercenaryId, null, 0)
                }));
                return;
            }
            //海盗造成伤害
            if (desc.Contains("使用包含此佣兵的队伍") && desc.Contains("使用海盗造成"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.海盗帕奇斯, "眼魔船长", EquipConst.勾住船员),
                    TaskAdapter.GetMercenary(mercenaryId, null, 0)
                }));
                return;
            }
            //恶魔造成伤害
            if (desc.Contains("使用包含此佣兵的队伍") && desc.Contains("使用恶魔造成"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.拉索利安, "巨型大恶魔", EquipConst.恶魔灰烬),
                    TaskAdapter.GetMercenary(mercenaryId, null, 0)
                }));
                return;
            }
            //野兽造成伤害
            if (desc.Contains("使用包含此佣兵的队伍") && desc.Contains("使用野兽造成"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.闪狐, null, 0),
                    TaskAdapter.GetMercenary(mercenaryId, null, 0)
                }));
                return;
            }
            //元素造成伤害
            if (desc.Contains("使用包含此佣兵的队伍") && desc.Contains("使用元素造成"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.冰雪之王洛克霍拉, null, 0),
                    TaskAdapter.GetMercenary(mercenaryId, null, 0)
                }));
                return;
            }
            //龙造成伤害
            if (desc.Contains("使用包含此佣兵的队伍") && desc.Contains("使用龙造成"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.奈法利安,"龙人突袭",EquipConst.多彩龙军团),
                    TaskAdapter.GetMercenary(mercenaryId, null, 0)

                }));
                return;
            }
            //人类造成伤害
            if (desc.Contains("使用包含此佣兵的队伍") && desc.Contains("使用人类造成"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.安东尼达斯,"烈焰风暴",EquipConst.烈焰饰环),
                    TaskAdapter.GetMercenary(mercenaryId, null, 0)

                }));
                return;
            }
            //造成神圣伤害
            if (desc.Contains("使用包含此佣兵的队伍") && desc.Contains("神圣伤害"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.泽瑞拉, null, 0),
                    TaskAdapter.GetMercenary(mercenaryId, null, 0)
                }));
                return;
            }
            //造成冰霜伤害
            if (desc.Contains("使用包含此佣兵的队伍") && desc.Contains("冰霜伤害"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.冰雪之王洛克霍拉,"冰雹", EquipConst.刺骨寒风),
                    TaskAdapter.GetMercenary(mercenaryId, null, 0)
                }));
                return;
            }
            //造成火焰伤害
            if (desc.Contains("使用包含此佣兵的队伍") && desc.Contains("火焰伤害"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.安东尼达斯,"烈焰风暴",EquipConst.烈焰饰环),
                    TaskAdapter.GetMercenary(mercenaryId, null, 0)
                }));
                return;
            }
            //造成暗影伤害
            if (desc.Contains("使用包含此佣兵的队伍") && desc.Contains("暗影伤害"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.沃金, "虚弱诅咒", 0),
                    TaskAdapter.GetMercenary(mercenaryId, null, 0)
                }));
                return;
            }
            //造成邪能伤害
            if (desc.Contains("使用包含此佣兵的队伍") && desc.Contains("邪能伤害"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.玛诺洛斯, null, 0),
                    TaskAdapter.GetMercenary(mercenaryId, null, 0)
                }));
                return;
            }
            //造成自然伤害
            if (desc.Contains("使用包含此佣兵的队伍") && desc.Contains("自然伤害"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 0, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(MercConst.布鲁坎, "闪电箭", EquipConst.雷鸣系带),
                    TaskAdapter.GetMercenary(mercenaryId, null, 0)
                }));
                return;
            }
            //英雄难度相关
            if (desc.Contains("英雄难度"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 1, "H1-1", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, null, 0)
                }));
                return;
            }
            if (desc.Contains("完成") && desc.Contains("个悬赏"))
            {
                LettuceMercenary mercenary = HsGameUtils.GetMercenary(mercenaryId);
                if (desc.Contains("30级时") && !mercenary.IsMaxLevel())
                {
                    tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 10, new MercenaryEntity[]
                    {
                        TaskAdapter.GetMercenary(mercenaryId, null, 0)
                    }));
                }
                else
                {
                    tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 5, "H1-1", new MercenaryEntity[]
                    {
                        TaskAdapter.GetMercenary(mercenaryId, null, 0)
                    }));
                }
                return;
            }
            //抵达神秘选项，赐福或灵魂医者处
            if (desc.Contains("赐福或灵魂医者"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, null, 0)
                }));
                TaskUtils.HaveTaskDocter = true;
                TaskUtils.HaveTaskCaster = true;
                TaskUtils.HaveTaskFighter = true;
                TaskUtils.HaveTaskTank = true;
                return;
            }

            //灵魂医者
            if (desc.Contains("利用灵魂医者"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 5, "H1-1", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, null, 0)
                }));
                TaskUtils.HaveTaskDocter = true;
                return;
            }
            //赐福
            if (desc.Contains("利用赐福"))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, 5, "H1-2", new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, null, 0)
                }));
                if (desc.Contains("施法者"))
                    TaskUtils.HaveTaskCaster = true;
                else if (desc.Contains("斗士"))
                    TaskUtils.HaveTaskFighter = true;
                else if (desc.Contains("护卫"))
                    TaskUtils.HaveTaskTank = true;
                return;
            }
            int num = desc.IndexOf("$ability(", StringComparison.Ordinal);
            if (num == -1)
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, null, 0)
                }));
                return;
            }
            int num2 = num + "$ability(".Length;
            int num3 = desc.IndexOf(")", num2, StringComparison.Ordinal);
            if (num3 == -1)
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, null, 0)
                }));
                return;
            }
            string[] array = desc.Substring(num2, num3 - num2).Split(new char[]
            {
                ','
            });
            int num4;
            int num5;
            if (!int.TryParse(array[0], out num4) || !int.TryParse(array[1], out num5))
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, null, 0)
                }));
                return;
            }
            LettuceMercenaryDbfRecord record = GameDbf.LettuceMercenary.GetRecord(mercenaryId);
            if (num4 >= record.LettuceMercenarySpecializations.Count)
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, null, 0)
                }));
                return;
            }
            LettuceMercenarySpecializationDbfRecord lettuceMercenarySpecializationDbfRecord = record.LettuceMercenarySpecializations[num4];
            if (num5 >= lettuceMercenarySpecializationDbfRecord.LettuceMercenaryAbilities.Count)
            {
                tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, new MercenaryEntity[]
                {
                    TaskAdapter.GetMercenary(mercenaryId, null, 0)
                }));
                return;
            }
            int lettuceAbilityId = lettuceMercenarySpecializationDbfRecord.LettuceMercenaryAbilities[num5].LettuceAbilityId;
            LettuceAbilityDbfRecord record2 = GameDbf.LettuceAbility.GetRecord(lettuceAbilityId);
            tasks.Add(TaskAdapter.GetTask(progressMessage, taskId, new MercenaryEntity[]
            {
                TaskAdapter.GetMercenary(mercenaryId, record2.AbilityName, GetEquipEnHanceSkill(record2.AbilityName))
            }));
        }

        public static void SetMainLineTask(List<Task> tasks, string desc)//5.7改动
        {
            Out.Log($"[主线] {desc}");


            Regex regex = new Regex(@"完成(\D.+)悬赏。$");
            Match match = regex.Match(desc);
            if (match.Groups.Count == 2)
            {
                string mapName = MapUtils.GetMapByBoss(match.Groups[1].Value)?.Name ?? "";
                tasks.Add(TaskAdapter.GetTask("", -1, 0, mapName, null));
                return;
            }

            switch (desc)
            {
                //贫瘠之地
                case "击败8只野兽。":
                    tasks.Add(TaskAdapter.GetTask("", -1, 0, "1-6", null));
                    break;
                //费伍德森林
                case "在费伍德森林击败20个敌人。":
                    tasks.Add(TaskAdapter.GetTask("", -1, 0, "2-1", null));
                    break;
                case "击败15个恶魔。":
                    tasks.Add(TaskAdapter.GetTask("", -1, 0, "2-6", null));
                    break;
                //冬泉谷
                case "击败12个熊怪。":
                case "在冬泉谷完成3个悬赏。":
                    tasks.Add(TaskAdapter.GetTask("", -1, 0, "3-1", null));
                    break;
                //黑石山
                case "在黑石山造成2000点伤害。":
                case "在黑石山击败50个敌人。":
                case "在黑石山击败60个敌人。":
                case "在黑石山造成2500点伤害。":
                    tasks.Add(TaskAdapter.GetTask("", -1, 0, "4-1", null));
                    break;
                case "在黑石山击败25个元素。":
                    tasks.Add(TaskAdapter.GetTask("", -1, 0, "4-4", null));
                    break;
                //奥特兰克山谷
                case "在奥特兰克山谷击败25个敌人。":
                case "在奥特兰克造成2000点伤害。":
                    tasks.Add(TaskAdapter.GetTask("", -1, 0, "5-1", null));
                    break;
            }
        }
        // Token: 0x06000054 RID: 84 RVA: 0x00005BB0 File Offset: 0x00003DB0
        private static MercenaryEntity[] GetQuickMercenary(int mercenaryId)
        {
            List<MercenaryEntity> list = new List<MercenaryEntity>();
            foreach (LettuceAbility lettuceAbility in CollectionManager.Get().GetMercenary((long)mercenaryId, false, true).m_abilityList)
            {
                CardDbfRecord cardRecord = GameDbf.GetIndex().GetCardRecord(lettuceAbility.GetCardId());
                string @string = cardRecord.TextInHand.GetString(Locale.zhCN);
                if (@string != null && @string.Contains("速度值") && @string.Contains("加快"))
                {
                    string string2 = cardRecord.Name.GetString(Locale.zhCN);
                    list.Add(TaskAdapter.GetMercenary(mercenaryId, string2.Substring(0, string2.Length - 1), 0));
                }
            }
            return list.ToArray();
        }

        // Token: 0x06000055 RID: 85 RVA: 0x00005C80 File Offset: 0x00003E80
        private static Task GetTask(string progress, int id, int priority, string map, params MercenaryEntity[] mercenaries)
        {
            return new Task
            {
                water = ++m_globalWater,
                Priority = priority,
                Map = map,
                Mercenaries = mercenaries?.ToList(),
                Id = id,
                ProgressMessage = progress
            };
        }

        private static Task GetTask(string progress, int id, int priority, params MercenaryEntity[] mercenaries)
        {
            return new Task
            {
                water = ++m_globalWater,
                Id = id,
                Mercenaries = mercenaries?.ToList(),
                Priority = priority,
                ProgressMessage = progress
            };
        }

        // Token: 0x06000056 RID: 86 RVA: 0x00005CA8 File Offset: 0x00003EA8
        private static Task GetTask(string progress, int id, params MercenaryEntity[] mercenaries)
        {
            return new Task
            {
                water = ++m_globalWater,
                Id = id,
                Mercenaries = mercenaries?.ToList(),
                Priority = 5
            };
        }

        // Token: 0x06000057 RID: 87 RVA: 0x00005CC9 File Offset: 0x00003EC9
        private static MercenaryEntity GetMercenary(int id, string skill = null, int eq = 0, int subskill = -1, HsMercenaryStrategy.TARGETTYPE targettype = HsMercenaryStrategy.TARGETTYPE.UNSPECIFIED)
        {
            return new MercenaryEntity(id, skill, eq, subskill, targettype);
        }

        private static int GetEquipEnHanceSkill(string skill)
        {
            return m_dictSkillEquip.ContainsKey(skill) ? m_dictSkillEquip[skill] : 0;
        }

        private static Dictionary<string, int> m_dictSkillEquip = new Dictionary<string, int>{
            { "神圣冲击", EquipConst.激励秘典 },//维伦12技能
			{ "裂解之光", EquipConst.受祝福的碎片 },
            { "致盲之光", EquipConst.强光魔杖 },//泽瑞拉 13技能
			{ "救赎", EquipConst.纯洁长袍 },
            { "魔爆术", EquipConst.奥术粉尘 },//豪斯13技能
			{ "强能奥术飞弹", EquipConst.魔网之杖 },
            { "集束暗影", EquipConst.暗影之眼 },//塞林123技能
			{ "集束之光", EquipConst.神圣之眼 },
            { "咒逐", EquipConst.诺达希尔碎片 },
            { "精灵吐息", EquipConst.光明之翼的项链 },//光明之翼123技能
			{ "妖精之尘", EquipConst.妖精口袋 },
            { "相位变换", EquipConst.光明之翼的圆环 },
            { "热力迸发", EquipConst.熔岩之刃 },//迦顿12技能
			{ "地狱火", EquipConst.焚火印记 },
            { "闪电箭", EquipConst.雷鸣系带 },//布鲁坎1技能
			{ "符文猛击", EquipConst.盛开菌菇 },//古夫123技能
			{ "活体荆棘", EquipConst.木棘图腾 },
            { "铁木树皮", EquipConst.土灵护腕 },
            { "冰风暴", EquipConst.寒冰药水 },//晨拥12技能
			{ "急速冰冻", EquipConst.霜冻之戒 },
            { "暗影箭", EquipConst.暗影符文 },//塔姆辛12技能
			{ "虚空吞噬者", EquipConst.虚空石 },
            { "毁灭之雨", EquipConst.玛诺洛斯之血 },//古尔丹23技能
			{ "生命虹吸", EquipConst.灵魂护符 },
            { "暗影震击", EquipConst.静滞海马 },//沃金12技能
			{ "暗影涌动", EquipConst.沃金战刃 },
            { "制裁之锤", EquipConst.光明使者 },//乌瑟尔13技能
			{ "复仇之怒", EquipConst.微光肩甲 },
            { "冰刺", EquipConst.寒冰碎片 },//吉安娜1技能
			{ "火球术", EquipConst.烬核之杖 },//安东尼12技能
			{ "烈焰风暴", EquipConst.烈焰饰环 },
            { "奥术飞掷", EquipConst.奥术尖牙 },//闪狐12技能
			{ "法力闪现", EquipConst.法力符文 },
            { "曲奇下厨", EquipConst.钢铁汤勺 },//曲奇2技能
			{ "暗影之爪", EquipConst.魔皇草 }, //卡扎1技能
			{ "天空守卫", EquipConst.浮夸腰带 },//空军上将罗杰斯1技能
			{ "烈焰之歌", EquipConst.烈焰系带 },//赤精12技能
			{ "振奋之歌", EquipConst.赤精之杖 },
            { "傲慢的凡人", EquipConst.锋刃之爪 },//奥妮克希亚12技能
			{ "深呼吸", EquipConst.更深的呼吸 },
            { "杀戮命令", EquipConst.迅羽之弓 },//雷克萨123技能
			{ "动物伙伴", EquipConst.熊妈妈之爪 },
            { "爆炸射击", EquipConst.猎手的步枪 },
            { "战术打击", EquipConst.军情七处合约 },//刀油123技能
			{ "邪恶挥刺", EquipConst.剥皮刀 },
            { "暗影之刃", EquipConst.精磨之杖 },
            { "部族战争", EquipConst.霜狼护身符 },//洛卡拉12技能
			{ "进攻集结", EquipConst.激励头盔 },
            { "侧翼突击", EquipConst.拳刃 }, //伊利丹12技能
			{ "流放攻击", EquipConst.埃辛诺斯战刃 },
            { "女妖之箭", EquipConst.哀伤之剑 }, //风行者12技能
			{ "重拾灵魂", EquipConst.灵魂缶 },
            { "鱼人入侵", EquipConst.泡泡魔杖 }, //老瞎眼123技能
			{ "邪鳍导航员", EquipConst.导航员的护符 },
            { "老蓟皮", EquipConst.始生鱼人 },
            { "顶级捕食者", EquipConst.烈焰利爪 },//暴龙王123技能
			{ "惊骇", EquipConst.新鲜的肉 },
            { "魔暴龙", EquipConst.岩质甲壳 },
            { "瞄准射击", EquipConst.高能枪弹 },//塔维什·雷矛123 技能
			{ "爆炸陷阱", EquipConst.拉线操纵 },
            { "捕熊陷阱", EquipConst.拉线操纵 },
            { "动员打击", EquipConst.萨隆踏靴 },//萨鲁法尔12技能
			{ "旋风斩", EquipConst.狂战士之刃 },
            { "二连击", EquipConst.燃烧之刃 },//剑圣123技能
			{ "镜像", EquipConst.幻觉饰带 },
            { "旋风之刃", EquipConst.磨砺之刃 },
            { "鳞甲嘲讽", EquipConst.炫彩项链 },//吃手手12技能
			{ "吞噬攻击", EquipConst.土灵护甲 },
            { "火焰吐息", EquipConst.巨龙印记 },//指挥官沃恩23技能
			{ "辟法巨龙", EquipConst.巨龙之爪 },
            { "毒蛇噬咬", EquipConst.剧毒毒液 },//安娜12技能
			{ "野兽恢复", EquipConst.狂野的徽记 },
            { "奥术射击", EquipConst.艾露恩的护符 },//泰兰德123技能
			{ "奥术齐射", EquipConst.苍翠反曲弓 },
            { "艾露恩的赐福", EquipConst.狂野之戒 },
            { "末日冲锋", EquipConst.恐怖利爪 },//迪亚波罗13技能
			{ "末日", EquipConst.熔岩魔角 },
            { "首脑的悬赏", EquipConst.公平分配 }, // 迪菲亚首脑23技能
			{ "辅助打击", EquipConst.轮番豪饮 },
            { "火炮突击", EquipConst.装填武器 },//尤朵拉123
			{ "准备火炮", EquipConst.装填武器 },
            { "掩护射击", EquipConst.装填武器 },
            { "致命打击", EquipConst.德雷克塔尔的恩惠 },//加尔范23技能
			{ "破胆怒吼", EquipConst.震耳咆哮 },
            { "蠕行疯狂", EquipConst.恐惧奇美拉 },//拉希奥12技能
			{ "扫尾", EquipConst.黑龙鳞片 },
            { "均衡一击", EquipConst.雪怒之矛 },//雪怒123技能
			{ "白虎飞扑", EquipConst.天神胸甲 },
            { "伏虎闪雷", EquipConst.滚雷护爪 },
            { "军团爆能", EquipConst.狂怒坠饰 }, //大王
			{ "加拉克苏斯之拳", EquipConst.实击护手 },
            { "邪能地狱火", EquipConst.邪能核心 },
            { "眼棱", EquipConst.混乱护符 }, //库尔特鲁斯123
			{ "屠魔者", EquipConst.恶魔卫士 },
            { "盲目突击", EquipConst.恶魔卫士 },
            { "热血", EquipConst.血吼 },//格罗玛什·地狱咆哮123
			{ "惊愕猛击", EquipConst.休止饰带 },
            { "战斗怒火", EquipConst.饮血坠饰 },
            { "爆裂打击", EquipConst.萨拉迈恩 },//瓦里安·乌瑞恩123
			{ "英勇飞跃", EquipConst.界限之靴 },
            { "反击", EquipConst.战争旗帜 },
            { "远征军打击", EquipConst.黎明之锤 },//凯瑞尔123
			{ "嘲讽", EquipConst.圣光秘典 },
            { "光明圣印", EquipConst.裁决秘典 },
            { "熔岩冲击", EquipConst.熔火之心 },//拉格纳罗斯123
			{ "死吧，虫子", EquipConst.炽烧符文 },
            { "陨石术", EquipConst.炽烧符文 },
            { "先祖勾拳", EquipConst.重生 },//凯恩血蹄123
			{ "坚韧光环", EquipConst.迅捷图腾 },
            { "大地践踏", EquipConst.雷霆饰带 },
            { "暴食香蕉", EquipConst.辐射香蕉 },//穆克拉123
			{ "大餐时间", EquipConst.辐射香蕉 },
            { "原始之力", EquipConst.穆克拉的大表哥 },
            { "为了部落", EquipConst.毁灭之锤 },//萨尔13
			{ "大酋长的命令", EquipConst.华丽的军号 },
            { "荣耀决斗", EquipConst.决斗护手 },//加尔鲁什·地狱咆哮123
			{ "部落之力", EquipConst.玛诺洛斯的獠牙 },
            { "战斗怒吼", EquipConst.兽人的旗帜 },
            { "奔跑破坏神", EquipConst.深渊领主之杖 },//玛诺洛斯123
			{ "邪能抽笞", EquipConst.邪能尖刺 },
            { "恐惧嚎叫", EquipConst.邪能魔肺 },
            { "塞纳里奥波动", EquipConst.活根草之杖 },//玛法里奥·怒风
			{ "纠缠根须", EquipConst.森林徽章 },
            { "大德鲁伊的召唤", EquipConst.石南草 },
            { "狂暴攻击", EquipConst.注能琥珀 },//蛇
			{ "反冲", EquipConst.锋锐利爪 },
            { "再生头颅", EquipConst.再生之鳞 },
            { "武艺精通" , EquipConst.猛击护手 },//考内留斯123
			{ "坚守前线" , EquipConst.启迪环带 },
            { "寒冰之噬", EquipConst.霜之哀伤 },//巫妖王12
			{ "凋零缠绕", EquipConst.虚空之踏 },
            { "嗞啦", EquipConst.加装锯刃 },
            { "启动电锯", EquipConst.泰坦神铁锯刃 },
            { "缴械", EquipConst.加装锯刃 },
            { "重拳猛击", EquipConst.水手帽 },//重拳13
			{ "落水追击", EquipConst.沉重铁锚 },
            { "剽窃轰击", EquipConst.恐惧之刃 },//梵妮莎·范克里夫
			{ "临时武器", EquipConst.夺刀手 },
            { "搬运背包", EquipConst.夺刀手 },
            { "冰雹", EquipConst.刺骨寒风 },//雪王
			{ "冰霜震击", EquipConst.刺骨寒风 },
            { "雪球", EquipConst.刺骨寒风 },
            { "雷霆打击", EquipConst.无坚不摧之力 },//范达尔·雷矛
			{ "向前推进", EquipConst.无法撼动之物 },
            { "天神下凡", EquipConst.无法撼动之物 },
            { "恐怖利爪", EquipConst.不死者之心 },//死亡之翼
			{ "源质护甲", EquipConst.不朽者之毅 },
        };

        private static int m_globalWater = 0;
    }
}
