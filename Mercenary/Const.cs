﻿using System.Collections.Generic;

namespace Mercenary
{
    // Token: 0x02000005 RID: 5
    internal class MercConst
    {
        public const int 乌瑟尔_光明使者 = 94;
        public const int 亚煞极 = 344;
        public const int 伊利丹_怒风 = 12;
        public const int 伊瑞尔 = 367;
        public const int 伊瑟拉 = 329;
        public const int 伊莉斯_逐星 = 383;
        public const int 先知维伦 = 5;
        public const int 光明之翼 = 24;
        public const int 克苏恩 = 328;
        public const int 冰雪之王洛克霍拉 = 251;
        public const int 凯恩_血蹄 = 26;
        public const int 凯瑞尔_罗姆 = 18;
        public const int 剑圣萨穆罗 = 53;
        public const int 加尔范上尉 = 250;
        public const int 加尔鲁什_地狱咆哮 = 50;
        public const int 加拉克苏斯大王 = 2;
        public const int 半兽人迦罗娜 = 416;
        public const int 卡德加 = 409;
        public const int 厨师曲奇 = 149;
        public const int 变装大师 = 376;
        public const int 古夫_符文图腾 = 28;
        public const int 古尔丹 = 60;
        public const int 吉安娜_普罗德摩尔 = 97;
        public const int 吉恩_格雷迈恩 = 411;
        public const int 塔姆辛_罗姆 = 45;
        public const int 塔维什_雷矛 = 44;
        public const int 奈姆希_灵沼 = 403;
        public const int 奈法利安 = 306;
        public const int 奔波尔霸 = 312;
        public const int 奥妮克希亚 = 291;
        public const int 娜塔莉_塞林 = 8;
        public const int 安东尼达斯 = 98;
        public const int 安娜科德拉 = 63;
        public const int 安度因_乌瑞恩 = 93;
        public const int 尤朵拉 = 236;
        public const int 尤格_萨隆 = 308;
        public const int 尼尔鲁_火刃 = 402;
        public const int 巫妖王 = 99;
        public const int 巴琳达_斯通赫尔斯 = 249;
        public const int 布莱恩_铜须 = 373;
        public const int 布鲁坎 = 21;
        public const int 希奈丝特拉 = 253;
        public const int 希尔瓦娜斯_风行者 = 15;
        public const int 库尔特鲁斯_陨烬 = 3;
        public const int 德雷克塔尔 = 293;
        public const int 恩佐斯 = 341;
        public const int 拉希奥 = 252;
        public const int 拉格纳罗斯 = 20;
        public const int 拉索利安 = 55;
        public const int 指挥官沃恩 = 57;
        public const int 提里奥_弗丁 = 4;
        public const int 摩洛克_福尔摩斯 = 421;
        public const int 斯卡布斯_刀油 = 7;
        public const int 斯尼德 = 156;
        public const int 暴龙王克鲁什 = 43;
        public const int 格罗玛什_地狱咆哮 = 6;
        public const int 格鲁尔 = 25;
        public const int 梵妮莎_范克里夫 = 238;
        public const int 死亡之翼 = 298;
        public const int 沃金 = 62;
        public const int 泰兰德_语风 = 69;
        public const int 泰瑞尔 = 311;
        public const int 泽瑞拉 = 19;
        public const int 洛卡拉 = 11;
        public const int 海巫扎尔吉拉 = 294;
        public const int 海盗帕奇斯 = 374;
        public const int 深水领主卡拉瑟雷斯 = 295;
        public const int 游学者周卓 = 366;
        public const int 滑矛领主 = 305;
        public const int 潮汐主母阿茜萨 = 304;
        public const int 火车王里诺艾 = 284;
        public const int 玉珑 = 285;
        public const int 玛克扎尔王子 = 397;
        public const int 玛法里奥_怒风 = 64;
        public const int 玛维_影歌 = 372;
        public const int 玛诺洛斯 = 54;
        public const int 珑心 = 254;
        public const int 瓦丝琪女士 = 289;
        public const int 瓦尔登_晨拥 = 38;
        public const int 瓦罗克_萨鲁法尔 = 52;
        public const int 瓦莉拉_萨古纳尔 = 17;
        public const int 瓦里安_乌瑞恩 = 9;
        public const int 砮皂 = 283;
        public const int 神谕者摩戈尔 = 41;
        public const int 穆克拉 = 42;
        public const int 穆坦努斯 = 56;
        public const int 空军上将罗杰斯 = 242;
        public const int 米尔豪斯_法力风暴 = 14;
        public const int 老瞎眼 = 40;
        public const int 考内留斯_罗姆 = 95;
        public const int 艾德温_迪菲亚首脑 = 159;
        public const int 艾萨拉女王 = 290;
        public const int 芬利爵士 = 339;
        public const int 苔丝_格雷迈恩 = 407;
        public const int 范达尔_雷矛 = 292;
        public const int 萨尔 = 49;
        public const int 血怒者科尔拉克 = 296;
        public const int 贝恩_血蹄 = 371;
        public const int 赤精 = 286;
        public const int 迦顿男爵 = 22;
        public const int 迪亚波罗 = 102;
        public const int 邦桑迪 = 408;
        public const int 重拳先生 = 237;
        public const int 钩牙船长 = 309;
        public const int 闪狐 = 100;
        public const int 阿克蒙德 = 398;
        public const int 阿兰娜_逐星 = 414;
        public const int 阿莱克丝塔萨 = 23;
        public const int 雪怒 = 282;
        public const int 雷克萨 = 1;
        public const int 雷诺_杰克逊 = 377;
        public const int 鞭笞者特里高雷 = 71;
        public const int 魔像师卡扎库斯 = 155;
        public const int 麦迪文 = 35;


        public static readonly List<int> First = new List<int>
        {   //佣兵解锁装备的优先队列
            MercConst.冰雪之王洛克霍拉,
            MercConst.巴琳达_斯通赫尔斯,
            MercConst.魔像师卡扎库斯,
            MercConst.瓦莉拉_萨古纳尔,
            MercConst.变装大师,
            MercConst.海盗帕奇斯,
        };
        public static readonly List<int> SnowFire = new List<int>
        { 
			MercConst.巴琳达_斯通赫尔斯,
            MercConst.迦顿男爵,
            MercConst.拉格纳罗斯,
			MercConst.瓦尔登_晨拥,
            MercConst.冰雪之王洛克霍拉,
            MercConst.吉安娜_普罗德摩尔,
        };
        public static readonly List<int> FireKill = new List<int>
        { //5.7改动
			//火焰刺杀队
			MercConst.巴琳达_斯通赫尔斯,
            MercConst.迦顿男爵,
            MercConst.拉格纳罗斯,
			MercConst.瓦莉拉_萨古纳尔,
            MercConst.魔像师卡扎库斯,
            MercConst.变装大师
        };
        public static readonly List<int> SnowTeam = new List<int>
        { //5.7改动
            MercConst.巴琳达_斯通赫尔斯,
            MercConst.瓦尔登_晨拥,
            MercConst.冰雪之王洛克霍拉,
            MercConst.吉安娜_普罗德摩尔,
            MercConst.厨师曲奇,
            MercConst.魔像师卡扎库斯
        };
        public static readonly List<int> Piratesnake = new List<int>
        { //5.7改动
            MercConst.海盗帕奇斯,
            MercConst.鞭笞者特里高雷,
            MercConst.尤朵拉,
            MercConst.瓦莉拉_萨古纳尔,
            MercConst.变装大师,
            MercConst.重拳先生,
        };
        public static readonly List<int> Nature = new List<int>
        { //5.7改动
			//自然队
            MercConst.奈姆希_灵沼,
			MercConst.玛法里奥_怒风,
            MercConst.布鲁坎,
            MercConst.安娜科德拉,
            MercConst.厨师曲奇,
            MercConst.冰雪之王洛克霍拉,
        };
        public static readonly List<int> Origin0 = new List<int>
        { 
			//初始队伍
			MercConst.米尔豪斯_法力风暴,
            MercConst.泽瑞拉,
            MercConst.泰兰德_语风,
            MercConst.凯瑞尔_罗姆,
        };
        public static readonly List<int> Origin = new List<int>
        { 
			//初始队伍
            MercConst.泽瑞拉,
            MercConst.米尔豪斯_法力风暴,
            MercConst.剑圣萨穆罗,
            MercConst.泰兰德_语风,
            MercConst.凯瑞尔_罗姆,
        };
        public static readonly List<int> AOE = new List<int>
        { 
			//初始队伍
			MercConst.米尔豪斯_法力风暴,
            MercConst.冰雪之王洛克霍拉,
            MercConst.瓦尔登_晨拥
        };
        public static readonly List<int> Fire = new List<int>
        {
            //初级火焰队
			MercConst.安东尼达斯,
            MercConst.迦顿男爵,
            MercConst.拉格纳罗斯,
        };
        public static readonly List<int> cooperate = new List<int>
        {
            
            MercConst.变装大师,        MercConst.伊莉斯_逐星,   MercConst.瓦罗克_萨鲁法尔,
            MercConst.先知维伦,        MercConst.赤精,          MercConst.拉索利安,     
            MercConst.贝恩_血蹄,       MercConst.布莱恩_铜须,   MercConst.尤朵拉,              
            MercConst.穆克拉,          MercConst.玛诺洛斯,      MercConst.沃金,                    
            MercConst.提里奥_弗丁,     MercConst.光明之翼,               
            
        };
        public static readonly List<int> prior = new List<int>
        {
            MercConst.巴琳达_斯通赫尔斯,MercConst.海盗帕奇斯,     MercConst.奈姆希_灵沼,
            MercConst.迦顿男爵,         MercConst.鞭笞者特里高雷, MercConst.玛法里奥_怒风,
            MercConst.拉格纳罗斯,       MercConst.尤朵拉,         MercConst.布鲁坎,
            MercConst.瓦尔登_晨拥,      MercConst.瓦莉拉_萨古纳尔,MercConst.安娜科德拉,
            MercConst.冰雪之王洛克霍拉, MercConst.变装大师,       MercConst.厨师曲奇,
            MercConst.吉安娜_普罗德摩尔,MercConst.重拳先生,       MercConst.冰雪之王洛克霍拉,
        };

        public static readonly List<int> Piratesnake_MapID = new List<int>
        { //5.7改动
            221,
            259,260//H4-14
        };
        public static readonly List<int> FireKill_MapID = new List<int>
        { //5.7改动
            120,//奈法利安
            163,//H奥妮克希亚
        };
        public static readonly List<int> SnowTeam_MapID = new List<int>
        { //5.7改动
            268, //H克苏恩
        };
        public static readonly List<int> Nature_MapID = new List<int>
        { //5.7改动
            //贫瘠之地
            57,58,59,60,61,62,63,64,65,242,
            243,//H1-10
            //费伍德森林
            67,68,69,70,71,72,
            //冬泉谷
            73,74,75,76,77,78,
            //黑石山
            79,80,81,82,83,84,114,116,118,123,121,122,
            156,
            107,
            108,//H大帝
            126,//H4-11
            //奥特兰克      
            135,129,131, 128,132,144,146,141,140,
            151,//H5-10
			149,//H5-8
			150,//H5-9
			153, //米达
            164,//H6-6
			226,227,//麦迪文的残影
            //216,//H8-9
        };
        // Token: 0x0400002B RID: 43
        public static readonly List<int> Ignore = new List<int>
        {
            MercConst.厨师曲奇,
            MercConst.希奈丝特拉,
            MercConst.泰瑞尔
        };
        public static readonly Dictionary<int, int> MercEquipID = new Dictionary<int, int>()
        { //5.7改动
            { MercConst.巴琳达_斯通赫尔斯, EquipConst.次级水元素 },
            { MercConst.迦顿男爵, EquipConst.焚火印记 },
            { MercConst.拉格纳罗斯, EquipConst.炽烧符文 },
            { MercConst.瓦尔登_晨拥, EquipConst.冰风护符 },
            { MercConst.冰雪之王洛克霍拉, EquipConst.凝聚冰凌 },
            { MercConst.吉安娜_普罗德摩尔, EquipConst.寒冰屏障护身符 },
            { MercConst.厨师曲奇, EquipConst.开胃前菜 },
            { MercConst.安东尼达斯,EquipConst.烬核之杖},

            { MercConst.玛法里奥_怒风, EquipConst.活根草之杖 },
            { MercConst.古夫_符文图腾, EquipConst.土灵护腕 },
            { MercConst.布鲁坎, EquipConst.雷鸣系带 },
            { MercConst.安娜科德拉, EquipConst.剧毒毒液 },
            {MercConst.奈姆希_灵沼, EquipConst.沼泽苔藓},

            { MercConst.凯瑞尔_罗姆, EquipConst.黎明之锤 },
            { MercConst.泽瑞拉,EquipConst.强光魔杖 },
            { MercConst.剑圣萨穆罗,EquipConst.燃烧之刃 },
            { MercConst.米尔豪斯_法力风暴,  EquipConst.奥术粉尘 },
            { MercConst.泰兰德_语风,EquipConst.苍翠反曲弓 },

            { MercConst.瓦莉拉_萨古纳尔,EquipConst.异常烟尘 },
            { MercConst.魔像师卡扎库斯,EquipConst.野葡萄藤 },
            { MercConst.斯卡布斯_刀油,EquipConst.精磨之杖 },

            { MercConst.海盗帕奇斯,EquipConst.武器柜 },
            { MercConst.鞭笞者特里高雷,EquipConst.锋锐利爪 },
            { MercConst.尤朵拉,EquipConst.装填武器 },
            { MercConst.变装大师,EquipConst.拟态面具 },
            { MercConst.重拳先生,EquipConst.沉重铁锚 },
        };
    }

    internal class EquipConst
    {
        public const int 光明使者 = 0;
        public const int 微光肩甲 = 1;
        public const int 牺牲圣契 = 2;
        public const int 亚煞极印记 = 0;
        public const int 亚煞极之心 = 1;
        public const int 亚煞极神像 = 2;
        public const int 拳刃 = 0;
        public const int 埃辛诺斯战刃 = 1;
        public const int 恶魔斗篷 = 2;
        public const int 纳鲁之锤 = 0;
        public const int 智慧圣契 = 1;
        public const int 卡拉波之光 = 2;
        public const int 伊瑟拉的复仇 = 0;
        public const int 梦境能量 = 1;
        public const int 翡翠之威 = 2;
        public const int 利爪饰品 = 0;
        public const int 绿洲水壶 = 1;
        public const int 猴爪 = 2;
        public const int 激励秘典 = 0;
        public const int 受祝福的碎片 = 1;
        public const int 圣光药水 = 2;
        public const int 光明之翼的项链 = 0;
        public const int 妖精口袋 = 1;
        public const int 光明之翼的圆环 = 2;
        public const int 克苏恩之心 = 0;
        public const int 克苏恩之口 = 1;
        public const int 克苏恩之躯 = 2;
        public const int 凝聚冰凌 = 0;
        public const int 刺骨寒风 = 1;
        public const int 冰雪之王的遮蔽 = 2;
        public const int 迅捷图腾 = 0;
        public const int 雷霆饰带 = 1;
        public const int 重生 = 2;
        public const int 黎明之锤 = 0;
        public const int 裁决秘典 = 1;
        public const int 圣光秘典 = 2;
        public const int 幻觉饰带 = 0;
        public const int 磨砺之刃 = 1;
        public const int 燃烧之刃 = 2;
        public const int 霜狼之怒 = 0;
        public const int 德雷克塔尔的恩惠 = 1;
        public const int 震耳咆哮 = 2;
        public const int 决斗护手 = 0;
        public const int 玛诺洛斯的獠牙 = 1;
        public const int 兽人的旗帜 = 2;
        public const int 狂怒坠饰 = 0;
        public const int 实击护手 = 1;
        public const int 邪能核心 = 2;
        public const int 暗影步 = 0;
        public const int 半兽人的传承 = 1;
        public const int 犹疑之刃 = 2;
        public const int 焙烤培根 = 0;
        public const int 无情冰寒 = 1;
        public const int 埃提耶什的赐福 = 2;
        public const int 调味的锅 = 0;
        public const int 钢铁汤勺 = 1;
        public const int 开胃前菜 = 2;
        public const int 木乃伊面具 = 0;
        public const int 拟态面具 = 1;
        public const int 愤怒面具 = 2;
        public const int 盛开菌菇 = 0;
        public const int 木棘图腾 = 1;
        public const int 土灵护腕 = 2;
        public const int 玛诺洛斯之血 = 0;
        public const int 灵魂护符 = 1;
        public const int 混乱之杖 = 2;
        public const int 寒冰碎片 = 0;
        public const int 冰霜之尘 = 1;
        public const int 寒冰屏障护身符 = 2;
        public const int 鲜血之月 = 0;
        public const int 灵活变身 = 1;
        public const int 吉尔尼斯之力 = 2;
        public const int 暗影符文 = 0;
        public const int 虚空石 = 1;
        public const int 最终仪祭 = 2;
        public const int 高能枪弹 = 0;
        public const int 狩猎篷布 = 1;
        public const int 拉线操纵 = 2;
        public const int 沼泽苔藓 = 0;
        public const int 垂钓诱饵 = 1;
        public const int 新鲜桃子 = 2;
        public const int 实验对象 = 0;
        public const int 多彩龙军团 = 1;
        public const int 备用肢体 = 2;
        public const int 虾戮时间 = 0;
        public const int 又湿又滑 = 1;
        public const int 鱼人冲锋 = 2;
        public const int 锋刃之爪 = 0;
        public const int 更深的呼吸 = 1;
        public const int 巢母之怒 = 2;
        public const int 暗影之眼 = 0;
        public const int 神圣之眼 = 1;
        public const int 诺达希尔碎片 = 2;
        public const int 烬核之杖 = 0;
        public const int 烈焰饰环 = 1;
        public const int 灼热吊坠 = 2;
        public const int 剧毒毒液 = 0;
        public const int 狂野的徽记 = 1;
        public const int 毒蛇点心 = 2;
        public const int 祥和钟杵 = 0;
        public const int 疗愈长袍 = 1;
        public const int 纯洁指环 = 2;
        public const int 弹片射击 = 0;
        public const int 装填武器 = 1;
        public const int 咸水护腕 = 2;
        public const int 尖利凝视 = 0;
        public const int 狂乱抽笞 = 1;
        public const int 觉醒咆哮 = 2;
        public const int 巨型小鬼 = 0;
        public const int 易燃小鬼 = 1;
        public const int 恶魔护甲 = 2;
        public const int 霜之哀伤 = 0;
        public const int 虚空之踏 = 1;
        public const int 统御头盔 = 2;
        public const int 次级火元素 = 0;
        public const int 次级水元素 = 1;
        public const int 雷矛的拯救 = 2;
        public const int 奥丹姆遗物 = 0;
        public const int 冒险者的包裹 = 1;
        public const int 意气风发 = 2;
        public const int 雷鸣系带 = 0;
        public const int 霜木图腾 = 1;
        public const int 引雷针 = 2;
        public const int 弯钩利爪 = 0;
        public const int 法力胸针 = 1;
        public const int 哀伤碎片 = 2;
        public const int 灵魂缶 = 0;
        public const int 哀伤之剑 = 1;
        public const int 死亡箭雨 = 2;
        public const int 混乱护符 = 0;
        public const int 邪能之刃 = 1;
        public const int 恶魔卫士 = 2;
        public const int 冰川之刃 = 0;
        public const int 德雷克塔尔的法术书 = 1;
        public const int 微光草药水 = 2;
        public const int 异变之力 = 0;
        public const int 腐化的神经元 = 1;
        public const int 永恒折磨 = 2;
        public const int 恐惧奇美拉 = 0;
        public const int 黑龙鳞片 = 1;
        public const int 拉希奥的角 = 2;
        public const int 熔火之心 = 0;
        public const int 萨弗拉斯 = 1;
        public const int 炽烧符文 = 2;
        public const int 恶魔灰烬 = 0;
        public const int 灵魂宝石 = 1;
        public const int 恶魔印记 = 2;
        public const int 巨龙印记 = 0;
        public const int 巨龙之爪 = 1;
        public const int 巨龙符文斧 = 2;
        public const int 王者之盔 = 0;
        public const int 灰烬使者 = 1;
        public const int 提里奥的护盾 = 2;
        public const int 可靠搭档 = 0;
        public const int 预付定金 = 1;
        public const int 人人有嫌疑 = 2;
        public const int 军情七处合约 = 0;
        public const int 剥皮刀 = 1;
        public const int 精磨之杖 = 2;
        public const int 颅骨之尘 = 0;
        public const int 泰坦神铁锯刃 = 1;
        public const int 加装锯刃 = 2;
        public const int 新鲜的肉 = 0;
        public const int 岩质甲壳 = 1;
        public const int 烈焰利爪 = 2;
        public const int 血吼 = 0;
        public const int 休止饰带 = 1;
        public const int 饮血坠饰 = 2;
        public const int 燃烧射击 = 0;
        public const int 巨龙之颅 = 1;
        public const int 龙爪之拳 = 2;
        public const int 恐惧之刃 = 0;
        public const int 加装口袋 = 1;
        public const int 夺刀手 = 2;
        public const int 不死者之心 = 0;
        public const int 不朽者之毅 = 1;
        public const int 恶魔之魂 = 2;
        public const int 静滞海马 = 0;
        public const int 沃金战刃 = 1;
        public const int 急速指环 = 2;
        public const int 艾露恩的护符 = 0;
        public const int 苍翠反曲弓 = 1;
        public const int 狂野之戒 = 2;
        public const int 圣羽之辉 = 0;
        public const int 夺目护手 = 1;
        public const int 纯洁之冠 = 2;
        public const int 强光魔杖 = 0;
        public const int 纳鲁碎片 = 1;
        public const int 纯洁长袍 = 2;
        public const int 霜狼护身符 = 0;
        public const int 激励头盔 = 1;
        public const int 先祖护甲 = 2;
        public const int 冰冷出击 = 0;
        public const int 侍者新贵 = 1;
        public const int 迟缓之环 = 2;
        public const int 勾住船员 = 0;
        public const int 武器柜 = 1;
        public const int 轮到我了 = 2;
        public const int 深水诱饵 = 0;
        public const int 甲板破拆 = 1;
        public const int 踏潮饰针 = 2;
        public const int 卷轴珍藏 = 0;
        public const int 中立之心 = 1;
        public const int 周氏传家宝 = 2;
        public const int 出笼鳗鱼 = 0;
        public const int 鱼人大锅 = 1;
        public const int 锐利思维 = 2;
        public const int 督军帕杰什 = 0;
        public const int 紧握仇恨 = 1;
        public const int 蔑视 = 2;
        public const int 大宝剑 = 0;
        public const int 需要治疗 = 1;
        public const int 需求怪 = 2;
        public const int 玉火之矛 = 0;
        public const int 翔龙吊坠 = 1;
        public const int 玉珑骊珠 = 2;
        public const int 飞空之斧 = 0;
        public const int 招架之刃 = 1;
        public const int 领先三步 = 2;
        public const int 森林徽章 = 0;
        public const int 石南草 = 1;
        public const int 活根草之杖 = 2;
        public const int 充能战刃 = 0;
        public const int 看守之眼 = 1;
        public const int 守望者斗篷 = 2;
        public const int 深渊领主之杖 = 0;
        public const int 邪能尖刺 = 1;
        public const int 邪能魔肺 = 2;
        public const int 群星之瓶 = 0;
        public const int 昆莱之晶 = 1;
        public const int 爆裂新星指环 = 2;
        public const int 寒冰之握 = 0;
        public const int 女巫披风 = 1;
        public const int 邪能坩埚 = 2;
        public const int 寒冰药水 = 0;
        public const int 霜冻之戒 = 1;
        public const int 冰风护符 = 2;
        public const int 萨隆踏靴 = 0;
        public const int 狂战士之刃 = 1;
        public const int 锯齿盾牌 = 2;
        public const int 暗影匕首 = 0;
        public const int 隐秘踪迹 = 1;
        public const int 异常烟尘 = 2;
        public const int 萨拉迈恩 = 0;
        public const int 界限之靴 = 1;
        public const int 战争旗帜 = 2;
        public const int 玄牛之带 = 0;
        public const int 砮皂之盔 = 1;
        public const int 玄牛护符 = 2;
        public const int 摩戈尔的手套 = 0;
        public const int 尖刺泡泡鱼 = 1;
        public const int 润泽之杖 = 2;
        public const int 新鲜香蕉 = 0;
        public const int 辐射香蕉 = 1;
        public const int 穆克拉的大表哥 = 2;
        public const int 炫彩项链 = 0;
        public const int 珠光之鳞 = 1;
        public const int 土灵护甲 = 2;
        public const int 浮夸腰带 = 0;
        public const int 船铃 = 1;
        public const int 侦查望远镜 = 2;
        public const int 奥术粉尘 = 0;
        public const int 法力魔棒 = 1;
        public const int 魔网之杖 = 2;
        public const int 泡泡魔杖 = 0;
        public const int 导航员的护符 = 1;
        public const int 始生鱼人 = 2;
        public const int 猛击护手 = 0;
        public const int 启迪环带 = 1;
        public const int 黎明之盾 = 2;
        public const int 黑色船旗 = 0;
        public const int 轮番豪饮 = 1;
        public const int 公平分配 = 2;
        public const int 萨拉塔斯 = 0;
        public const int 高戈奈斯潮汐之石 = 1;
        public const int 莎拉达尔_女王权杖 = 2;
        public const int 威武战驹_麦克斯韦 = 0;
        public const int 探险眼镜 = 1;
        public const int 芬利的冒险头盔 = 2;
        public const int 暗影斗篷 = 0;
        public const int 坚定信念 = 1;
        public const int 谋生工具 = 2;
        public const int 无坚不摧之力 = 0;
        public const int 无法撼动之物 = 1;
        public const int 雷矛化身 = 2;
        public const int 华丽的军号 = 0;
        public const int 力量之戒 = 1;
        public const int 毁灭之锤 = 2;
        public const int 冰斧之力 = 0;
        public const int 荣誉束缚 = 1;
        public const int 永生不灭 = 2;
        public const int 图腾掌握 = 0;
        public const int 家传草药 = 1;
        public const int 酋长的羽毛 = 2;
        public const int 烈焰系带 = 0;
        public const int 赤精之杖 = 1;
        public const int 焰心水晶 = 2;
        public const int 熔岩之刃 = 0;
        public const int 焚火印记 = 1;
        public const int 燃烧护腕 = 2;
        public const int 熔岩魔角 = 0;
        public const int 恐怖利爪 = 1;
        public const int 黑暗灵魂石 = 2;
        public const int 竞技场选手 = 0;
        public const int 新客户 = 1;
        public const int 亡者之灵 = 2;
        public const int 水手帽 = 0;
        public const int 沉重铁锚 = 1;
        public const int 锋锐剑鞘 = 2;
        public const int 骷髅黑帆 = 0;
        public const int 船长的骄傲 = 1;
        public const int 随身刀具 = 2;
        public const int 奥术尖牙 = 0;
        public const int 法力符文 = 1;
        public const int 第十条尾巴 = 2;
        public const int 地狱火典籍 = 0;
        public const int 阿古斯诏令 = 1;
        public const int 阿克蒙德的金币 = 2;
        public const int 坚固背包 = 0;
        public const int 抑扬韵律 = 1;
        public const int 执刃着陆 = 2;
        public const int 阿莱克丝塔萨的胸针 = 0;
        public const int 巨龙军团护身符 = 1;
        public const int 迅捷坠饰 = 2;
        public const int 雪怒之矛 = 0;
        public const int 天神胸甲 = 1;
        public const int 滚雷护爪 = 2;
        public const int 迅羽之弓 = 0;
        public const int 猎手的步枪 = 1;
        public const int 熊妈妈之爪 = 2;
        public const int 探险帽 = 0;
        public const int 要发财了 = 1;
        public const int 宝物探员 = 2;
        public const int 注能琥珀 = 0;
        public const int 锋锐利爪 = 1;
        public const int 再生之鳞 = 2;
        public const int 魔皇草 = 0;
        public const int 皇血草 = 1;
        public const int 野葡萄藤 = 2;
        public const int 不稳定的符文 = 0;
        public const int 麦迪文的吊坠 = 1;
        public const int 法杖埃提耶什 = 2;
    }
}








































































































































































































































































































































































