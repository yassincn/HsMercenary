﻿using System;
using System.IO;
using System.Text;

namespace Mercenary
{
    // Token: 0x02000006 RID: 6
    public static class Out
    {
        // Token: 0x06000031 RID: 49 RVA: 0x0000417B File Offset: 0x0000237B
        public static void Log(string log, string Filename = "mercenarylog")
        {
            string errorLogFilePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Log");
            if (!Directory.Exists(errorLogFilePath))
            {
                Directory.CreateDirectory(errorLogFilePath);
            }
            string logFile = System.IO.Path.Combine(errorLogFilePath, Filename + "@" + DateTime.Today.ToString("yyyy-MM-dd") + ".log");
            bool writeBaseInfo = System.IO.File.Exists(logFile);
            StreamWriter swLogFile = new StreamWriter(logFile, true, Encoding.Unicode);
            swLogFile.WriteLine(DateTime.Now.ToString("HH:mm:ss") + "\t" + log);
            swLogFile.Close();
            swLogFile.Dispose();
        }

        // Token: 0x06000032 RID: 50 RVA: 0x0000417D File Offset: 0x0000237D
        public static void UI(string log)
        {
            UIStatus.Get().AddInfo(log);
        }
    }
    public static class TeamOut//showinfo佣兵阵容
    {
        public static QueueList<string> ReadFile()
        {
            QueueList<string> allTeamLog = new QueueList<string>();
            //string LogFilePath = HsMod.PluginConfig.hsMatchLogPath.Value;
            string LogFilePath = HsMod.ConfigValue.Get().HsMatchLogPathValue;
            //string LogFilePath = @"BepInEx\merc.log";
            if (!File.Exists(LogFilePath))
            {
                // Create a file to write to.
                File.CreateText(LogFilePath);
                return allTeamLog;
            }
            using (StreamReader sr = File.OpenText(LogFilePath))
            {
                string s;
                while ((s = sr.ReadLine()) != null)
                {
                    allTeamLog.Enqueue(s);
                }
                sr.Close();
                sr.Dispose();
            }

            return allTeamLog;
        }
        public static void WriteFile(QueueList<string> allTeamLog, string log)
        {
            string LogFilePath = HsMod.ConfigValue.Get().HsMatchLogPathValue;
            //string LogFilePath = HsMod.PluginConfig.hsMatchLogPath.Value;
            string newLog = DateTime.Today.ToString("yyyy-MM-dd ") + DateTime.Now.ToString("HH:mm:ss") + "," + log;
            allTeamLog.Enqueue(newLog);
            while (allTeamLog.Count > 500)
                allTeamLog.Dequeue();
            using (FileStream fs = File.Open(LogFilePath, FileMode.Create))
            {
                foreach (string s in allTeamLog)
                {
                    AddText(fs, s + '\n');
                }
                fs.Close();
                fs.Dispose();
            }
        }
        private static void AddText(FileStream fs, string value)
        {
            byte[] info = new UTF8Encoding(true).GetBytes(value);
            fs.Write(info, 0, info.Length);
        }
        // Token: 0x06000032 RID: 50 RVA: 0x0000417D File Offset: 0x0000237D
        public static void UI(string log)
        {
            UIStatus.Get().AddInfo(log);
        }
    }
}
