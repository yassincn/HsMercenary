﻿namespace Mercenary
{
    // Token: 0x02000003 RID: 3
    internal static class Mode
    {
        // Token: 0x04000003 RID: 3
        public const string 刷图 = "刷图";

        // Token: 0x04000004 RID: 4
        public const string 神秘人 = "神秘人";

        // Token: 0x04000005 RID: 5
        public const string 佣兵任务 = "佣兵任务";

        // Token: 0x04000006 RID: 6
        public const string 解锁装备 = "解锁装备";

        public const string 主线任务 = "主线任务";

        public const string 一条龙 = "一条龙";

        public const string 挂机收菜 = "挂机收菜";

        // Token: 0x04000007 RID: 7
        public const string PVP = "PVP";
    }
}
