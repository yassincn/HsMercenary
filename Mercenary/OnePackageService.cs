﻿using Assets;
using Hearthstone.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mercenary
{
    public static class OnePackageService
    {
        public static int GetsTheCurrentStage()
        {
            int stage = 0;
            //target1：初始队伍，凯瑞尔，豪斯，泰兰德，泽瑞拉，4个人，是否全部30级
            //一旦有一个未30级
            //返回0 刷图1-1 
            try
            {
                foreach (int ID in MercConst.Origin0)
                {
                    LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
                    if (!mercenary.IsReadyForCrafting()
                        && mercenary.m_owned
                        && !mercenary.IsMaxLevel())
                    {
                        return stage;
                    }
                }
                stage++;

                //target2：剑圣是否拥有
                //如果未拥有返回1 自动主线
                LettuceMercenary mercenary_temp = HsGameUtils.GetMercenary(MercConst.剑圣萨穆罗);
                if (!mercenary_temp.m_owned)
                    return stage;
                stage++;

                //target3: 初始队伍，豪斯，泽瑞拉，剑圣，泰兰德，凯瑞尔， 5个人，是否30级
                //返回2 刷图H1-1
                foreach (int ID in MercConst.Origin)
                {
                    LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
                    if (!mercenary.IsReadyForCrafting()
                        && mercenary.m_owned
                        && !mercenary.IsMaxLevel())
                    {
                        return stage;
                    }
                }
                stage++;

                //target4：2-6 是否解锁
                //返回3 自动主线
                if (false == MercenariesDataUtil.IsBountyComplete(72))
                    return stage;
                stage++;

                //target5: AOE初级队，是否获得，雪王，晨拥，米尔豪斯
                //返回4 2-6  初始队 神秘人 
                foreach (int ID in MercConst.AOE)
                {
                    LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
                    if (!mercenary.m_owned)
                    {
                        return stage;
                    }
                }
                stage++;

                //target6: AOE初级队， 是否碎片够满级  雪王，晨拥，米尔豪斯
                //返回5 H1-1 刷图
                foreach (int ID in MercConst.AOE)
                {
                    LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
                    if (Main.CalcMercenaryCoinNeed(mercenary) > 2000 && Main.CalcMercenaryCoinNeed(mercenary) != 8192)
                    {
                        return stage;
                    }
                }
                stage++;
                //target7: 大德是否获得 
                //返回6 刷图 2-3 
                mercenary_temp = HsGameUtils.GetMercenary(MercConst.玛法里奥_怒风);
                if (!mercenary_temp.m_owned)
                    return stage;
                stage++;

                //target8: 自然队是否全拥有
                //返回7 神秘人
                foreach (int ID in MercConst.Nature)
                {
                    LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
                    if (!mercenary.m_owned)
                    {
                        return stage;
                    }
                }
                stage++;

                //target9:玛法里奥是否获得了 装备3
                ////返回8 刷图H1-1
                LettuceAbility lettuceAbility = mercenary_temp.m_equipmentList[2];
                if (!lettuceAbility.Owned)
                    return stage;
                stage++;

                //target10：自然队是否全满级
                //返回9 刷图H1-1
                int sum = 0;
                foreach (int ID in MercConst.Nature)
                {
                    LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
                    if (Main.CalcMercenaryCoinNeed(mercenary) > 1000 && Main.CalcMercenaryCoinNeed(mercenary) != 8192)
                    {
                        sum++;
                    }
                }
                if (sum > 1)
                    return stage;
                stage++;

                //target11：4 - 1 是否解锁
                //返回10 自动主线
                if (false == MercenariesDataUtil.IsBountyComplete(78))
                {
                    return stage;
                }
                stage++;

                //target12： 是否有初级火焰队
                //返回11 刷图
                foreach (int ID in MercConst.Fire)
                {
                    LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
                    if (!mercenary.m_owned)
                    {
                        return stage;
                    }
                }
                stage++;

                //target13： 初级火焰队是否碎片足够满级
                //返回12 刷图 H1-1
                foreach (int ID in MercConst.Fire)
                {
                    LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
                    if (mercenary.m_owned &&
                        Main.CalcMercenaryCoinNeed(mercenary) > 1000 && Main.CalcMercenaryCoinNeed(mercenary) != 8192)
                    {
                        return stage;
                    }
                }
                stage++;

                //target14：配合紫色佣兵是否集齐
                //返回13 刷神秘人
                sum = 0;
                foreach (int ID in MercConst.cooperate)
                {
                    LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
                    if (!mercenary.m_owned)
                    {
                        //Out.Log($"{mercenary.m_mercName}未获得");
                        sum++;
                    }
                }
                if (sum > 8)
                    return stage;
                stage++;
                //target15： 当前的佣兵任务栏，是不是空的
                //返回14 佣兵任务
                //TaskUtils.UpdateTask();
                //Out.Log($"{TaskUtils.GetTasks().Count}");

                NetCache.NetCacheMercenariesVillageVisitorInfo netObject =
                    NetCache.Get().GetNetObject<NetCache.NetCacheMercenariesVillageVisitorInfo>();
                // 因为新任务是添加在第一条，所以倒序做任务，先进先做
                for (int i = netObject.VisitorStates.Count - 1; i >= 0; --i)
                // 			foreach (MercenariesVisitorState mercenariesVisitorState in netObject.VisitorStates)
                {
                    MercenaryVillageTaskItemDataModel mercenaryVillageTaskItemDataModel =
                        LettuceVillageDataUtil.CreateTaskModelByTaskState(netObject.VisitorStates[i].ActiveTaskState, null, false, false);
                    VisitorTaskDbfRecord taskRecordByID = LettuceVillageDataUtil.GetTaskRecordByID(netObject.VisitorStates[i].ActiveTaskState.TaskId);

                    if (mercenaryVillageTaskItemDataModel.TaskType == MercenaryVisitor.VillageVisitorType.STANDARD)
                    {
                        return stage;
                    }
                }

                stage++;
                //target16：预设卡组的所有佣兵是否获得
                //返回15 刷神秘人
                foreach (int ID in MercConst.SnowFire)
                {
                    LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
                    if (!mercenary.m_owned)
                    {
                        return stage;
                    }
                }
                foreach (int ID in MercConst.Nature)
                {
                    LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
                    if (!mercenary.m_owned)
                    {
                        return stage;
                    }
                }
                foreach (int ID in MercConst.FireKill)
                {
                    LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
                    if (!mercenary.m_owned)
                    {
                        return stage;
                    }
                }
                foreach (int ID in MercConst.SnowTeam)
                {
                    LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
                    if (!mercenary.m_owned)
                    {
                        return stage;
                    }
                }
                stage++;
                //target17：预设卡组的所有佣兵是否全部碎片够+1+5
                //返回16 刷图H1-1
                foreach (int ID in MercConst.SnowFire)
                {
                    LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
                    if (!mercenary.m_owned || Main.CalcMercenaryCoinNeed(mercenary) != 8192)
                    {
                        return stage;
                    }
                }
                foreach (int ID in MercConst.Nature)
                {
                    LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
                    if (!mercenary.m_owned || Main.CalcMercenaryCoinNeed(mercenary) != 8192)
                    {
                        return stage;
                    }
                }
                foreach (int ID in MercConst.FireKill)
                {
                    LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
                    if (!mercenary.m_owned || Main.CalcMercenaryCoinNeed(mercenary) != 8192)
                    {
                        return stage;
                    }
                }
                foreach (int ID in MercConst.SnowTeam)
                {
                    LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
                    if (!mercenary.m_owned || Main.CalcMercenaryCoinNeed(mercenary) != 8192)
                    {
                        return stage;
                    }
                }
                stage++;

                //target18: 冰火队的两个解锁装备的地图是否解锁128
                //返回17 自动主线
                if (false == MercenariesDataUtil.IsBountyComplete(128))
                {

                    return stage;
                }
                stage++;

                //target19：巴琳达 雪王 是否解锁装备
                //返回18 解锁装备
                mercenary_temp = HsGameUtils.GetMercenary(MercConst.冰雪之王洛克霍拉);
                lettuceAbility = mercenary_temp.m_equipmentList[0];
                if (!lettuceAbility.Owned)
                    return stage;
                mercenary_temp = HsGameUtils.GetMercenary(MercConst.巴琳达_斯通赫尔斯);
                lettuceAbility = mercenary_temp.m_equipmentList[1];
                if (!lettuceAbility.Owned)
                    return stage;

                stage++;

                //target20：是否全佣兵
                //返回19 刷神秘人
                List<LettuceMercenary> mercenaries = CollectionManager.Get().
                    FindOrderedMercenaries(null, null, null, null, null).m_mercenaries;
                foreach (LettuceMercenary mercenary in mercenaries)
                {
                    if (!mercenary.m_owned && !mercenary.IsReadyForCrafting())
                    {
                        Out.Log($"[阶段20] 佣兵 {mercenary.m_mercName} /未制作");
                        return stage;
                    }
                }
                stage++;

                //target21：是否完成主线任务
                //返回20 主线任务
                if (HsGameUtils.GetMainLineTask().Count > 0)
                {
                    return stage;
                }
                stage++;

                //target22：是否有佣兵未解锁装备
                //返回21
                if (Main.InitMap() == true)
                    return stage;
                stage++;
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            //以上全部完成返回22
            //0核心刷图H1-1
            return stage;
        }
    }
}
