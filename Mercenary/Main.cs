﻿using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using Hearthstone.DataModels;
using Hearthstone.Progression;
using HsMercenaryStrategy;
using PegasusLettuce;
using PegasusShared;
using PegasusUtil;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
namespace Mercenary
{
	// Token: 0x02000004 RID: 4
	[BepInPlugin("io.github.jimowushuang.hs", "免费佣兵插件 Pro", "1.3.9")]
	public class Main : BaseUnityPlugin
	{
		// Token: 0x06000005 RID: 5 RVA: 0x00002274 File Offset: 0x00000474
		private void OnGUI()
		{
			if (!Main.isRunning)
			{
				return;
			}
			GUIStyle textStyle = new GUIStyle();
			textStyle.normal.textColor = Color.white;
			textStyle.fontSize = 12;
			GUILayout.Label(new GUIContent("免费佣兵插件 Pro 1.3.9" + "\t\t" +OPSLable + PvpRate + '\n' + PvpLable + ExpLable), textStyle, new GUILayoutOption[]
			{
				GUILayout.Width(300f)
			});
		}

		// Token: 0x06000006 RID: 6 RVA: 0x000022D0 File Offset: 0x000004D0
		private void Awake()
		{
			Main.isPluginEnable = base.Config.Bind<bool>("配置", "插件启用", true, new ConfigDescription("插件是否启用（修改该配置需要重启）", null, new object[]
			{
				"Advanced"
			}));
			Main.isStrategyEnable = base.Config.Bind<bool>("配置", "日志输出策略", true, new ConfigDescription("日志输出策略", null, new object[]
			{
				"Advanced"
			}));

			if (!Main.isPluginEnable.Value) return;

			this._harmony.PatchAll(typeof(Main));
			Main.runningConf = base.Config.Bind<bool>("配置", "插件开关", true, new ConfigDescription("插件开关", null, new object[]
			{
				"Advanced"
			}));
			Main.modeConf = base.Config.Bind<string>("配置", "插件运行模式", "刷图", new ConfigDescription("插件运行模式", new AcceptableValueList<string>(new string[]
			{
				Mode.刷图,
				Mode.神秘人,
				Mode.佣兵任务,
				Mode.主线任务,
				Mode.解锁装备,
				Mode.一条龙,
				Mode.PVP,
				Mode.挂机收菜
			}), Array.Empty<object>()));
			Main.teamNameConf = base.Config.Bind<string>("配置", "使用的队伍名称", "PVE", "使用的队伍名称");
			Main.strategyConf = base.Config.Bind<string>("配置", "战斗策略", PveNormal.StrategyName, 
				new ConfigDescription("使用的策略,注意只有在非全自动化模式下才会生效", 
				new AcceptableValueList<string>(StrategyHelper.GetAllStrategiesName().ToArray()), Array.Empty<object>()));
			Main.mapConf = base.Config.Bind<string>(new ConfigDefinition("配置Pve", "要刷的地图"), "1-1", 
				new ConfigDescription("要刷的地图", new AcceptableValueList<string>(MapUtils.GetMapNameList()), Array.Empty<object>()));
			Main.autoTimeGearConf = base.Config.Bind<bool>("配置", "是否自动齿轮", false, "是否在Pve战斗中使用齿轮，战斗中自动4倍速，并且启用快速战斗");
			
			Main.autoUpdateSkillConf = base.Config.Bind<bool>("配置Pve", "是否自动升级技能", true, "是否自动升级技能");
			Main.autoCraftConf = base.Config.Bind<bool>("配置Pve", "是否自动制作佣兵", true, "是否自动制作佣兵");
			Main.autoAckConf = base.Config.Bind<bool>("配置Pve", "是否领取成就装备", false, "是否领取成就装备");
			Main.teamNumConf = base.Config.Bind<int>("配置Pve", "总队伍人数", 6, 
				new ConfigDescription("总队伍人数（PVE下生效）", new AcceptableValueRange<int>(1, 6), Array.Empty<object>()));
			Main.coreTeamNumConf = base.Config.Bind<int>("配置Pve", "队伍核心人数", 6,
				new ConfigDescription("前n个佣兵不会被自动换掉（PVE下生效）", new AcceptableValueRange<int>(0, 6), Array.Empty<object>()));
			Main.cleanTaskConf = base.Config.Bind<string>(new ConfigDefinition("配置Pve", "自动清理任务时间"), "不开启", 
				new ConfigDescription("会定时清理长时间没完成的任务（全自动模式生效）",
				new AcceptableValueList<string>(new List<string>(TaskUtils.CleanConf.Keys).ToArray()), Array.Empty<object>()));
			Main.awakeTimeConf = base.Config.Bind<string>("配置", "唤醒时间", "1999/1/1 0:0:0",
				new ConfigDescription("唤醒时间", null, new object[]
            {
                "Advanced"
            }));
			Main.awakeTimeIntervalConf = base.Config.Bind<int>("配置", "唤醒时间间隔(min)", 22, new ConfigDescription("唤醒时间间隔", null, new object[]
            {
                "Advanced"
            }));
			Main.delayTime = base.Config.Bind<int>("配置", "登场烧绳间隔", 10, "每个佣兵上场之间时间间隔");
			Main.autoConcede = base.Config.Bind<bool>("配置Pvp", "是否自动投降", false, "开局秒投");
			Main.PvpRandomTeam = base.Config.Bind<bool>("配置Pvp", "是否随机换队", false, "每次随机选择一个队伍");
		}

		// Token: 0x06000007 RID: 7 RVA: 0x00002513 File Offset: 0x00000713
		private void Start()
		{
			if (!Main.isPluginEnable.Value) return;
			Main.runningConf.SettingChanged += delegate
			{
				if (Main.runningConf != null && Main.runningConf.Value == true)
				{
					HsMod.ConfigValue.Get().ConfigTemplateValue = HsMod.Utils.ConfigTemplate.AwayFromKeyboard;
				}
				else if (Main.runningConf != null && Main.runningConf.Value == false)
				{
					HsMod.ConfigValue.Get().ConfigTemplateValue = HsMod.Utils.ConfigTemplate.AntiAwayFromKeyboard;
				}
				else
				{
					HsMod.ConfigValue.Get().ConfigTemplateValue = HsMod.Utils.ConfigTemplate.AntiAwayFromKeyboard;
				}
			};
			if (Main.autoTimeGearConf.Value == true)
			{
				HsMod.ConfigValue.Get().IsQuickModeEnableValue = true;
				HsMod.ConfigValue.Get().TimeGearEnable = true;	
			}
				
			Main.autoTimeGearConf.SettingChanged += delegate
			{
				if (Main.autoTimeGearConf != null && Main.autoTimeGearConf.Value == true)
				{
					HsMod.ConfigValue.Get().TimeGearEnable = true;
					HsMod.ConfigValue.Get().IsQuickModeEnableValue = true;
					HsMod.ConfigValue.Get().TimeGearValue = 2;
				}
				else
				{
					HsMod.ConfigValue.Get().TimeGearValue = 1;
					HsMod.ConfigValue.Get().IsQuickModeEnableValue = false;
					HsMod.ConfigValue.Get().TimeGearEnable = false;
				}
			};

			Out.Log("启动");
			Main.isRunning = Main.runningConf.Value;
			if (Main.isRunning)
            {
				HsMod.ConfigValue.Get().ConfigTemplateValue = HsMod.Utils.ConfigTemplate.AwayFromKeyboard;
				HsMod.ConfigValue.Get().TimeGearValue = 2;
			}
			else
			{
				HsMod.ConfigValue.Get().ConfigTemplateValue = HsMod.Utils.ConfigTemplate.AntiAwayFromKeyboard;
				HsMod.ConfigValue.Get().TimeGearValue = 1;
			}
			

		}
		// Token: 0x06000009 RID: 9 RVA: 0x00002552 File Offset: 0x00000752
		[HarmonyPrefix]//拦截来访者画面
		[HarmonyPatch(typeof(LettuceMapDisplay), "OnVisitorSelectionResponseReceived")]
		public static bool _PreOnVisitorSelectionResponseReceived()
		{

			if (!Main.isRunning)
			{
				return true;
			}
			Network.Get().GetMercenariesMapVisitorSelectionResponse();
			return false;
		}
		// Token: 0x0600000C RID: 12 RVA: 0x00002575 File Offset: 0x00000775
		[HarmonyPostfix]
		[HarmonyPatch(typeof(LettuceMissionEntity), "ShiftPlayZoneForGamePhase")]
		public static void _PostShiftPlayZoneForGamePhase(int phase)
		{
			//游戏阶段
			Main.phaseID = phase;
		}

		//// Token: 0x0600000F RID: 15 RVA: 0x000025BD File Offset: 0x000007BD
		//[HarmonyPostfix]//拦截错误提示
		//[HarmonyPatch(typeof(AlertPopup), "Show")]
		//public static void _PreAlertPopupShow()
		//{
		//
		//	Out.Log("AlertPopup.Show");
		//}

		// Token: 0x06000010 RID: 16 RVA: 0x000025D1 File Offset: 0x000007D1
		[HarmonyPostfix] // 拦截神秘人选择
		[HarmonyPatch(typeof(LettuceMapDisplay), "ShouldShowVisitorSelection")]
		public static void PostShouldShowVisitorSelection(PegasusLettuce.LettuceMap map, ref bool __result)
		{
			if (!Main.isRunning || map == null)
			{
				return;
			}
			__result = false;
			if (map.HasPendingVisitorSelection && map.PendingVisitorSelection.VisitorOptions.Count > 0)
			{
                foreach (var iter in map.PendingVisitorSelection.VisitorOptions)
                {
                    int num = 0;
                    if (iter.HasVisitorId)
                    {
                        num = LettuceVillageDataUtil.GetMercenaryIdForVisitor(GameDbf.MercenaryVisitor.GetRecord(iter.VisitorId), null);
                        Out.Log($"[来访者选择] VisitorId");
                    }
                    if (num == 0 && iter.HasFallbackMercenaryId)
                    {
                        num = iter.FallbackMercenaryId;
                        Out.Log($"[来访者选择] FallbackMercenaryId");
                    }
                    global::LettuceMercenary mercenary = CollectionManager.Get().GetMercenary((long)num, false, true);
                    int taskChainIndex = GameDbf.GetIndex().GetTaskChainIndexForTask(iter.TaskId);
                    Out.Log($"[来访者选择] {mercenary.m_mercName} index:{taskChainIndex}");
                }

                Out.Log(string.Format("[来访者拦截] 选择第一个来访者"));
                Network.Get().MakeMercenariesMapVisitorSelection(0);
            }
		}

		// Token: 0x06000011 RID: 17 RVA: 0x00002607 File Offset: 0x00000807
		[HarmonyPrefix] //"弹出揭示卡"
		[HarmonyPatch(typeof(LettuceMapDisplay), "DisplayNewlyGrantedAnomalyCards")]
		public static bool _PreDisplayNewlyGrantedAnomalyCards(global::LettuceMap lettuceMap, int completedNodeId)
		{

			//Out.Log("_PreDisplayNewlyGrantedAnomalyCards");
			return !Main.isRunning;
		}

		// Token: 0x06000012 RID: 18 RVA: 0x0000261C File Offset: 0x0000081C
		[HarmonyPostfix]
		[HarmonyPatch(typeof(global::LettuceMap), "CreateMapFromProto")]
		public static void _PostCreateMapFromProto(PegasusLettuce.LettuceMap lettuceMap)
		{
			Out.Log("[地图信息识别]");
			if (!Main.isRunning || lettuceMap == null)
			{
				return;
			}
			if (Main.modeConf.Value == Mode.PVP)
			{
				Out.Log(string.Format("[地图信息识别] Pvp模式，回到村庄"));
				HsGameUtils.GotoSceneVillage();
				return;
			}
			NetCache.NetCacheMercenariesVillageVisitorInfo netObject = NetCache.Get().GetNetObject<NetCache.NetCacheMercenariesVillageVisitorInfo>();
			if (netObject != null)
			{
				foreach (MercenariesVisitorState mercenariesVisitorState in netObject.VisitorStates)
				{
					if (mercenariesVisitorState.ActiveTaskState.Status_ == MercenariesTaskState.Status.COMPLETE)
					{
						Out.Log(string.Format("[地图信息识别] [TID:{0}]完成", mercenariesVisitorState.ActiveTaskState.TaskId));
						Network.Get().ClaimMercenaryTask(mercenariesVisitorState.ActiveTaskState.TaskId);
					}
				}
			}
			foreach (LettuceMapNode lettuceMapNode in lettuceMap.Nodes)
			{
				if (GameUtils.IsFinalBossNodeType((int)lettuceMapNode.NodeTypeId) && lettuceMapNode.NodeState_ == LettuceMapNode.NodeState.COMPLETE)
				{
					Out.Log(string.Format("[地图信息识别] 回到悬赏面板"));
					SceneMgr.Get().SetNextMode(SceneMgr.Mode.LETTUCE_BOUNTY_BOARD, SceneMgr.TransitionHandlerType.NEXT_SCENE, null, null);
					return;
				}
			}
			if (Main.IsTreasure(lettuceMap))
			{
				//5.7改动
				string[] findTreasure =
				{
					//T0
					"刺杀","强化飞刺","冷酷严冬","自然之噬","雷暴之怒","强化闪电箭",
					"洄梦仙酒","月之祝福","自然之杖","元素研究","火焰之杖",
					"火炮轰击","宝箱","灵魂虹吸","吸取灵魂",
					//T1
					"火球齐射","蔓延炸弹","便携冰墙","冰霜之杖","冰霜齐射",
					
					//T2
					"火舌图腾","元素之力","精灵旗帜","冰霜之环",
					"部落的旗帜","联盟战争旗帜","暴风城战袍","血之契印","奥格瑞玛战袍",
					"隐蔽武器","近在眼前",
					//T3
					"毒蛇印记","负向平衡","正向平衡",
					"心能抗原",
					"不许摸","药膏瓶","强韧","萨隆邪铁护甲","防护之戒","火炮弹幕","乔丹法杖"

				};
				List<string> treasureList = new List<string>();
				foreach (int dbId in lettuceMap.PendingTreasureSelection.TreasureOptions)
				{
					string cardId = GameUtils.TranslateDbIdToCardId(dbId, false);
					string name = DefLoader.Get()?.GetEntityDef(cardId)?.GetName();
					Out.Log($"[宝藏选择] {name}");
					if (name.Length > 0 &&
						Char.IsNumber(name[name.Length - 1]))
						name = name.Substring(0, name.Length - 1);
					treasureList.Add(name);
				}

				int findIndex = -1;
				foreach (var iter in findTreasure)
				{
					findIndex = treasureList.IndexOf(iter);
					if (findIndex != -1)
						break;
				}
				Out.Log($"[宝藏选择识别] 选择第{findIndex+1}个宝藏\n");
				Network.Get().MakeMercenariesMapTreasureSelection(Math.Max(0, findIndex));
			}
			if (Main.IsVisitor(lettuceMap))
			{
				Network.Get().MakeMercenariesMapVisitorSelection(0);
			}
		}

		// Token: 0x06000014 RID: 20 RVA: 0x000028BC File Offset: 0x00000ABC
		[HarmonyPostfix]
		[HarmonyPatch(typeof(LettuceMapDisplay), "TryAutoNextSelectCoin")]
		public static void _PostTryAutoNextSelectCoin(LettuceMapDisplay __instance)
		{
			Out.Log("[节点选择]");
			if (!Main.isRunning)
			{
				return;
			}
			LettuceMap map = (global::LettuceMap)Traverse.Create(__instance).Field("m_lettuceMap").GetValue();

			Main.ResetIdle();
			if (Main.modeConf.Value == Mode.佣兵任务)
			{
				TaskUtils.UpdateTask();
			}
			Main.SelectNextNode(map);
		}
		[HarmonyPostfix]
		[HarmonyPatch(typeof(LettuceMapDisplay), "OnLettuceMapChooseNodeResponseReceived")]
		public static void _PostOnLettuceMapChooseNodeResponseReceived()
		{
			LettuceMapChooseNodeResponse lettuceMapChooseNodeResponse = Network.Get().GetLettuceMapChooseNodeResponse();
			if (lettuceMapChooseNodeResponse == null ||
				!lettuceMapChooseNodeResponse.Success)
			{
				Out.Log(string.Format("[节点选择] 应答失败，返回村庄重试"));
				Main.Sleep(3);
				HsGameUtils.GotoSceneVillage();
				return;
			}
		}

		[HarmonyPostfix] //PVP结算统计
		[HarmonyPatch(typeof(GameEntity), "ShowEndGameScreen")]
		public static void PatchEndGameScreenShow(ref TAG_PLAYSTATE playState)
		{
			try
			{
				switch (playState)
				{
					case TAG_PLAYSTATE.WINNING:
					case TAG_PLAYSTATE.WON:
						++PvpWin;
						break;
					case TAG_PLAYSTATE.CONCEDED:
					case TAG_PLAYSTATE.LOST:
					case TAG_PLAYSTATE.LOSING:
						++PvpLose;
						break;
					case TAG_PLAYSTATE.TIED:
						break;
					default:
						break;
				}
			}
			catch (Exception ex)
			{
				Out.Log("空间名：" + ex.Source + "；" + '\n' +
							  "方法名：" + ex.TargetSite + '\n' +
							  "故障点：" + ex.StackTrace.Substring(ex.StackTrace.LastIndexOf("\\") + 1, ex.StackTrace.Length - ex.StackTrace.LastIndexOf("\\") - 1) + '\n' +
							  "错误提示：" + ex.Message);

			}
		}
		// Token: 0x06000019 RID: 25 RVA: 0x00002A01 File Offset: 0x00000C01


		private static bool IsTreasure(PegasusLettuce.LettuceMap map)
		{
			return map.HasPendingTreasureSelection && map.PendingTreasureSelection.TreasureOptions.Count > 0;
		}

		// Token: 0x0600001A RID: 26 RVA: 0x00002A20 File Offset: 0x00000C20
		private static bool IsVisitor(PegasusLettuce.LettuceMap map)
		{
			return map.HasPendingVisitorSelection && map.PendingVisitorSelection.VisitorOptions.Count > 0;
		}

		// Token: 0x0600001B RID: 27 RVA: 0x00002A40 File Offset: 0x00000C40
		private void AutoChangeTeam()
		{
			Out.Log("[队伍编辑]");

			global::LettuceTeam lettuceTeam = HsGameUtils.GetAllTeams().Find((global::LettuceTeam t) => t.Name.Equals(Main.teamNameConf.Value));
			if (lettuceTeam == null)
			{
				return;
			}
			List<LettuceMercenary> mercs = lettuceTeam.GetMercs();
			List<int> list = new List<int>();
			int num = 0;

			foreach (LettuceMercenary lettuceMercenary in mercs)
			{
				if (num < Main.coreTeamNumConf.Value)
				{
					num++;
				}
				else
				{
					list.Add(lettuceMercenary.ID);
				}
			}
			foreach (int mercId in list)
			{
				lettuceTeam.RemoveMerc(mercId);
			}
			if (lettuceTeam.GetMercCount() < Main.teamNumConf.Value)
			{
                //考虑全自动模式
                if (Main.modeConf.Value == Mode.佣兵任务)
				{
					foreach (Task task in TaskUtils.GetTasks())
					{
						foreach (MercenaryEntity mercenaryEntity in task.Mercenaries)
						{
							LettuceMercenary mercenary = CollectionManager.Get().GetMercenary((long)mercenaryEntity.ID, false, true);
							if (mercenary != null && mercenary.m_owned && !lettuceTeam.IsMercInTeam(mercenaryEntity.ID, true))
							{
								HsGameUtils.UpdateEq(mercenaryEntity.ID, mercenaryEntity.Equipment);
								lettuceTeam.AddMerc(mercenary, -1, null);
								Out.Log(string.Format("[队伍编辑] 添加[{0}][装备iD:{1}]，因为佣兵任务",
									mercenary.m_mercName, mercenaryEntity.Equipment));
								if (lettuceTeam.GetMercCount() == Main.teamNumConf.Value)
								{
									break;
								}
							}
						}
						if (lettuceTeam.GetMercCount() == Main.teamNumConf.Value)
						{
							break;
						}
					}
				}
				List<LettuceMercenary> mercenaries = CollectionManager.Get().FindOrderedMercenaries(null, new bool?(true), null, null, null).m_mercenaries;

				if (Main.modeConf.Value == Mode.解锁装备 || Main.modeConf.Value == Mode.主线任务)
				{   //5.7改动
					Out.Log("[队伍编辑] 解锁装备/主线任务");
					InitMap();
					int mapId = GetMapId();
					if (Main.coreTeamNumConf.Value == 0) // 核心人数 = 0 ，玩家使用预设卡组，
					{
						// 自然队
						if (MercConst.Nature_MapID.Contains(mapId))
						{
							foreach (int num2 in MercConst.Nature)
							{
								HsGameUtils.UpdateEq(num2, MercConst.MercEquipID[num2]);
								LettuceMercenary mercenary2 = HsGameUtils.GetMercenary(num2);
								if (!lettuceTeam.IsMercInTeam(num2, true)
									&& !mercenary2.IsReadyForCrafting()
									&& mercenary2.m_owned)
								{
									lettuceTeam.AddMerc(mercenary2, -1, null);
									Out.Log(string.Format("[队伍编辑] 添加[{0}]，解锁装备/主线任务",
										mercenary2.m_mercName));
									if (lettuceTeam.GetMercCount() == 5 && Main.modeConf.Value == Mode.解锁装备)
									{
										break;
									}
								}
							}
						}
						// 海盗蛇
						else if (MercConst.Piratesnake_MapID.Contains(mapId))
						{
							foreach (int num2 in MercConst.Piratesnake)
							{
								HsGameUtils.UpdateEq(num2, MercConst.MercEquipID[num2]);
								LettuceMercenary mercenary2 = HsGameUtils.GetMercenary(num2);
								if (!lettuceTeam.IsMercInTeam(num2, true)
									&& !mercenary2.IsReadyForCrafting()
									&& mercenary2.m_owned)
								{
									lettuceTeam.AddMerc(mercenary2, -1, null);
									Out.Log(string.Format("[队伍编辑] 添加[{0}]，解锁装备/主线任务",
										mercenary2.m_mercName));
									if (lettuceTeam.GetMercCount() == 5 && Main.modeConf.Value == Mode.解锁装备)
									{
										break;
									}
								}
							}
						}
						//火焰刺杀
						else if (MercConst.FireKill_MapID.Contains(mapId))
						{
							foreach (int num2 in MercConst.FireKill)
							{
								HsGameUtils.UpdateEq(num2, MercConst.MercEquipID[num2]);
								LettuceMercenary mercenary2 = HsGameUtils.GetMercenary(num2);
								if (!lettuceTeam.IsMercInTeam(num2, true)
									&& !mercenary2.IsReadyForCrafting()
									&& mercenary2.m_owned)
								{
									lettuceTeam.AddMerc(mercenary2, -1, null);
									Out.Log(string.Format("[队伍编辑] 添加[{0}]，解锁装备/主线任务",
										mercenary2.m_mercName));
									if (lettuceTeam.GetMercCount() == 5 && Main.modeConf.Value == Mode.解锁装备)
									{
										break;
									}
								}
							}
						}
						// 冰队
						else if (MercConst.SnowTeam_MapID.Contains(mapId))
						{
							foreach (int num2 in MercConst.SnowTeam)
							{
								if (num2 != MercConst.瓦尔登_晨拥)
									HsGameUtils.UpdateEq(num2, MercConst.MercEquipID[num2]);
								else
									HsGameUtils.UpdateEq(num2, 1);
								LettuceMercenary mercenary2 = HsGameUtils.GetMercenary(num2);
								if (!lettuceTeam.IsMercInTeam(num2, true)
									&& !mercenary2.IsReadyForCrafting()
									&& mercenary2.m_owned)
								{
									lettuceTeam.AddMerc(mercenary2, -1, null);
									Out.Log(string.Format("[队伍编辑] 添加[{0}]，解锁装备/主线任务",
										mercenary2.m_mercName));
									if (lettuceTeam.GetMercCount() == 5 && Main.modeConf.Value == Mode.解锁装备)
									{
										break;
									}
								}
							}
						}
						// 冰火队
						else
						{
							foreach (int num2 in MercConst.SnowFire)
							{
								HsGameUtils.UpdateEq(num2, MercConst.MercEquipID[num2]);
								LettuceMercenary mercenary2 = HsGameUtils.GetMercenary(num2);
								if (!lettuceTeam.IsMercInTeam(num2, true)
									&& !mercenary2.IsReadyForCrafting()
									&& mercenary2.m_owned)
								{
									lettuceTeam.AddMerc(mercenary2, -1, null);
									Out.Log(string.Format("[队伍编辑] 添加[{0}]，解锁装备/主线任务",
										mercenary2.m_mercName));
									if (lettuceTeam.GetMercCount() == 5 && Main.modeConf.Value == Mode.解锁装备)
									{
										break;
									}
								}
							}
						}
					}
					if (unlockMercID.Count > 0 && Main.modeConf.Value == Mode.解锁装备) // 存在待解锁的佣兵
					{
						LettuceMercenary mercBoss = HsGameUtils.GetMercenary(unlockMercID[0]);
						if (!lettuceTeam.IsMercInTeam(mercBoss.ID, true))
							lettuceTeam.AddMerc(mercBoss, -1, null);
						Out.Log(string.Format("[队伍编辑] 添加[{0}]，解锁装备", mercBoss.m_mercName));
					}
				}
				else if(Main.modeConf.Value == Mode.一条龙)
                {
					int stage = OnePackageService.GetsTheCurrentStage();
					switch(stage)
                    {
						case 0:
                            //练30级阶段
                            foreach (int ID in MercConst.Origin0)
                            {
                                LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
                                if (!mercenary.IsReadyForCrafting()
                                    && mercenary.m_owned
                                    && !mercenary.IsMaxLevel()
                                    && !lettuceTeam.IsMercInTeam(ID, true))
                                {
                                    lettuceTeam.AddMerc(mercenary, -1, null);
                                    Out.Log(string.Format("[队伍编辑] 添加[{0}]，一条龙 阶段{1}",
                                        mercenary.m_mercName, (stage + 1).ToString()));
                                }
                            }
                            break;
                        case 1:
							//解锁剑圣阶段
							foreach (int ID in MercConst.Origin0)
							{
								LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
								if (!mercenary.IsReadyForCrafting()
									&& mercenary.m_owned
									&& !lettuceTeam.IsMercInTeam(ID,true))
								{
									lettuceTeam.AddMerc(mercenary, -1, null);
									Out.Log(string.Format("[队伍编辑] 添加[{0}]，一条龙 阶段{1}",
										mercenary.m_mercName, (stage+1).ToString()));
								}
							}
							break;
						case 2:
							//初始队伍刷30阶段
							foreach (int ID in MercConst.Origin)
							{
								LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
								if (!mercenary.IsReadyForCrafting()
									&& mercenary.m_owned
									&& !mercenary.IsMaxLevel()
									&& !lettuceTeam.IsMercInTeam(ID, true))
								{
									lettuceTeam.AddMerc(mercenary, -1, null);
									Out.Log(string.Format("[队伍编辑] 添加[{0}]，一条龙 阶段{1}",
										mercenary.m_mercName, (stage + 1).ToString()));
								}
							}
							break;
						case 3:
							//解锁到2-6阶段
						case 4:
							//刷神秘人阶段
							foreach (int ID in MercConst.Origin)
							{
								HsGameUtils.UpdateEq(ID, MercConst.MercEquipID[ID]);
								LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
								lettuceTeam.AddMerc(mercenary, -1, null);
								Out.Log(string.Format("[队伍编辑] 添加[{0}]，一条龙 阶段{1}",
									mercenary.m_mercName, (stage + 1).ToString()));
							}
							break;
						case 5:
							//AOE队伍刷到2000
							foreach (int ID in MercConst.AOE )
							{
								HsGameUtils.UpdateEq(ID, MercConst.MercEquipID[ID]);
								LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
								if (!mercenary.IsReadyForCrafting()
									&& mercenary.m_owned
									&& Main.CalcMercenaryCoinNeed(mercenary) != 8192
									&& Main.CalcMercenaryCoinNeed(mercenary) > 2000
									&& !lettuceTeam.IsMercInTeam(ID, true))
								{
									lettuceTeam.AddMerc(mercenary, -1, null);
									Out.Log(string.Format("[队伍编辑] 添加[{0}]，一条龙 阶段{1}",
										mercenary.m_mercName, (stage + 1).ToString()));
								}
							}
							Main.teamNumConf.Value = 3;
							break;
						case 6:
							//刷大德阶段
						case 7:
							//刷自然队阶段
							foreach (int ID in MercConst.AOE)
							{
								HsGameUtils.UpdateEq(ID, MercConst.MercEquipID[ID]);
								LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
								lettuceTeam.AddMerc(mercenary, -1, null);
								Out.Log(string.Format("[队伍编辑] 添加[{0}]，一条龙 阶段{1}",
									mercenary.m_mercName, (stage + 1).ToString()));
							}
							break;
						case 9:
							//自然队刷到1000
							foreach (int ID in MercConst.Nature)
							{
								HsGameUtils.UpdateEq(ID, MercConst.MercEquipID[ID]);
								LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
								if (!mercenary.IsReadyForCrafting()
									&& mercenary.m_owned
									&& Main.CalcMercenaryCoinNeed(mercenary) != 8192
									&& Main.CalcMercenaryCoinNeed(mercenary) > 1000
									&& !lettuceTeam.IsMercInTeam(ID, true))
								{
									lettuceTeam.AddMerc(mercenary, -1, null);
									Out.Log(string.Format("[队伍编辑] 添加[{0}]，一条龙 阶段{1}",
										mercenary.m_mercName, (stage + 1).ToString()));
								}
							}
							Main.teamNumConf.Value = 3;
							break;
						case 10:
							//解锁到4-1阶段
						case 11:
							//刷火焰队阶段
							foreach (int ID in MercConst.Nature)
							{
								HsGameUtils.UpdateEq(ID, MercConst.MercEquipID[ID]);
								LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
								lettuceTeam.AddMerc(mercenary, -1, null);
								Out.Log(string.Format("[队伍编辑] 添加[{0}]，一条龙 阶段{1}",
									mercenary.m_mercName, (stage + 1).ToString()));
							}
							break;
						case 12:
							//火焰队刷满级阶段
							foreach (int ID in MercConst.Fire)
							{
								HsGameUtils.UpdateEq(ID, MercConst.MercEquipID[ID]);
								LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
								if (!mercenary.IsReadyForCrafting()
									&& mercenary.m_owned
									&& Main.CalcMercenaryCoinNeed(mercenary) != 8192
									&& !lettuceTeam.IsMercInTeam(ID, true))
								{
									lettuceTeam.AddMerc(mercenary, -1, null);
									Out.Log(string.Format("[队伍编辑] 添加[{0}]，一条龙 阶段{1}",
										mercenary.m_mercName, (stage + 1).ToString()));
								}
							}
							Main.teamNumConf.Value = 3;
							break;
						case 13:
						case 15:
							//刷神秘人 全佣兵阶段
							foreach (int ID in MercConst.Fire)
							{
								HsGameUtils.UpdateEq(ID, MercConst.MercEquipID[ID]);
								LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
								lettuceTeam.AddMerc(mercenary, -1, null);
								Out.Log(string.Format("[队伍编辑] 添加[{0}]，一条龙 阶段{1}",
									mercenary.m_mercName, (stage + 1).ToString()));
							}
							break;
						case 8:
						case 14:
							//佣兵任务阶段
							if(stage == 8)
                            {
								LettuceMercenary mercenary = HsGameUtils.GetMercenary(MercConst.玛法里奥_怒风);
								lettuceTeam.AddMerc(mercenary, -1, null);
								Out.Log(string.Format("[队伍编辑] 添加[{0}]，一条龙 阶段{1}",
									mercenary.m_mercName, (stage + 1).ToString()));
								mercenary = HsGameUtils.GetMercenary(MercConst.米尔豪斯_法力风暴);
								lettuceTeam.AddMerc(mercenary, -1, null);
								Out.Log(string.Format("[队伍编辑] 添加[{0}]，一条龙 阶段{1}",
									mercenary.m_mercName, (stage + 1).ToString()));
							}
							foreach (Task task in TaskUtils.GetTasks())
							{
								foreach (MercenaryEntity mercenaryEntity in task.Mercenaries)
								{	
									LettuceMercenary mercenary = CollectionManager.Get().GetMercenary((long)mercenaryEntity.ID, false, true);
									if (mercenary != null && mercenary.m_owned && !lettuceTeam.IsMercInTeam(mercenaryEntity.ID, true))
									{
										HsGameUtils.UpdateEq(mercenaryEntity.ID, mercenaryEntity.Equipment);
										lettuceTeam.AddMerc(mercenary, -1, null);
										Out.Log(string.Format("[队伍编辑] 添加[{0}][装备iD:{1}]，一条龙 阶段{2}",
											mercenary.m_mercName, mercenaryEntity.Equipment, (stage + 1).ToString()));
										if (lettuceTeam.GetMercCount() == Main.teamNumConf.Value)
										{
											break;
										}
									}
								}
								if (lettuceTeam.GetMercCount() == Main.teamNumConf.Value)
								{
									break;
								}
							}
							break;
						case 16:
							//刷预设卡组满级
							foreach (int ID in MercConst.SnowFire)
							{
								HsGameUtils.UpdateEq(ID, MercConst.MercEquipID[ID]);
								if (lettuceTeam.GetMercCount() == Main.teamNumConf.Value)
									break;
								LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
								if (!mercenary.IsReadyForCrafting()
									&& mercenary.m_owned
									&& Main.CalcMercenaryCoinNeed(mercenary) != 8192
									&& !lettuceTeam.IsMercInTeam(ID, true))
								{
									lettuceTeam.AddMerc(mercenary, -1, null);
									Out.Log(string.Format("[队伍编辑] 添加[{0}]，一条龙 阶段{1}",
										mercenary.m_mercName, (stage + 1).ToString()));
								}
							}
							foreach (int ID in MercConst.Nature)
							{
								HsGameUtils.UpdateEq(ID, MercConst.MercEquipID[ID]);
								if (lettuceTeam.GetMercCount() == Main.teamNumConf.Value)
									break;
								LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
								if (!mercenary.IsReadyForCrafting()
									&& mercenary.m_owned
									&& Main.CalcMercenaryCoinNeed(mercenary) != 8192
									&& !lettuceTeam.IsMercInTeam(ID, true))
								{
									lettuceTeam.AddMerc(mercenary, -1, null);
									Out.Log(string.Format("[队伍编辑] 添加[{0}]，一条龙 阶段{1}",
										mercenary.m_mercName, (stage + 1).ToString()));
								}
							}
							foreach (int ID in MercConst.FireKill)
							{
								HsGameUtils.UpdateEq(ID, MercConst.MercEquipID[ID]);
								if (lettuceTeam.GetMercCount() == Main.teamNumConf.Value)
									break;
								LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
								if (!mercenary.IsReadyForCrafting()
									&& mercenary.m_owned
									&& Main.CalcMercenaryCoinNeed(mercenary) != 8192
									&& !lettuceTeam.IsMercInTeam(ID, true))
								{
									lettuceTeam.AddMerc(mercenary, -1, null);
									Out.Log(string.Format("[队伍编辑] 添加[{0}]，一条龙 阶段{1}",
										mercenary.m_mercName, (stage + 1).ToString()));
								}
							}
							foreach (int ID in MercConst.Piratesnake)
							{
								HsGameUtils.UpdateEq(ID, MercConst.MercEquipID[ID]);
								if (lettuceTeam.GetMercCount() == Main.teamNumConf.Value)
									break;
								LettuceMercenary mercenary = HsGameUtils.GetMercenary(ID);
								if (!mercenary.IsReadyForCrafting()
									&& mercenary.m_owned
									&& Main.CalcMercenaryCoinNeed(mercenary) != 8192
									&& !lettuceTeam.IsMercInTeam(ID, true))
								{
									lettuceTeam.AddMerc(mercenary, -1, null);
									Out.Log(string.Format("[队伍编辑] 添加[{0}]，一条龙 阶段{1}",
										mercenary.m_mercName, (stage + 1).ToString()));
								}
							}
							Main.teamNumConf.Value = 2;
							break;
						case 17:
                        case 18:
                        case 21:
                        case 20:
							InitMap();
                            int mapId = GetMapId();
							if (MercConst.Nature_MapID.Contains(mapId))
							{

								foreach (int num2 in MercConst.Nature)
								{
									HsGameUtils.UpdateEq(num2, MercConst.MercEquipID[num2]);
									LettuceMercenary mercenary2 = HsGameUtils.GetMercenary(num2);
									if (!lettuceTeam.IsMercInTeam(num2, true)
										&& !mercenary2.IsReadyForCrafting()
										&& mercenary2.m_owned)
									{
										lettuceTeam.AddMerc(mercenary2, -1, null);
										Out.Log(string.Format("[队伍编辑] 添加[{0}]，一条龙 阶段{1}",
											mercenary2.m_mercName, (stage + 1).ToString()));
										if (lettuceTeam.GetMercCount() == 5 && stage == 21 )
										{
											break;
										}
									}
								}
							}
							// 海盗蛇
							else if (MercConst.Piratesnake_MapID.Contains(mapId))
							{
								foreach (int num2 in MercConst.Piratesnake)
								{
									HsGameUtils.UpdateEq(num2, MercConst.MercEquipID[num2]);
									LettuceMercenary mercenary2 = HsGameUtils.GetMercenary(num2);
									if (!lettuceTeam.IsMercInTeam(num2, true)
										&& !mercenary2.IsReadyForCrafting()
										&& mercenary2.m_owned)
									{
										lettuceTeam.AddMerc(mercenary2, -1, null);
										Out.Log(string.Format("[队伍编辑] 添加[{0}]，一条龙 阶段{1}",
											mercenary2.m_mercName, (stage + 1).ToString()));
										if (lettuceTeam.GetMercCount() == 5 && stage == 21)
										{
											break;
										}
									}
								}
							}
							//火焰刺杀
							else if (MercConst.FireKill_MapID.Contains(mapId))
							{
								foreach (int num2 in MercConst.FireKill)
								{
									HsGameUtils.UpdateEq(num2, MercConst.MercEquipID[num2]);
									LettuceMercenary mercenary2 = HsGameUtils.GetMercenary(num2);
									if (!lettuceTeam.IsMercInTeam(num2, true)
										&& !mercenary2.IsReadyForCrafting()
										&& mercenary2.m_owned)
									{
										lettuceTeam.AddMerc(mercenary2, -1, null);
										Out.Log(string.Format("[队伍编辑] 添加[{0}]，一条龙 阶段{1}",
											mercenary2.m_mercName, (stage + 1).ToString()));
										if (lettuceTeam.GetMercCount() == 5 && stage == 21)
										{
											break;
										}
									}
								}
							}
							// 冰队
							else if (MercConst.SnowTeam_MapID.Contains(mapId))
							{
								foreach (int num2 in MercConst.SnowTeam)
								{
									if (num2 != MercConst.瓦尔登_晨拥)
										HsGameUtils.UpdateEq(num2, MercConst.MercEquipID[num2]);
									else
										HsGameUtils.UpdateEq(num2, 1);
									LettuceMercenary mercenary2 = HsGameUtils.GetMercenary(num2);
									if (!lettuceTeam.IsMercInTeam(num2, true)
										&& !mercenary2.IsReadyForCrafting()
										&& mercenary2.m_owned)
									{
										lettuceTeam.AddMerc(mercenary2, -1, null);
										Out.Log(string.Format("[队伍编辑] 添加[{0}]，一条龙 阶段{1}",
											mercenary2.m_mercName, (stage + 1).ToString()));
										if (lettuceTeam.GetMercCount() == 5 &&  stage == 21)
										{
											break;
										}
									}
								}
							}
							// 冰火队
							else
							{
								foreach (int num2 in MercConst.SnowFire)
								{
									HsGameUtils.UpdateEq(num2, MercConst.MercEquipID[num2]);
									LettuceMercenary mercenary2 = HsGameUtils.GetMercenary(num2);
									if (!lettuceTeam.IsMercInTeam(num2, true)
										&& !mercenary2.IsReadyForCrafting()
										&& mercenary2.m_owned)
									{
										lettuceTeam.AddMerc(mercenary2, -1, null);
										Out.Log(string.Format("[队伍编辑] 添加[{0}]，一条龙 阶段{1}",
											mercenary2.m_mercName, (stage + 1).ToString()));
										if (lettuceTeam.GetMercCount() == 5 && stage == 21)
										{
											break;
										}
									}
								}
							}
							if (unlockMercID.Count > 0 && lettuceTeam.GetMercCount() == 5) // 存在待解锁的佣兵
							{
								LettuceMercenary mercBoss = HsGameUtils.GetMercenary(unlockMercID[0]);
								if (!lettuceTeam.IsMercInTeam(mercBoss.ID, true))
									lettuceTeam.AddMerc(mercBoss, -1, null);
								Out.Log(string.Format("[队伍编辑] 添加[{0}]，解锁装备", mercBoss.m_mercName));
							}
							break;
                        case 19:
							//冰火队刷神秘人
							foreach (int num2 in MercConst.SnowFire)
							{
								HsGameUtils.UpdateEq(num2, MercConst.MercEquipID[num2]);
								LettuceMercenary mercenary2 = HsGameUtils.GetMercenary(num2);
								if (!lettuceTeam.IsMercInTeam(num2, true)
									&& !mercenary2.IsReadyForCrafting()
									&& mercenary2.m_owned)
								{
									lettuceTeam.AddMerc(mercenary2, -1, null);
									Out.Log(string.Format("[队伍编辑] 添加[{0}]，一条龙 阶段{1}",
										mercenary2.m_mercName, (stage + 1).ToString()));
									if (lettuceTeam.GetMercCount() == 3)
									{
										break;
									}
								}
							}
							break;
					}
                }
				if (lettuceTeam.GetMercCount() < Main.teamNumConf.Value)
				{
					foreach (LettuceMercenary lettuceMercenary2 in mercenaries)
					{
						if (!lettuceTeam.IsMercInTeam(lettuceMercenary2.ID, true)
							&& !lettuceMercenary2.IsReadyForCrafting()
							&& !lettuceMercenary2.IsMaxLevel())
						{
							lettuceTeam.AddMerc(lettuceMercenary2, -1, null);
							Out.Log(string.Format("[队伍编辑] 添加[{0}]，因为未30级",
								lettuceMercenary2.m_mercName));
							if (lettuceTeam.GetMercCount() == Main.teamNumConf.Value)
							{
								break;
							}
						}
					}
				}
				if (lettuceTeam.GetMercCount() < Main.teamNumConf.Value)
				{
					foreach (int num2 in MercConst.prior)
					{
						LettuceMercenary mercenary2 = HsGameUtils.GetMercenary(num2);
						if (!lettuceTeam.IsMercInTeam(num2, true)
							&& !mercenary2.IsReadyForCrafting()
							&& CalcMercenaryCoinNeed(mercenary2) != 8192
							&& mercenary2.m_owned)
						{
							lettuceTeam.AddMerc(mercenary2, -1, null);
							Out.Log(string.Format("[队伍编辑] 添加[{0}]，因为+1+5优先级设置高",
								mercenary2.m_mercName));
							if (lettuceTeam.GetMercCount() == Main.teamNumConf.Value)
							{
								break;
							}
						}
					}
				}
				if (lettuceTeam.GetMercCount() < Main.teamNumConf.Value)
				{
					foreach (LettuceMercenary lettuceMercenary3 in mercenaries)
					{
						if (!lettuceTeam.IsMercInTeam(lettuceMercenary3.ID, true)
							&& !lettuceMercenary3.IsReadyForCrafting()
							&& CalcMercenaryCoinNeed(lettuceMercenary3) != 8192
							&& !MercConst.Ignore.Contains(lettuceMercenary3.ID))
						{
							lettuceTeam.AddMerc(lettuceMercenary3, -1, null);
							Out.Log(string.Format("[队伍编辑] 添加[{0}]，碎片不够+1+5",
								lettuceMercenary3.m_mercName));
							if (lettuceTeam.GetMercCount() == Main.teamNumConf.Value)
							{
								break;
							}
						}
					}
				}
                if (lettuceTeam.GetMercCount() < Main.teamNumConf.Value)
                {
                    foreach (LettuceMercenary lettuceMercenary3 in mercenaries)
                    {
                        if (!lettuceTeam.IsMercInTeam(lettuceMercenary3.ID, true)
                            && !lettuceMercenary3.IsReadyForCrafting()
                            && CalcMercenaryCoinNeed(lettuceMercenary3) != 8192)
                        {
                            lettuceTeam.AddMerc(lettuceMercenary3, -1, null);
                            Out.Log(string.Format("[队伍编辑] 添加[{0}]，碎片不够+1+5",
                                lettuceMercenary3.m_mercName));
                            if (lettuceTeam.GetMercCount() == Main.teamNumConf.Value)
                            {
                                break;
                            }
                        }
                    }
                }
                if (lettuceTeam.GetMercCount() < Main.teamNumConf.Value)
				{
					foreach (LettuceMercenary lettuceMercenary4 in mercenaries)
					{
						if (!lettuceTeam.IsMercInTeam(lettuceMercenary4.ID, true) && !lettuceMercenary4.IsReadyForCrafting())
						{
							lettuceTeam.AddMerc(lettuceMercenary4, -1, null);
							Out.Log(string.Format("[队伍编辑] 添加[{0}]，满级优先级设置低",
								lettuceMercenary4.m_mercName));
							if (lettuceTeam.GetMercCount() == Main.teamNumConf.Value)
							{
								break;
							}
						}
					}
				}
				lettuceTeam.SendChanges();
			}
		}
		public static long CalcMercenaryCoinNeed(LettuceMercenary merc)
		{
			long coinNeed = 0;
			if (!merc.m_owned)
			{
				coinNeed = merc.GetCraftingCost() - merc.m_currencyAmount;
				return (coinNeed > 0) ? coinNeed : 4096;
			}

			foreach (var ability in merc.m_abilityList)
			{
				if (ability.GetNextUpgradeCost() <= 0)
				{
					continue;
				}
				int tier = ability.GetNextTier() - 1;
				switch (tier)
				{
					case 1:
						coinNeed += 50 + 125 + 150 + 150;
						break;
					case 2:
						coinNeed += 125 + 150 + 150;
						break;
					case 3:
						coinNeed += 150 + 150; ;
						break;
					case 4:
						coinNeed += 150;
						break;
					case 5:
						coinNeed += 0;
						break;
				}
			}
			foreach (var equipment in merc.m_equipmentList)
			{
				if (equipment.GetNextUpgradeCost() <= 0)
				{
					continue;
				}
				int tier = equipment.GetNextTier() - 1;
				switch (tier)
				{
					case 1:
						coinNeed += 100 + 150 + 175;
						break;
					case 2:
						coinNeed += 150 + 175;
						break;
					case 3:
						coinNeed += 175; 
						break;
					case 4:
						coinNeed += 0;
						break;
				}
			}
			coinNeed -= merc.m_currencyAmount;
			return (coinNeed > 0) ? coinNeed : 8192;
		}
		private static void SelectNextNode(LettuceMap map)
		{

			List<LettuceMapNode> nodes = map.NodeData;

			ValueTuple<LettuceMapNode, int> nextNode = Main.GetNextNode(nodes.FindAll((LettuceMapNode n) => n.NodeState_ == LettuceMapNode.NodeState.UNLOCKED), nodes);
			LettuceMapNode lettuceMapNode = nextNode.Item1;
			int item = nextNode.Item2;
			if (lettuceMapNode == null)
			{
				Out.Log("[节点选择] 没有找到神秘节点 重开");
				Network.Get().RetireLettuceMap();
				Main.Sleep(2);
				return;
			}
			if (!Main.NeedCompleted() && item > 3)
			{
				Out.Log(string.Format("[节点选择] 通往神秘节点数{0}大于3 重开", item));
				Network.Get().RetireLettuceMap();
				Main.Sleep(2);
				return;
			}
			if (HsGameUtils.IsMonster(lettuceMapNode.NodeTypeId))
			{
				GameMgr gameMgr = GameMgr.Get();
				GameType gameType = GameType.GT_MERCENARIES_PVE;
				FormatType formatType = FormatType.FT_WILD;
				int missionId = 3790;
				int brawlLibraryItemId = 0;
				long deckId = 0L;
				string aiDeck = null;
				int? lettuceMapNodeId = new int?((int)lettuceMapNode.NodeId);
				Out.Log(string.Format("[节点选择] 怪物节点[NID:{0}][NTYPE:{1}] 进入战斗", lettuceMapNode.NodeId, lettuceMapNode.NodeTypeId));
				gameMgr.FindGame(gameType, formatType, missionId, brawlLibraryItemId, deckId, aiDeck, null, false, null, lettuceMapNodeId, 0L, GameType.GT_UNKNOWN);
				if (modeConf.Value == "挂机收菜")
				{
					readyToHang = true;
					Out.Log("挂机收菜 下局战斗将写入收菜时间");
				}
			}
			else
			{
				Out.Log(string.Format("[节点选择] 普通节点[NID:{0}][NTYPE:{1}]", lettuceMapNode.NodeId, lettuceMapNode.NodeTypeId));
				Network.Get().ChooseLettuceMapNode(lettuceMapNode.NodeId);
			}
		}
		// Token: 0x0600001C RID: 28 RVA: 0x00002EC8 File Offset: 0x000010C8
		private static void Sleep(float time)
		{
			if ((float)HsMod.ConfigValue.Get().TimeGearValue != 0)
				Main.sleepTime += (float)time / (float)HsMod.ConfigValue.Get().TimeGearValue;
			else Main.sleepTime += (float)time;
		}

		// Token: 0x0600001D RID: 29 RVA: 0x00002ED7 File Offset: 0x000010D7
		private void GameInit()
		{
			if (InactivePlayerKicker.Get() != null)
			{
				Main.initialize = true;
				InactivePlayerKicker.Get().SetKickSec(180000f);
				Main.Sleep(8);
			}
		}

		// Token: 0x0600001E RID: 30 RVA: 0x00002EFC File Offset: 0x000010FC
		private void Update()
		{
			if (!Main.isPluginEnable.Value) return;

			if (Input.GetKeyUp(Main.StartKey))
			{
				Main.isRunning = !Main.isRunning;
				UIStatus.Get().AddInfo(Main.isRunning ? "插件启动" : "插件关闭");
				Main.runningConf.Value = Main.isRunning;
			}


			if (Input.GetKeyUp(KeyCode.F3))
			{
				Out.Log("F3查询");

				CollectionManager.FindMercenariesResult result = CollectionManager.Get().FindOrderedMercenaries(null, true);
				foreach (LettuceMercenary mercy in result.m_mercenaries)
				{
					foreach (LettuceAbility ability in mercy.m_abilityList)
					{
						string upgrade = "不可升级";
						if (mercy.IsCardReadyForUpgrade(ability.ID, CollectionUtils.MercenariesModeCardType.Ability))
							upgrade = "可升级";
						Out.Log(string.Format("[佣兵;{0}][佣兵ID:{1}][技能:{2}][技能ID:{3}][{4}]",
							mercy.m_mercName, mercy.ID, ability.GetCardName(), ability.ID, upgrade));
					}
					foreach (LettuceAbility ability in mercy.m_equipmentList)
					{
						string upgrade = "不可升级";
						if (mercy.IsCardReadyForUpgrade(ability.ID, CollectionUtils.MercenariesModeCardType.Equipment))
						{
							Network.Get().UpgradeMercenaryEquipment(mercy.ID, ability.ID);
							upgrade = "可升级";
						}
						Out.Log(string.Format("[佣兵;{0}][佣兵ID:{1}][技能:{2}][装备ID:{3}][{4}]",
							mercy.m_mercName, mercy.ID, ability.GetCardName(), ability.ID, upgrade));
					}
				}
			}
			if (Input.GetKeyUp(KeyCode.F7))
			{
				Out.Log("F7查询");
				for(int i = 0; i < name_temp.Count; i++)
				{
					Out.Log(generatePassword(name_temp[i]));
				}
				Out.Log($"{isFriend("君莫笑#123123")}");

				//Out.Log(string.Format("一条龙阶段:{0}", OnePackageService.GetsTheCurrentStage()));
			}

			if (!Main.isRunning)
			{
				return;
			}
			this.CheckIdleTime();
			if ((double)(Time.realtimeSinceStartup - Main.sleepTime) <= 0.01)
			{
				return;
			}
			Main.sleepTime = Time.realtimeSinceStartup;
			if (!Main.initialize)
			{
				this.GameInit();
				return;
			}
			GameMgr gameMgr = GameMgr.Get();
			GameType gameType = gameMgr.GetGameType();
			SceneMgr sceneMgr = SceneMgr.Get();
			SceneMgr.Mode mode = sceneMgr.GetMode();
			GameState gameState = GameState.Get();


			#region 查找比赛
			if (gameMgr.IsFindingGame())
			{
				if (Main.autoTimeGearConf.Value) HsMod.ConfigValue.Get().TimeGearValue = 2;
				Out.Log("[状态] 查找比赛，休息15秒");
				Main.Sleep(15);
				return;
			}
			#endregion


			#region 村子或者角斗场
			if (gameType == GameType.GT_UNKNOWN && (mode == SceneMgr.Mode.LETTUCE_VILLAGE || mode == SceneMgr.Mode.LETTUCE_PLAY) && gameState == null)
			{
				if (Main.autoTimeGearConf.Value) HsMod.ConfigValue.Get().TimeGearValue = 2;
				
				if (!(Main.modeConf.Value == Mode.PVP))
				{
					Out.Log("[状态] 目前处于村庄/角斗场，切换到地图，休息5秒");
					HsGameUtils.GotoSceneMap();
					Main.Sleep(5);
					Main.ResetIdle();
					return;
				}
				List<global::LettuceTeam> teams = CollectionManager.Get().GetTeams();
				if (teams.Count == 0)
				{
					UIStatus.Get().AddInfo("请先创建队伍并在设置里选择队伍！");
					Out.Log("未创建过队伍，插件暂停");
					Main.isRunning = false;
					return;
				}
                if (Main.PvpRandomTeam.Value)
                {
                    System.Random rd = new System.Random();
                    int num = rd.Next(0, teams.Count);
					if(teams[num].Name == "PVE" || teams[num].Name == "初始队伍")
					{
						num = (num + 1) % teams.Count();
					}
						Main.teamNameConf.Value = teams[num].Name;
                    Out.Log($"队伍编号:{num} 当前队伍名:{Main.teamNameConf.Value}");
                }
                global::LettuceTeam lettuceTeam = teams.FindLast((global::LettuceTeam t) => t.Name == Main.teamNameConf.Value);
				
				if (lettuceTeam == null)
				{
					UIStatus.Get().AddInfo("配置队伍名不匹配！F1打开配置");
					Out.Log("没有预设名称对应的队伍，插件暂停");
					Main.isRunning = false;
					return;
				}
				HsGameUtils.GotoSceneVillage();
				Main.Sleep(1);
				GameMgr.Get().FindGame(GameType.GT_MERCENARIES_PVP, FormatType.FT_WILD, 3743, 0, 0L, null, null, false, null, null, lettuceTeam.ID, GameType.GT_UNKNOWN);
                OPSLable = "";
                return;
			}
			#endregion

			#region 主界面
			if (gameType == GameType.GT_UNKNOWN && mode == SceneMgr.Mode.HUB && gameState == null)
			{
				if (Main.autoTimeGearConf.Value) HsMod.ConfigValue.Get().TimeGearValue = 2;
				Out.Log("[状态] 目前处于主界面，切换到村庄，休息5秒");
				HsGameUtils.GotoSceneVillage();
				Main.Sleep(5);
				return;
			}
			#endregion

			#region 悬赏面板
			if (gameType == GameType.GT_UNKNOWN && mode == SceneMgr.Mode.LETTUCE_BOUNTY_BOARD && gameState == null)
			{
				
				if (Main.autoTimeGearConf.Value) HsMod.ConfigValue.Get().TimeGearValue = 2;
				Out.Log(string.Format("[状态] 目前处于悬赏面板，切换到队伍选择，选择[MAPID:{0}]，休息6秒", GetMapId()));
				HsGameUtils.SelectBoss(this.GetMapId());
				Main.ResetIdle();
				Main.Sleep(6);
				return;
			}
			#endregion

			#region 队伍选择
			if (gameType == GameType.GT_UNKNOWN && mode == SceneMgr.Mode.LETTUCE_BOUNTY_TEAM_SELECT && gameState == null)
			{
				if (Main.autoTimeGearConf.Value) HsMod.ConfigValue.Get().TimeGearValue = 2;
				if(Main.autoAckConf.Value)	AckMercFullLevel();
				this.AutoUpdateSkill();
				Main.Sleep(5);
				this.AutoCraft();
				try
				{
					if (Main.modeConf.Value == Mode.佣兵任务)
					{

						Main.coreTeamNumConf.Value = 0;
						Main.teamNumConf.Value = 6;
						TaskUtils.UpdateTask();
						foreach (Task task in TaskUtils.GetTasks())
						{
							Out.Log(string.Format("[TID:{0}] 已持续：{1}s",
								task.Id, (TaskUtils.Current() - task.StartAt)));
							if (TaskUtils.CleanConf[Main.cleanTaskConf.Value] != -1 
								&& TaskUtils.Current() - task.StartAt > (long)TaskUtils.CleanConf[Main.cleanTaskConf.Value])
							{
								Out.Log(string.Format("[TID:{0}] 已过期，放弃",
									task.Id));
								HsGameUtils.CleanTask(task.Id);
							}
						}
					}
					else if (Main.modeConf.Value == Mode.解锁装备)
					{   //5.7改动
						Main.teamNumConf.Value = 6;
						Main.coreTeamNumConf.Value = 0;
					}
					else if (Main.modeConf.Value == Mode.一条龙)
					{   //一条龙
						Main.teamNumConf.Value = 6;
						Main.coreTeamNumConf.Value = 0;
                        TaskUtils.UpdateTask();
                        int stage = OnePackageService.GetsTheCurrentStage();
						OPSLable = "一条龙阶段: " + (stage+1).ToString() + "阶段 " + StageInfo[stage+1];
						switch (stage)
						{
							case 1:
							case 3:
							case 10:
							case 17:
							case 20:
								//自动主线
								TaskUtils.UpdateMainLineTask();
								break;
							case 8:
								//TaskUtils.UpdateTask();
								foreach(Task task in TaskUtils.GetTasks())
								{
									foreach(var item in task.Mercenaries)
                                    {
										if(item.ID != MercConst.玛法里奥_怒风)
                                        {
											Out.Log($"佣兵任务[Mid:{item.ID}]不是玛法里奥·怒风，放弃");
											HsGameUtils.CleanTask(task.Id);
											break;
                                        }
                                    }
                                }
								break;
							case 14:
								//TaskUtils.UpdateTask();
								Main.cleanTaskConf.Value = "60分钟";
                                foreach (Task task in TaskUtils.GetTasks())
								{
									
									Out.Log(string.Format("[TID:{0}] 已持续：{1}s",
										task.Id, (TaskUtils.Current() - task.StartAt)));
									if (TaskUtils.CleanConf[Main.cleanTaskConf.Value] != -1 
										&& TaskUtils.Current() - task.StartAt > (long)TaskUtils.CleanConf[Main.cleanTaskConf.Value])
									{
										Out.Log(string.Format("[TID:{0}] 已过期，放弃",
											task.Id));
										HsGameUtils.CleanTask(task.Id);
									}
								}
								break;
							default:
								break;
						}
					}
					else if (Main.modeConf.Value == Mode.主线任务)
					{
						//5.7改动
						TaskUtils.UpdateMainLineTask();
						Main.teamNumConf.Value = 6;

					}
				}
				catch (Exception ex)
				{
					Out.Log("空间名：" + ex.Source + "；" + '\n' +
							"方法名：" + ex.TargetSite + '\n' +
							"故障点：" + ex.StackTrace.Substring(ex.StackTrace.LastIndexOf("\\") + 1,
									ex.StackTrace.Length - ex.StackTrace.LastIndexOf("\\") - 1) + '\n' +
							"错误提示：" + ex.Message);
				}
				this.AutoChangeTeam();
				if ((double)Main.idleTime > 20.0)
				{
					HsGameUtils.GotoSceneVillage();
					return;
				}
				Main.Sleep(1);
				List<global::LettuceTeam> teams2 = CollectionManager.Get().GetTeams();
				if (teams2.Count == 0)
				{
					UIStatus.Get().AddInfo("F1打开配置,检查队伍名,默认PVE");
					Out.Log("未创建过队伍，插件暂停");
					Main.isRunning = false;
					return;
				}
				global::LettuceTeam lettuceTeam2 = teams2.FindLast((global::LettuceTeam t) => t.Name == Main.teamNameConf.Value);
				if (lettuceTeam2 == null)
				{
					UIStatus.Get().AddInfo("F1打开配置,检查队伍名,默认PVE");
					Out.Log("没有预设名称对应的队伍，插件暂停");
					Main.isRunning = false;
					return;
				}
				LettuceVillageDisplay.LettuceSceneTransitionPayload lettuceSceneTransitionPayload = new LettuceVillageDisplay.LettuceSceneTransitionPayload();
				int mapId = this.GetMapId();
				LettuceBountyDbfRecord record = GameDbf.LettuceBounty.GetRecord(mapId);
				lettuceSceneTransitionPayload.m_TeamId = lettuceTeam2.ID;
				lettuceSceneTransitionPayload.m_SelectedBounty = record;
				lettuceSceneTransitionPayload.m_SelectedBountySet = record.BountySetRecord;
				lettuceSceneTransitionPayload.m_IsHeroic = record.Heroic;
				Out.Log(string.Format("[状态] 目前处于队伍选择，选择[MAPID:{0}]", mapId));
				if (Main.modeConf.Value == "挂机收菜")
				{
					readyToHang = true;
					Out.Log(string.Format("[状态] 下局战斗将写入收菜时间"));
				}
				SceneMgr.Get().SetNextMode(SceneMgr.Mode.LETTUCE_MAP, SceneMgr.TransitionHandlerType.CURRENT_SCENE, null, lettuceSceneTransitionPayload);
				return;
			}
			#endregion

			#region 地图
			if (gameType == GameType.GT_UNKNOWN && mode == SceneMgr.Mode.LETTUCE_MAP && gameState == null && (double)Main.idleTime > 20.0)
			{
				Out.Log("[状态] 目前处于地图，空闲时间超过20s，返回村庄");
				sceneMgr.SetNextMode(SceneMgr.Mode.LETTUCE_VILLAGE, SceneMgr.TransitionHandlerType.SCENEMGR, null, null);
				Main.Sleep(1);
				return;
			}
			#endregion

			#region 排队中
			if (gameState != null && ((gameType != GameType.GT_MERCENARIES_PVE && gameType != GameType.GT_MERCENARIES_PVP) || sceneMgr.GetMode() != SceneMgr.Mode.GAMEPLAY || !gameState.IsGameCreatedOrCreating()))
			{
				Out.Log("[状态] 排队中");
				if (Main.autoTimeGearConf.Value) HsMod.ConfigValue.Get().TimeGearValue = 2;
				Main.Sleep(1);
				return;
			}
			#endregion


			#region 游戏结束
			if (gameState == null || gameState.IsGameOver())
			{

				if (EndGameScreen.Get())
				{

					PegUIElement hitbox = EndGameScreen.Get().m_hitbox;
					Blizzard.T5.Core.Map<UIEventType, List<UIEvent.Handler>> eventListeners =
						(Blizzard.T5.Core.Map<UIEventType, List<UIEvent.Handler>>)Traverse.Create(hitbox).Field("m_eventListeners").GetValue();
					if (hitbox != null && Main.modeConf.Value != Mode.PVP)
					{
						hitbox.TriggerPress();
						hitbox.TriggerRelease();
						//Out.Log("[对局结束] 悬赏结束，点击");
						if (Main.autoTimeGearConf.Value) HsMod.ConfigValue.Get().TimeGearValue = 2;

						if (eventListeners.Count > 0)
						{
							//Main.Sleep(1);
							if (ExpFlag == false)
								ExpLable = "经验效率:" + ((int)MercXpCalculation()).ToString() + "/小时";
						}
						else
						{
							//Main.Sleep(1);
							if (ExpFlag == false)
								ExpLable = "经验效率:" + ((int)MercXpCalculation()).ToString() + "/小时";
						}
						//Main.Sleep(1);
						Main.ResetIdle();
					}
					else if (hitbox != null)//pvp结束
					{
						hitbox.TriggerPress();
						hitbox.TriggerRelease();
						//Out.Log("[对局结束] Pvp结束，点击");

						if (eventListeners.Count > 0)
						{
							MercTeamCount();//保存佣兵阵容
							if (ExpFlag == false)
								ExpLable = "经验效率:" + ((int)MercXpCalculation()).ToString() + "/小时";
							Main.Sleep(1);
						}
						else
						{
							//Out.Log("[对局结束] Pvp结束，保存佣兵阵容");
							MercTeamCount();//保存佣兵阵容
							if (ExpFlag == false)
								ExpLable = "经验效率:" + ((int)MercXpCalculation()).ToString() + "/小时";
							Out.Log(ExpLable);
							Main.Sleep(1);
						}
						Main.Sleep(1);
						Main.ResetIdle();
					}
				}
				return;
			}
			#endregion
			Main.Sleep(1);
			this.HandlePlay();
		}
		private static double MercXpCalculation()
		{
			int PrevXp = 0, CurrXp = 0, PrevLevel = 0, CurrLevel = 0;
			// 经验
			RewardXpNotificationManager rewardXpNotificationManager = RewardXpNotificationManager.Get();
			if (rewardXpNotificationManager != null)
			{
				List<RewardTrackXpChange> xpChanges = (List<RewardTrackXpChange>)Traverse.Create(rewardXpNotificationManager).Field("m_xpChanges").GetValue();
				foreach (RewardTrackXpChange xpChange in xpChanges)
				{
					CurrXp = xpChange.CurrXp;
					CurrLevel = xpChange.CurrLevel;
					PrevXp = xpChange.PrevXp;
					PrevLevel = xpChange.PrevLevel;
				}
			}
			int XpDelta = 0;
			for (int i = PrevLevel; i < CurrLevel; i++)
			{
				if (i <= 131)
					XpDelta += expLevels[i];
				else XpDelta += 1500;
			}
			XpDelta = XpDelta - PrevXp + CurrXp;
			totalExp += XpDelta;
			TimeSpan TimeDelta = DateTime.Now - dateTimePre;
			//Out.Log(string.Format("[经验演算]开始挂机时间:{0}\t对局结束时间:{1}\t时间间隔:{2}\t", dateTimePre, DateTime.Now, TimeDelta.TotalSeconds));
			//Out.Log(string.Format("[经验演算]对局前等级:{0}\t对局后等级{1}\t对局前经验:{2}\t对局后经验:{3}\t对局经验:{4}\t累计总经验:{5}", PrevLevel, CurrLevel, PrevXp, CurrXp, XpDelta, totalExp));
			ExpFlag = true;
			return totalExp / TimeDelta.TotalSeconds * 3600;
		}
		private static void MercTeamCount()//保存佣兵阵容
		{
			int rateResult = 0, rateResultDelta = 0;
			if (GameState.Get().GetGameEntity() is LettuceMissionEntity)
			{
				LettuceMissionEntity lettuceGameEntity = (LettuceMissionEntity)GameState.Get().GetGameEntity();
				if (lettuceGameEntity != null && lettuceGameEntity.RatingChangeData != null)
				{
					rateResult = lettuceGameEntity.RatingChangeData.PvpRating;
					PvpRate = "当前Pvp分数:" + rateResult;
					rateResultDelta = lettuceGameEntity.RatingChangeData.Delta;
				}
			}

			string strlog = "";//佣兵阵容
			QueueList<string> allMatchLog = new QueueList<string>();
			int i = 0;
			if (MercTeam.Count == 0) return;
			foreach (string str in MercTeam)
			{
				if (i % 2 == 0)
					strlog += string.Format("{0}<br/>", str);
				else
				{
					strlog += string.Format("{0},", str);
				}
				i++;
			}

			//当前胜率 胜-负
			string score = PvpWin.ToString() + '-' + PvpLose.ToString();
			PvpLable = "当前胜率: " + score + "\n";



			allMatchLog = TeamOut.ReadFile();
			//分数变化，当前分数，卡组名，你的对手,佣兵阵容
			TeamOut.WriteFile(allMatchLog, String.Format("{0},{1},{2},{3},{4}",
				 rateResultDelta.ToString(), rateResult.ToString(), Main.teamNameConf.Value + ':' + score,
				 (string.IsNullOrEmpty(HsMod.ConfigValue.Get().CacheOpponentFullName))?
				 "官方AI": HsMod.ConfigValue.Get().CacheOpponentFullName, strlog));

			MercTeam.Clear();
		}
		

		// Token: 0x0600001F RID: 31 RVA: 0x00003444 File Offset: 0x00001644
		private void AutoCraft()
		{
			if (!Main.autoCraftConf.Value)
			{
				return;
			}
			Out.Log("[制作佣兵]");
			foreach (LettuceMercenary lettuceMercenary in CollectionManager.Get().FindOrderedMercenaries(null, new bool?(true), null, null, null).m_mercenaries)
			{
				if (lettuceMercenary.IsReadyForCrafting())
				{
					Network.Get().CraftMercenary(lettuceMercenary.ID);
					Out.Log(string.Format("[制作佣兵] [MID:{0}]", lettuceMercenary.ID));
				}
			}
		}

		// Token: 0x06000020 RID: 32 RVA: 0x000034F8 File Offset: 0x000016F8
		private void AutoUpdateSkill()
		{
			Out.Log("[升级技能]");
			if (!Main.autoUpdateSkillConf.Value)
			{
				return;
			}
			HsGameUtils.UpdateAllSkill();
		}

		// Token: 0x06000021 RID: 33 RVA: 0x0000350C File Offset: 0x0000170C
		private int EnsureMapHasUnlock(int id)
		{
			LettuceBountyDbfRecord record = GameDbf.LettuceBounty.GetRecord(id);

			if(record.RequiredCompletedBounty > 0)
            {
				//1.非第一小关
				if (false == MercenariesDataUtil.IsBountyComplete(record.RequiredCompletedBounty))
					return MapUtils.GetUnCompleteMap();
            }
			else
            {
				if (id == 57 || id == 85)
					return id;
				if ((id == 67 || id == 94 )&& false == MercenariesDataUtil.IsBountyComplete(65))
					return MapUtils.GetUnCompleteMap();
				if ((id == 73 || id == 100 )&& false == MercenariesDataUtil.IsBountyComplete(72))
					return MapUtils.GetUnCompleteMap();
				else if (false == MercenariesDataUtil.IsBountyComplete(78))
					return MapUtils.GetUnCompleteMap();
            }
			return id;
		}

		// Token: 0x06000022 RID: 34 RVA: 0x00003548 File Offset: 0x00001748
		private int GetMapId()
		{
			int result = 57;

			if (Main.modeConf.Value == Mode.解锁装备)
			{
				InitMap();
				if (unlockMapID.Count > 0)
				{
					Out.Log(string.Format("自动解锁装备地图ID:{0}", unlockMapID[0]));
					return unlockMapID[0];
				}
				else
				{
					Out.Log(string.Format("无待解锁装备地图"));
					return MapUtils.GetUnCompleteMap();
				}
			}
			if (Main.modeConf.Value == Mode.佣兵任务 ||
				Main.modeConf.Value == Mode.主线任务)
			{
				int taskMap = TaskUtils.GetTaskMap();
				if (taskMap != -1)
				{
					return this.EnsureMapHasUnlock(taskMap);
				}
				Out.Log("没约定特定的地图，我要去2-5混了");
				return this.EnsureMapHasUnlock(MapUtils.GetMapId("2-5"));
			}
			else if (Main.modeConf.Value == Mode.一条龙)
			{
				int stage = OnePackageService.GetsTheCurrentStage();
				int taskMap = -1;
				switch (stage)
				{
					case 0:
						Out.Log($"[地图选择]第{stage}阶段完成，准备刷图H1-1");
						return 85;
					case 1:
						taskMap = TaskUtils.GetTaskMap();
						if (taskMap != -1)
						{
							Out.Log($"[地图选择]第{stage}阶段完成，准备自动主线");
							return this.EnsureMapHasUnlock(taskMap);
						}
						break;
					case 2:
						Out.Log($"[地图选择]第{stage}阶段完成，准备刷图H1-1");
						return 85;
					case 3:
						Out.Log($"[地图选择]第{stage}阶段完成，准备解锁到2-6");
						taskMap = TaskUtils.GetTaskMap();
						if (taskMap != -1)
						{
							Out.Log($"[地图选择]第{stage}阶段完成，准备自动主线");
							return this.EnsureMapHasUnlock(taskMap);
						}
						break;
					case 4:
						Out.Log($"[地图选择]第{stage}阶段完成，准备刷神秘人");
						return 72;
					case 5:
						Out.Log($"[地图选择]第{stage}阶段完成，准备刷图H1-1");
						return 85;
					case 6:
						Out.Log($"[地图选择]第{stage}阶段完成，准备刷图2-3");
						return 69;
					case 7:
						Out.Log($"[地图选择]第{stage}阶段完成，准备刷神秘人");
						return 72;
					case 9:
						Out.Log($"[地图选择]第{stage}阶段完成，准备刷图H1-1");
						return 85;
					case 10:
						Out.Log($"[地图选择]第{stage}阶段完成，准备解锁到4-1");
						taskMap = TaskUtils.GetTaskMap();
						if (taskMap != -1)
						{
							Out.Log($"[地图选择]第{stage}阶段完成，准备自动主线");
							return this.EnsureMapHasUnlock(taskMap);
						}
						break;
					case 11:
						LettuceMercenary mercenary = HsGameUtils.GetMercenary(MercConst.拉格纳罗斯);
						if (!mercenary.m_owned)
						{
							Out.Log($"[地图选择]第{stage}阶段完成，准备刷拉格纳罗斯");
							return 75;
						}
						mercenary = HsGameUtils.GetMercenary(MercConst.迦顿男爵);
						if (!mercenary.m_owned)
                        {
							Out.Log($"[地图选择]第{stage}阶段完成，准备刷迦顿男爵");
							return 74;
						}
						Out.Log($"[地图选择]第{stage}阶段完成，准备刷安东尼");
						return 76;
					case 12:
						Out.Log($"[地图选择]第{stage}阶段完成，准备刷图H1-1");
						return 85;
					case 13:
                    case 15:
                        Out.Log($"[地图选择]第{stage}阶段完成，准备刷神秘人");
						return 72;
					case 8:
					case 14:
						taskMap = TaskUtils.GetTaskMap();
                        if (taskMap != -1)
						{
							Out.Log($"[地图选择]第{stage}阶段完成，准备佣兵任务");
							return this.EnsureMapHasUnlock(taskMap);
						}
						Out.Log("没约定特定的地图，我要去2-5混了");
						return this.EnsureMapHasUnlock(MapUtils.GetMapId("2-5"));
					case 16:
						Out.Log($"[地图选择]第{stage}阶段完成，准备刷图H1-1");
						return 85;
					case 17:
						Out.Log($"[地图选择]第{stage}阶段完成，准备解锁到冰火装备地图");
                        taskMap = TaskUtils.GetTaskMap();
						if (taskMap != -1)
						{
							return this.EnsureMapHasUnlock(taskMap);
						}
						break;
					case 18:
						InitMap();
						if (unlockMapID.Count > 0)
						{
							Out.Log($"[地图选择]第{stage}阶段完成，准备解锁巴琳达，雪王装备装备");
							Out.Log(string.Format("自动解锁装备地图ID:{0}", unlockMapID[0]));
							return unlockMapID[0];
						}
						else
						{
							Out.Log(string.Format("无待解锁装备地图"));
							return MapUtils.GetUnCompleteMap();
						}
					case 19:
						Out.Log($"[地图选择]第{stage}阶段完成，准备刷神秘人");
						return 72;
					case 20:
						Out.Log($"[地图选择]第{stage}阶段完成，准备主线任务");
						taskMap = TaskUtils.GetTaskMap();
						if (taskMap != -1)
						{
							Out.Log($"[地图选择]第{stage}阶段完成，准备自动主线");
							return this.EnsureMapHasUnlock(taskMap);
						}
						break;
					case 21:
						InitMap();
						if (unlockMapID.Count > 0)
						{
							Out.Log($"[地图选择]第{stage}阶段完成，准备解锁装备");
							Out.Log(string.Format("自动解锁装备地图ID:{0}", unlockMapID[0]));
							return unlockMapID[0];
						}
						else
						{
							Out.Log(string.Format("无待解锁装备地图"));
							return MapUtils.GetUnCompleteMap();
						}
					default:
						Out.Log("[地图选择]第20阶段完成，准备刷图H1-1");
						return 85;
				}
				Out.Log("你这是来干嘛的?");
				return 85;
			}
			else
			{
				Map map = MapUtils.GetMap(Main.mapConf.Value);
				if (map == null)
				{
					UIStatus.Get().AddInfo("地图查找失败，自动关闭插件");
					Main.isRunning = false;
					return result;
				}
				LettuceBountyDbfRecord record = GameDbf.LettuceBounty.GetRecord(map.ID);
				if (record == null)
				{
					UIStatus.Get().AddInfo("地图查找失败，自动关闭插件");
					Main.isRunning = false;
					return result;
				}
				while (record.RequiredCompletedBounty > 0 && !MercenariesDataUtil.IsBountyComplete(record.RequiredCompletedBounty))
				{
					record = GameDbf.LettuceBounty.GetRecord(record.RequiredCompletedBounty);
				}
				return this.EnsureMapHasUnlock(record.ID);
			}
		}

		// Token: 0x06000023 RID: 35 RVA: 0x00003638 File Offset: 0x00001838
		private void CheckIdleTime()
		{
			Main.idleTime += Time.deltaTime;
			if (Main.modeConf.Value != Mode.PVP && Main.idleTime > 1200f)
			{
				if (Main.idleTime > 1320f)
				{
                    Out.Log("[IDLE] 1320s 游戏关闭");
                    Application.Quit();
				}
				if (GameState.Get() != null)
				{
					Out.Log("[IDLE]1200s 投降");
					GameState.Get().Concede();
				}
			}
			if (Main.idleTime > 1200f)
			{
				if (Main.idleTime > 2400f)
				{
					Out.Log("[IDLE] 1260s 游戏关闭");
					Application.Quit();
				}
				if (GameState.Get() != null)
				{
					Out.Log("[IDLE] 1200s 投降");
					GameState.Get().Concede();
					HsGameUtils.GotoSceneVillage();
					Out.Log("可能是卡死了，返回村庄看看");
					Main.Sleep(1);
				}
			}
		}

		// Token: 0x06000024 RID: 36 RVA: 0x000036EC File Offset: 0x000018EC
		private static void ResetIdle()
		{
			//Out.Log("[IDLE] reset");
			Main.idleTime = 0f;
		}

		// Token: 0x06000025 RID: 37 RVA: 0x00003702 File Offset: 0x00001902
		private void OnDestroy()
		{
			if (!Main.isPluginEnable.Value) return;
			else
            {
				HsMod.PluginConfig.isPluginEnable.Value = true;
            }
			this._harmony.UnpatchSelf();
		}

		// Token: 0x06000026 RID: 38 RVA: 0x00003710 File Offset: 0x00001910
		private void HandlePlay()
		{
			if (Main.autoTimeGearConf.Value && HsMod.ConfigValue.Get().TimeGearValue <= 4) 
					HsMod.ConfigValue.Get().TimeGearValue = 8;
            if (Main.phaseID == 3)
			{
				return;
			}
			if (Main.phaseID == 0)
			{
				NetCache.NetCacheMercenariesPlayerInfo mercenariesPlayerInfo = NetCache.Get()?.GetNetObject<NetCache.NetCacheMercenariesPlayerInfo>();
				
				if (Main.modeConf.Value == Mode.PVP)
				{
					try
					{
						string name = HsMod.ConfigValue.Get().CacheOpponentFullName;
						if (!string.IsNullOrEmpty(name))
						{
                            Console.WriteLine($"{name},{isFriend(name)}");
                            Out.Log($"{name},{isFriend(name)}");
                            if (isFriend(name))
							{
                                Out.Log("匹配到群友，我自愿烧绳");
                                Main.Sleep(10);
                                return;
                            }
						}
					}
					catch (Exception e)
					{
						Out.Log(e.ToString());
					}
				}
				//Out.Log("[对局中] 回合结束");
				InputManager.Get().DoEndTurnButton();
				return;
			}

			if (EndTurnButton.Get().m_ActorStateMgr.GetActiveStateType() == ActorStateType.ENDTURN_NO_MORE_PLAYS)
			{
				NetCache.NetCacheMercenariesPlayerInfo mercenariesPlayerInfo = NetCache.Get()?.GetNetObject<NetCache.NetCacheMercenariesPlayerInfo>();
                if (Main.modeConf.Value == Mode.PVP)
                {
                    try
                    {
                        string name = HsMod.ConfigValue.Get().CacheOpponentFullName;
                        if (!string.IsNullOrEmpty(name))
                        {
                            if (isFriend(name))
                            {
                                Out.Log("匹配到群友，我自愿烧绳");
                                Console.WriteLine("匹配到群友，我烧绳");
                                Main.Sleep(10);
                                return;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Out.Log(e.ToString());
                    }
                }
                //Out.Log("[对局中] 点击结束按钮");
                InputManager.Get().DoEndTurnButton();
				return;
			}
			int stage = OnePackageService.GetsTheCurrentStage();
			// 策略计算
			List<BattleTarget> battleTargets = 
				((Main.modeConf.Value == Mode.佣兵任务 
				|| Main.modeConf.Value == Mode.一条龙 &&(stage == 8 || stage == 14) ) 
				? StrategyHelper.GetStrategy("_Sys_Default") : 
				StrategyHelper.GetStrategy(Main.strategyConf.Value)).GetBattleTargets(
				GameState.Get().GetTurn(),
				this.BuildTargetFromCards(ZoneMgr.Get().FindZoneOfType<ZonePlay>(Player.Side.OPPOSING).GetCards(), Player.Side.OPPOSING),
				this.BuildTargetFromCards(ZoneMgr.Get().FindZoneOfType<ZonePlay>(Player.Side.FRIENDLY).GetCards(), Player.Side.FRIENDLY),
				this.BuildTargetFromCards(ZoneMgr.Get().FindZoneOfType<ZoneGraveyard>(Player.Side.OPPOSING).GetCards(), Player.Side.OPPOSING)
				);
			Dictionary<int, BattleTarget> dict = battleTargets.FindAll((BattleTarget i) => i.SkillId != -1).ToDictionary((BattleTarget i) => i.SkillId, (BattleTarget i) => i);
			foreach (BattleTarget battleTarget in battleTargets)
			{
				if (battleTarget.SkillId == -1)
					continue;
				if (!dict.ContainsKey(battleTarget.SkillId))
					dict.Add(battleTarget.SkillId, battleTarget);
			}

			// 选择目标阶段
			if (GameState.Get().GetResponseMode() == GameState.ResponseMode.OPTION_TARGET)
			{
				if(Main.isStrategyEnable.Value)
					foreach (BattleTarget battleTarget in battleTargets)
						Out.Log(string.Format("[对局中] 策略判断 [SkillId:{0}] [TargetId:{1}] [TTYPE:{2}]",
							battleTarget.SkillId, battleTarget.TargetId, battleTarget.TargetType));
			   
				List<Card> cards_opposite = ZoneMgr.Get().FindZoneOfType<ZonePlay>(Player.Side.OPPOSING).GetCards().FindAll((Card i) => (i.GetActor().GetActorStateType() == ActorStateType.CARD_VALID_TARGET || i.GetActor().GetActorStateType() == ActorStateType.CARD_VALID_TARGET_MOUSE_OVER) && !i.GetEntity().IsStealthed());
				List<Card> cards_friend = ZoneMgr.Get().FindZoneOfType<ZonePlay>(Player.Side.FRIENDLY).GetCards().FindAll((Card i) => (i.GetActor().GetActorStateType() == ActorStateType.CARD_VALID_TARGET || i.GetActor().GetActorStateType() == ActorStateType.CARD_VALID_TARGET_MOUSE_OVER));
				string strlog = "";
				if (Main.isStrategyEnable.Value)
                {
					foreach (Card card1 in cards_opposite)
						strlog += string.Format("{0}({1},{2})\t",
							card1.GetEntity().GetEntityId(), card1.GetEntity().GetCurrentHealth(), card1.GetEntity().GetDefHealth());
					Out.Log(string.Format("[对局中] 场面：敌方 {0}", strlog));
					strlog = "";
					foreach (Card card1 in cards_friend)
						strlog += string.Format("{0}({1},{2})\t",
							card1.GetEntity().GetEntityId(), card1.GetEntity().GetCurrentHealth(), card1.GetEntity().GetDefHealth());
					Out.Log(string.Format("[对局中] 场面：友方 {0}", strlog));
				}
					
				//这个是当前停留的技能id
				Network.Options.Option.SubOption networkSubOption = GameState.Get().GetSelectedNetworkSubOption();
				if (Main.isStrategyEnable.Value)
					Out.Log(string.Format("[对局中] 技能目标 当前技能 [SID:{0}]",networkSubOption.ID));
				Card card = null;
				// 先
				if (dict.ContainsKey(networkSubOption.ID) && dict[networkSubOption.ID].TargetId != -1)
				{
					if (Main.isStrategyEnable.Value)
						Out.Log("[对局中] 技能目标 匹配策略");
					if (dict[networkSubOption.ID].TargetType == TARGETTYPE.UNSPECIFIED)
					{
						card = cards_opposite.Find((Card i) => i.GetEntity().GetEntityId() == dict[networkSubOption.ID].TargetId);
						if (card == null)
							card = cards_friend.Find((Card i) => i.GetEntity().GetEntityId() == dict[networkSubOption.ID].TargetId);
					}
					else if (dict[networkSubOption.ID].TargetType == TARGETTYPE.FRIENDLY)
					{
						card = cards_friend.Find((Card i) => i.GetEntity().GetEntityId() == dict[networkSubOption.ID].TargetId);
					}
				}

				if (card == null && cards_opposite.Count != 0)
				{
                    if (Main.isStrategyEnable.Value)
                        Out.Log("[对局中] 技能目标 敌方1号位");

                    card = cards_opposite[0];
					
				}
				if (card == null && cards_friend.Count != 0)
				{
                    if (Main.isStrategyEnable.Value)
                        Out.Log("[对局中] 技能目标 友方首位");

					card = cards_friend[0];
				}
				if (card == null)
				{
					Out.Log("[对局中] 技能目标 无可用目标 过");
					InputManager.Get().DoEndTurnButton();
				}
				Traverse.Create(InputManager.Get()).Method("DoNetworkOptionTarget", new object[]
				{
					card.GetEntity()
				}).GetValue();
			}
			if (GameState.Get().GetResponseMode() == GameState.ResponseMode.OPTION)
			{

				ExpFlag = false;
				if (Main.autoConcede.Value && Main.modeConf.Value == Mode.PVP)
				{
					GameState.Get().Concede();
					Out.Log("[配置] Pvp自动投降");
				}
				if(Main.modeConf.Value == Mode.PVP && GameState.Get().GetTurn() >= 3)
                {
					string name = HsMod.ConfigValue.Get().CacheOpponentFullName;
					try
					{

                       if (!string.IsNullOrEmpty(name)
                            && (GetMd5Hash(abstractName(name)) == "e59e4415210dbbdda6e2e77027d4668e"
                            || GetMd5Hash(abstractName(name)) == "9b2dd2ace4fae2ba85df02e3ebf3f89e"))
                        {
							Out.Log("匹配到君莫笑，我自愿投降");
                            GameState.Get().Concede();
                        }
                        
					}catch(Exception e)
                    {
						Out.Log(e.ToString());
                    }

				}
				
				if (modeConf.Value == Mode.挂机收菜 && readyToHang == true)
				{
					System.Random rd = new System.Random();
                    awakeTimeIntervalConf.Value = rd.Next(0, 5) + 20;
                    Out.Log(string.Format("[战斗中] 初次进入战斗，休息{0}min后再见~", awakeTimeIntervalConf.Value));
					awakeTimeConf.Value = DateTime.Now.AddMinutes(awakeTimeIntervalConf.Value).ToString("G");
					Application.Quit();
					return;
				}
				try
                {

					//上怪
					if (Main.phaseID == 1 && EndTurnButton.Get().m_ActorStateMgr.GetActiveStateType() == ActorStateType.ENDTURN_YOUR_TURN)
					{
						
						ZonePlay zonePlay = ZoneMgr.Get().FindZoneOfType<ZonePlay>(Player.Side.FRIENDLY);
						ZoneHand zoneHand = ZoneMgr.Get().FindZoneOfType<ZoneHand>(Player.Side.FRIENDLY);
						if (GameMgr.Get().GetGameType() == GameType.GT_MERCENARIES_PVE && Main.delayTime.Value <= 10)
						{
							InputManager.Get().DoEndTurnButton();
							Main.Sleep(2);
							return;
						}
						if (zoneHand != null)
						{
							(int hand_index, int play_index) = StrategyHelper.GetStrategy(Main.strategyConf.Value).GetEnterOrder(
								BuildTargetFromCards(zoneHand.GetCards(), Player.Side.FRIENDLY),
								BuildTargetFromCards(zonePlay.GetCards(), Player.Side.FRIENDLY)
								);
							GameState gameState = GameState.Get();
							if (gameState != null)
							{
								if (Main.isStrategyEnable.Value)
									Out.Log(string.Format("[佣兵登场] 选择[佣兵{0}:{1}]，位置[{2}]",
										hand_index, zoneHand.GetCardAtIndex(hand_index).GetEntity().GetName(), play_index));
								gameState.SetSelectedOption(hand_index + 1);
								gameState.SetSelectedSubOption(-1);
								gameState.SetSelectedOptionTarget(0);
								gameState.SetSelectedOptionPosition(play_index + 1);
								gameState.SendOption();
								Main.Sleep(Main.delayTime.Value);

							}
							return;
						}
					}
				}
				catch (Exception ex)
				{
					if (Main.isStrategyEnable.Value)
						Out.Log("空间名：" + ex.Source + "；" + '\n' +
								  "方法名：" + ex.TargetSite + '\n' +
								  "故障点：" + ex.StackTrace.Substring(ex.StackTrace.LastIndexOf("\\") + 1, ex.StackTrace.Length - ex.StackTrace.LastIndexOf("\\") - 1) + '\n' +
								  "错误提示：" + ex.Message);

				}

				// 佣兵技能选择
				if (Main.phaseID == 2)
				{
					ZonePlay zonePlay_opposing = ZoneMgr.Get().FindZoneOfType<ZonePlay>(Player.Side.OPPOSING);
					ZonePlay zonePlay_friendly = ZoneMgr.Get().FindZoneOfType<ZonePlay>(Player.Side.FRIENDLY);
					// PVP模式记录对面佣兵的登场情况
					if (GameMgr.Get().GetGameType() == GameType.GT_MERCENARIES_PVP)
					{
						foreach (Card card in zonePlay_opposing.GetCards())
						{
							if (card.GetEntity().GetTag<TAG_ROLE>(GAME_TAG.LETTUCE_ROLE) != TAG_ROLE.CASTER &&
								card.GetEntity().GetTag<TAG_ROLE>(GAME_TAG.LETTUCE_ROLE) != TAG_ROLE.TANK &&
								card.GetEntity().GetTag<TAG_ROLE>(GAME_TAG.LETTUCE_ROLE) != TAG_ROLE.FIGHTER)
								continue;
							string equipment = card.GetEntity().GetEquipmentEntity()?.GetName() ?? "";
							if (equipment.Length > 0 && Char.IsNumber(equipment[equipment.Length - 1]))
								equipment = equipment.Substring(0, equipment.Length - 1);
							if(card.GetEntity().GetName() != "大法师卡德加")
								MercTeam.Add(card.GetEntity().GetName() + '-' + equipment);
						}
					}
					if (null == zonePlay_opposing.GetCards().Find((Card i) => false == i.GetEntity().IsStealthed()))
					{
						Out.Log("[对局中] 他们都藏起来了？？！！");
						InputManager.Get().DoEndTurnButton();
						return;
					}
					Entity currentSelectMerc_Entity = ZoneMgr.Get().GetLettuceAbilitiesSourceEntity();
					if (currentSelectMerc_Entity == null)
					{
						foreach (Card card in zonePlay_friendly.GetCards())
						{
							Entity entity = card.GetEntity();
							if (!entity.HasSelectedLettuceAbility() || !entity.HasTag(GAME_TAG.LETTUCE_HAS_MANUALLY_SELECTED_ABILITY))
							{
								if (Main.isStrategyEnable.Value)
									Out.Log(string.Format("[对局中] 佣兵选择 [{0}]的技能界面", entity.GetName()));
								ZoneMgr.Get().DisplayLettuceAbilitiesForEntity(entity);
								Main.ResetIdle();
								return;
							}
						}
					}
					else
					{
						BattleTarget currentMerc_BattleTarget = battleTargets.Find((BattleTarget i) => i.MercName == currentSelectMerc_Entity.GetName());
						// 策略规定此佣兵可以操作
						if (currentMerc_BattleTarget == null || currentMerc_BattleTarget.NeedActive == true)
						{
							if (Main.isStrategyEnable.Value)
								Out.Log(string.Format("[对局中] 技能选择 操作佣兵 [{0}]", currentSelectMerc_Entity.GetName()));
							Card card = null;
							List<Card> displayedLettuceAbilityCards = ZoneMgr.Get().GetLettuceZoneController().GetDisplayedLettuceAbilityCards();
							foreach (BattleTarget batterTarget in battleTargets)
							{
								card = displayedLettuceAbilityCards.Find((Card i) => i.GetEntity().GetEntityId() == batterTarget.SkillId && GameState.Get().HasResponse(i.GetEntity(), new bool?(false)));
								if (card != null)
									break;
							}
							if (card != null)
							{
								if (Main.isStrategyEnable.Value)
									Out.Log(string.Format("[对局中] 技能选择 匹配策略[{0}]", card.GetEntity().GetName()));
							}
							else
							{
								card = displayedLettuceAbilityCards.Find((Card i) => GameState.Get().HasResponse(i.GetEntity(), new bool?(false)));
								if (Main.isStrategyEnable.Value)
									Out.Log(string.Format("[对局中] 技能选择 顺序选择[{0}]", card.GetEntity().GetName()));
							}
							Traverse.Create(InputManager.Get()).Method("HandleClickOnCardInBattlefield", new object[]
							{
							card.GetEntity(),
							true
							}).GetValue();
							Main.ResetIdle();
							return;
						}
						// 策略规定此佣兵不可以操作
						else
						{
							if (Main.isStrategyEnable.Value)
								Out.Log(string.Format("[对局中] 操作佣兵 设置为不操作[{0}]", currentSelectMerc_Entity.GetName()));
							Dictionary<string, bool> dict_mercactive = new Dictionary<string, bool>();
							foreach (BattleTarget battleTarget in battleTargets)
							{
								if (battleTarget.MercName.Length <= 0)
									continue;
								if (!dict_mercactive.ContainsKey(battleTarget.MercName))
									dict_mercactive.Add(battleTarget.MercName, battleTarget.NeedActive);
							}
							bool result = false;
							Card nextSelectMerc_Card = zonePlay_friendly.GetCards().Find(
								(Card i) =>
								(!i.GetEntity().HasSelectedLettuceAbility() || !i.GetEntity().HasTag(GAME_TAG.LETTUCE_HAS_MANUALLY_SELECTED_ABILITY)) &&
								(false == dict_mercactive.TryGetValue(i.GetEntity().GetName(), out result) || result == true)
								);
							if (nextSelectMerc_Card != null)
							{
								if (Main.isStrategyEnable.Value)
									Out.Log(string.Format("[对局中] 操作佣兵 手动选择下一个佣兵[{0}]", nextSelectMerc_Card.GetEntity().GetName()));
								ZoneMgr.Get().DisplayLettuceAbilitiesForEntity(nextSelectMerc_Card.GetEntity());
								Main.ResetIdle();
								return;
							}
							else
							{
								if (Main.isStrategyEnable.Value)
									Out.Log(string.Format("[对局中] 操作佣兵 无可操作佣兵 结束回合"));
								InputManager.Get().DoEndTurnButton();
								Main.ResetIdle();
								return;
							}
						}
					}
				}
			}
			// 抉择
			if (GameState.Get().GetResponseMode() == GameState.ResponseMode.SUB_OPTION)
			{
				List<Card> friendlyCards = ChoiceCardMgr.Get().GetFriendlyCards();
				int skill_id = ChoiceCardMgr.Get().GetSubOptionParentCard().GetEntity().GetEntityId();
				
				Card card = null;
				if (dict.ContainsKey(skill_id))
				{
					if (dict[skill_id].SubSkillIndex != -1)
						card = friendlyCards.ElementAtOrDefault(dict[skill_id].SubSkillIndex);
					else if (dict[skill_id].SubSkillName != "")
						card = friendlyCards.Find((Card x) => x.GetEntity().GetName().Contains(dict[skill_id].SubSkillName));
				}
				if (card == null)
					card = friendlyCards.Last();
				if (Main.isStrategyEnable.Value)
					Out.Log($"[对局中] 技能选择 使用抉择技能 {card.GetEntity().GetName()}");
				InputManager.Get().HandleClickOnSubOption(card.GetEntity(), false);
				Main.ResetIdle();
				return;
			}
		}

		private List<Target> BuildTargetFromCards(List<Card> cards, Player.Side side)
		{
			List<Target> list = new List<Target>();
			foreach (Card card in cards)
			{
				bool flag_avalue = card.GetActor().GetActorStateType() == ActorStateType.CARD_VALID_TARGET || card.GetActor().GetActorStateType() == ActorStateType.CARD_VALID_TARGET_MOUSE_OVER;
				if (side != Player.Side.FRIENDLY)
					flag_avalue = flag_avalue && !card.GetEntity().IsStealthed();
				List<Skill> skills = new List<Skill>();
				foreach (int id in card.GetEntity().GetLettuceAbilityEntityIDs())
				{
					Entity entity2 = GameState.Get().GetEntity(id);
					if (entity2 != null && !entity2.IsLettuceEquipment())
					{
						//有些技能没有等级，不删掉后面的数字
						string tmpName = entity2.GetName();
						if (tmpName.Length > 0 &&
							Char.IsNumber(tmpName[tmpName.Length - 1]))
							tmpName = entity2.GetName().Substring(0, entity2.GetName().Length - 1);
						skills.Add(new Skill
						{
							Name = tmpName,
							Id = entity2.GetEntityId()
						});
					}
				}
				Target item = new Target
				{
					Name = card.GetEntity().GetName(),
					Id = card.GetEntity().GetEntityId(),
					Attack = card.GetEntity().GetATK(),
					Health = card.GetEntity().GetCurrentHealth(),
					Speed = card.GetPreparedLettuceAbilitySpeedValue(),
					DefHealth = card.GetEntity().GetDefHealth(),
					Role = (HsMercenaryStrategy.TAG_ROLE)(card.GetEntity().GetTag<TAG_ROLE>(GAME_TAG.LETTUCE_ROLE)),
					Enable = flag_avalue,
					Skills = skills,
				};
				list.Add(item);
			}
			return list;
		}

        public static string GetMd5Hash(string input) // MD5加密
        {
            MD5 md5Hash = MD5.Create();
            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();
            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
		private static string abstractName(string name)
		{
			int index = 0;
			for(int i = 0; i < name.Length; i++)
			{
				if (name[i] == '#')
				{
                    index = i;
					break;
                }
			}
			if(index > 0)
				return name.Substring(0,index);
			return "官方AI";
		}
		private static bool isFriend(string name)
		{
			string oppName = abstractName(name);
			string MD5password = GetMd5Hash(oppName);
			if(password.Contains(MD5password))
			{
				return true;
			}
			return false;
		}
		private static string generatePassword(string name)
		{
			return GetMd5Hash(name);
		}
        // Token: 0x0600002A RID: 42 RVA: 0x00003F04 File Offset: 0x00002104
        private static ValueTuple<LettuceMapNode, int> GetNextNode(List<LettuceMapNode> nodes, List<LettuceMapNode> allNodes)
		{
			int num = int.MaxValue;
			LettuceMapNode item = nodes[0];
			foreach (LettuceMapNode lettuceMapNode in nodes)
			{
				int minNode = Main.GetMinNode(lettuceMapNode, 0, allNodes);
				if (minNode != -1 && minNode < num)
				{
					item = lettuceMapNode;
					num = minNode;
				}
			}
			if (num != 2147483647)
			{
				return new ValueTuple<LettuceMapNode, int>(item, num);
			}
			return new ValueTuple<LettuceMapNode, int>(null, 0);
		}
		//初始化 解锁装备 //5.7改动
		public static bool InitMap()
		{
			Dictionary<int, int> equipMapId = new Dictionary<int, int>();
			equipMapId.Clear();
			var equipTierByMap = GameDbf.BonusBountyDropChance.GetRecords();
			foreach (var item in equipTierByMap)
			{
				var tierRecord = GameDbf.LettuceEquipmentTier.GetRecord(item.LettuceEquipmentTierId);
				if (tierRecord != null)
				{
					LettuceBountyDbfRecord record = GameDbf.LettuceBounty.GetRecord(item.LettuceBountyRecord.ID);
					if (record.RequiredCompletedBounty > 0 && !MercenariesDataUtil.IsBountyComplete(record.RequiredCompletedBounty))
					{ //地图未解锁
						continue;
					}
					else
					{
						equipMapId.Add(tierRecord.LettuceEquipmentId, item.LettuceBountyRecord.ID);
						//Out.Log(string.Format("待解锁的装备ID:{0},待解锁的地图ID：{1}",tierRecord.LettuceEquipmentId, item.LettuceBountyRecord.ID));
					}
				}
			}
			unlockMercID.Clear();
			unlockMapID.Clear();
			List<LettuceMercenary> mercenaries = CollectionManager.Get().FindOrderedMercenaries(null, new bool?(true), null, null, null).m_mercenaries;

			foreach(var MercID in MercConst.First)
            {	//遍历优先解锁佣兵队列
				LettuceMercenary mercenary2 = HsGameUtils.GetMercenary(MercID);
				foreach (var equip in mercenary2.m_equipmentList) // 遍历当前佣兵的装备列表
				{
					if (!equip.Owned && mercenary2.m_owned) // 如果当前装备没有获得 && 当前佣兵已经合成
					{
						if (equipMapId.ContainsKey(equip.ID)) //如果未获得的装备已经在字典中
						{
							//将佣兵id + 地图id 加入队列
							unlockMercID.Enqueue(mercenary2.ID);
							unlockMapID.Enqueue(equipMapId[equip.ID]);
							//Out.Log(string.Format("待解锁的佣兵：{0} ID: {1} 地图ID: {2}",merc.m_mercName,merc.ID, equipMapId[equip.ID]));

						}
					}
				}
			}
			foreach (var merc in mercenaries) // 遍历所有的佣兵
			{
				if(!MercConst.First.Contains(merc.ID))//如果这个佣兵不是优先解锁
                {
					foreach (var equip in merc.m_equipmentList) // 遍历当前佣兵的装备列表
					{
						if (!equip.Owned && merc.m_owned) // 如果当前装备没有获得 && 当前佣兵已经合成
						{
							if (equipMapId.ContainsKey(equip.ID)) //如果未获得的装备已经在字典中
							{
								//将佣兵id + 地图id 加入队列
								unlockMercID.Enqueue(merc.ID);
								unlockMapID.Enqueue(equipMapId[equip.ID]);
								//Out.Log(string.Format("待解锁的佣兵：{0} ID: {1} 地图ID: {2}",merc.m_mercName,merc.ID, equipMapId[equip.ID]));

							}
						}
					}
				}
			}
			if (unlockMercID.Count > 0) return true;
			return false;
		}
		
		//领取成就装备
		public static void AckMercFullLevel()
        {
			_AckByAchSection(AchSection.MercFullLevel);
			//Network.Get().GetAccountLicenseAchieveResponse();
			
        }
		private static void _AckByAchSection(AchSection achSec)
        {

			List<AchievementDbfRecord> record = GameDbf.Achievement.GetRecords(x => x.AchievementSection == (int)achSec);
			var achievementManager = AchievementManager.Get();
			List<AchievementDataModel> list = new List<AchievementDataModel>();
			foreach(var item in record)
            {
				list.Add(achievementManager.GetAchievementDataModel(item.ID));
            }
			foreach(var item in list)
            {
				if(item.Status == AchievementManager.AchievementStatus.COMPLETED)
                {
					Network.Get().ClaimAchievementReward(item.ID);
					Out.Log($"[成就系统] {item.Name}:{item.Description} 自动领取");
                }
            }
        }
		private enum AchSection
        {
			MercFullLevel = 327,
        }


		// Token: 0x0600002B RID: 43 RVA: 0x00003F8C File Offset: 0x0000218C
		private static bool NeedCompleted()
		{   //5.7改动
			if (Main.modeConf.Value == Mode.主线任务
			|| Main.modeConf.Value == Mode.解锁装备
			|| Main.modeConf.Value == Mode.刷图
			|| Main.modeConf.Value == "挂机收菜"
			|| TaskUtils.GetTaskMap() != -1)
            {
				//Out.Log("需要完成地图");
                return true;
            }
			else if(Main.modeConf.Value == Mode.一条龙)
            {
				int stage = OnePackageService.GetsTheCurrentStage();
				if (stage != 4 && stage != 7 && stage != 8 && stage != 13 && stage != 15 && stage != 19)
				{
					return true;
				}
            }
			return false;
		}
		// Token: 0x0600002C RID: 44 RVA: 0x00003FC8 File Offset: 0x000021C8
		// 最短路径
		private static int GetMinNode(LettuceMapNode node, int value, List<LettuceMapNode> nodes)
		{
            int stage = OnePackageService.GetsTheCurrentStage();
            //全自动做任务，如果有赐福，需要走对应的点
            if (Main.modeConf.Value == Mode.佣兵任务)
			{
				if ((TaskUtils.HaveTaskDocter && HsGameUtils.IsDoctor(node.NodeTypeId)) ||
					(TaskUtils.HaveTaskFighter && HsGameUtils.IsFighter(node.NodeTypeId)) ||
					(TaskUtils.HaveTaskCaster && HsGameUtils.IsCaster(node.NodeTypeId)) ||
					(TaskUtils.HaveTaskTank && HsGameUtils.IsTank(node.NodeTypeId)))
					return value;
			}
			else if(Main.modeConf.Value == Mode.一条龙 && (stage == 8 || stage == 14))
			{
                if ((TaskUtils.HaveTaskDocter && HsGameUtils.IsDoctor(node.NodeTypeId)) ||
                    (TaskUtils.HaveTaskFighter && HsGameUtils.IsFighter(node.NodeTypeId)) ||
                    (TaskUtils.HaveTaskCaster && HsGameUtils.IsCaster(node.NodeTypeId)) ||
                    (TaskUtils.HaveTaskTank && HsGameUtils.IsTank(node.NodeTypeId)))
                    return value;
            }
			// 需要不需要完成地图，只打到神秘人
			if (!Main.NeedCompleted())
			{
				if (HsGameUtils.IsMysteryNode(node.NodeTypeId))
				{
					return value;
				}
				if (HsGameUtils.IsBoss(node.NodeTypeId))
				{
					return -1;
				}
			}
			else
			{
				if (HsGameUtils.IsBoss(node.NodeTypeId))
				{
					return value;
				}
			}
			int num = 0;
			
			if (Main.modeConf.Value == Mode.主线任务 
			|| Main.modeConf.Value == Mode.解锁装备 
			|| Main.teamNameConf.Value == "刷图"
			|| Main.modeConf.Value == Mode.一条龙
			&&(stage == 3 || stage == 10 || stage == 17 || stage == 18 || stage == 20 || stage == 21))
			{   
				if (HsGameUtils.IsCaster(node.NodeTypeId)
				|| HsGameUtils.IsFighter(node.NodeTypeId)
				|| HsGameUtils.IsTank(node.NodeTypeId)
				|| HsGameUtils.IsMysteryNode(node.NodeTypeId))
					num = 99;
				else if (HsGameUtils.IsMonster(node.NodeTypeId))
					num = 1;
				else num = 0;
			}
            else if(HsGameUtils.IsMysteryNode(node.NodeTypeId)) num = -3;
			else 
            {
                num = (!HsGameUtils.IsMonster(node.NodeTypeId)) ? 0 : 1;
            }
				

			if (node.ChildNodeIds.Count == 1)
			{
				return Main.GetMinNode(nodes[(int)node.ChildNodeIds[0]], value + num, nodes);
			}
			int minNode = Main.GetMinNode(nodes[(int)node.ChildNodeIds[0]], value + num, nodes);
			int minNode2 = Main.GetMinNode(nodes[(int)node.ChildNodeIds[1]], value + num, nodes);
			if (minNode == -1)
			{
				return minNode2;
			}
			if (minNode2 == -1)
			{
				return minNode;
			}
			return Math.Min(minNode, minNode2);
		}
		// Token: 0x04000008 RID: 8
		private readonly Harmony _harmony = new Harmony("hs.patch");

		// Token: 0x04000009 RID: 9
		private static bool isRunning = true;

		
		private static ConfigEntry<bool> isStrategyEnable;
		private static ConfigEntry<bool> isPluginEnable;

		private static ConfigEntry<bool> autoTimeGearConf;

		// Token: 0x0400000A RID: 10
		private static ConfigEntry<string> mapConf;

		// Token: 0x0400000B RID: 11
		private static ConfigEntry<string> teamNameConf;

		// Token: 0x0400000C RID: 12
		private static ConfigEntry<bool> autoUpdateSkillConf;

		// Token: 0x0400000D RID: 13
		private static ConfigEntry<bool> autoCraftConf;

		private static ConfigEntry<bool> autoAckConf;
		//自动投降开关
		private static ConfigEntry<bool> autoConcede;

		//PVP随机换队
		private static ConfigEntry<bool> PvpRandomTeam;


		// Token: 0x0400000E RID: 14
		private static ConfigEntry<int> coreTeamNumConf;

		// Token: 0x0400000F RID: 15
		private static ConfigEntry<int> teamNumConf;

		// Token: 0x04000010 RID: 16
		private static ConfigEntry<string> modeConf;

		// Token: 0x04000011 RID: 17
		private static ConfigEntry<bool> runningConf;

		// Token: 0x04000012 RID: 18
		private static ConfigEntry<string> strategyConf;

		// Token: 0x04000013 RID: 19
		private static ConfigEntry<string> cleanTaskConf;

		private static ConfigEntry<string> awakeTimeConf;

		private static ConfigEntry<int> awakeTimeIntervalConf;

		private static ConfigEntry<int> delayTime;


		// Token: 0x04000014 RID: 20
		private static float sleepTime;

		// Token: 0x04000015 RID: 21
		private static float idleTime;

		// Token: 0x04000016 RID: 22
		private static bool initialize;

		// Token: 0x04000017 RID: 23
		private static int phaseID;

		// 挂机收菜模式 下次战斗准备挂机
		private static bool readyToHang = false;

		//showinfo佣兵阵容
		//Pvp胜利次数
		private static int PvpWin = 0;
		//Pvp失败次数
		private static int PvpLose = 0;

		//佣兵阵容的集合
		private static HashSet<string> MercTeam = new HashSet<string>();
		//计算经验效率
		private static DateTime dateTimePre = DateTime.Now;
		private static int totalExp = 0;
		private static bool ExpFlag = false;
		//每级经验
		private static readonly List<int> expLevels = new List<int>()
		{
			-1,
			0,100,100,150,150,225,225,300,300,325,
			325,350,350,375,375,400,400,425,425,450,
			450,550,600,650,675,675,875,875,1000,1100,
			1200,1200,1250,1250,1300,1300,1350,1350,1400,1400,
			1450,1450,1500,1500,1550,1550,1600,1600,1650,1650,
			1700,1700,1750,1750,1800,1800,1850,1850,1900,1900,
			1950,1950,2000,2000,2050,2050,2125,2125,2250,2250,
			2375,2375,2500,2500,2500,2500,2500,2500,2500,2500,
			2500,2500,2500,2500,2500,2500,2500,2500,2500,2500,
			2500,2500,2500,2500,2500,2500,2500,2500,2500,2500,

			1325,1325,1325,1325,1325,1350,1350,1350,1350,1375,
			1375,1400,1400,1400,1400,1400,1400,1400,1425,1425,
			1450,1450,1450,1450,1450,1450,1475,1475,1475,1475,
			1500
		};
		private static readonly List<string> StageInfo = new List<string>()
		{
			"",
			"凯豪泰泽4人30级",
			"解锁剑圣",
            "凯豪泰泽剑5人30级",
			"解锁到2-6",
			"刷神秘人获得雪王晨拥",
			"豪雪晨3人碎片到2000",
			"刷2-3获取大德",
			"刷神秘人获取自然队",
			"获取玛法里奥装备3",
			"刷自然队碎片到1000",
			"解锁到4-1",
			"刷火焰队",
			"刷火焰队碎片到1000",
			"刷神秘人获得一半史诗配合佣兵",
			"佣兵任务栏不空，佣兵任务",
			"预设卡组不齐，刷神秘人",
			"刷预设卡组+1+5",
			"解锁到冰火队的装备地图",
			"解锁冰火队装备",
			"刷神秘人到全佣兵",
			"完成所有主线任务",
			"解锁所有装备",
			"刷H1-1"
        };
        private static readonly List<string> password = new List<string>()
        {
            "e59e4415210dbbdda6e2e77027d4668e","549e1f5b6c848e1bf628a043a4d74060","d1a13936d973419ba69a81428e155fac",
            "9b2dd2ace4fae2ba85df02e3ebf3f89e","3730306654443319870a495d61c68593","ac11ec49b2c391697fa6d06495ceb696",
            "c378bc48797af742b0dd078460dddc19","e4dbef5d71f00d151d26e6077b3d798e","dd87fea1f10479d4b1748c2e3ef11842",
			"46ad3d305e428b65e277d1eb8357fb7e","bd25cc1e544953e985f171f757b6949c","868bf79011489fd77f06d12b1232de9e",
			"e0cf6dc05c207049a1f2adad3435652a","fc3629baf5c4d3deeb392873899d785a","94398de6ca22e821cdbedb5752f621ba",
			"76f5cf6d1f39661c64995b7c055bd22c","85caab9f87aaab2166693ab8a1aeaa97","3a868389c81d0af768cecaeb45ee28fa",
			"b13161c776ddcc940b9976e89d5d8da4","85744b9fc3bc8d99c7cb9d3c5c069cea","bb13b4a6c3091416448ae99c4a60ae33",
            "8307da9b887667b5da23b796757c32fb","e32582b4a41306e04bdd66d01405938d","69d1365f41ac70af52f097135f56afb3",
            "6f6dd239f1f828d27199961b857e45f8","6c25cdff46b3a5129c1cd2aad1f05f43","7ccf23ce69b2d45e7a5ec0951cd9fb88",
            "f32f182e7de6d41f0c81a7d72b4d2072","e5444c8407ae6b4c0e55d17d795489a0","2102979ed77005432e45a69710633e71",
            "a2041ebdf2824a692273be908257c476","12617ba0ce3b7b8aa114328b38574eb1","b723cd2d18850dd672cf9056c5312a47",
            "7d965c2a8296db79adf03c7ae97b0446","8b54675e586e42b02f2940fabb279790","c0267dded6bb164cd045a9c351be90cf",
            "1c85f85a7eb301a439ee82eed0731adc","51a7956d8e6a66e7593ac014a448a4cd","9fd15e5505cf39bb658f5a2b6e28f406",
            "e125cfc4d6707f1bbb3e778292d58170","742dd5ff69e316542cdce6abdee2fbd0","a3b94d8f86e879d1470f0361d87cc453",
            "b2133f2f5b886d31782f4f8f2e45e6af","b1b6bbbf5359f599614129d952bee455","4edbe44ff3f3c673a8d63053aa23fb3b",
            "5e876eeec50e4e223d4bc57ff1c5f62b","a9eb79761cc8d2677a502e82cb350140","a5894c7a28e16ab551e3e51bf79e3426",
            "ac3bcd9023935fa91fd3a8ea562f19d8","fa9b17f0469e950d5046bbaefecec61f","5d05b18d2cfd797e53a27134a7bfb79c",
            "d61641f9ba5c693003a33f6e22342798","0662cb932489cb269a4d76ea62b6cfa5","a27dcf15095cf92c1c8280fa5585e07a",
            "fbd80d0cb1baf3bcf1f23dc081a4eaf9","a93912e8c50fad71eeb383568b90df62","b8eae10f48b831bca4f30223aecb3adc",
            "0a89c5b55dbdea08ca7c089550e92140","12a2a8bb2bd916affda76a2cb2ec5fc7","018063ec1edbad2eda042a4267dbde45",
            "5f14b2f77d1dfbaaff3d965e9d35a764","c2e42fddb3f99e192864ddf5789c9a27","07716070b3424f1256eea20c64ff5f79",
            "fc595de063716d651b8715f4343eb0f7","d3576ecdb4797102ffa21b0158f059e6","2b9a191ac642de60f22b8f187e1b02c2",
            "e9a0216d64df3c7c1589e3aa7af19480","ce6d1510b0317ca47c762613300a609e","75e7263a0850ef465b366d19bb13ad31",
            "eafa867fd2b1f89d0d698469bc558426","33b138a3542f45be2caa11575c96ed79","48330b845f20ff9f78797a93df5d758c",
            "e605300c2ed821a63bd245c531c81da9","1f23f6d907def401dd7fc73d1b955288","43d3fdb0bcc879e9259ebbc9215c2e5d",
            "2177e3f11461d7fdf269a4b6ac2c4f2a","bf43009ec621e5d8e507070bfc035a88","5354fc230b3621d78357549a6cef88af",
            "4e0a227cb76aa7f280d71c489d8b66e5","b8cc7498be2949657b1cf0760acc9efc","13ec5991696e880e6af793149f2c7a29",
            "20f1aeb7819d7858684c898d1e98c1bb","4f5380c40ade9ac82aca9a689dc401c8","4621b3c0c063338a997e1d14dd34f9c0",
            "d11a19876ee2fffa3829984798e817e7","9516579ca5e6e5e35292419e66402668","6cb4fd5fbefa4575e8a8da197b9bf838",
            "8e4c05f07d8a40d3f48aa4ace45ae645","cb97d25ef8a0af512c53edd59827cdc6","ab842921cff119a3f4ba975812286223",
            "c7adad4eb52581963e56e8ebcf9c0478","4139a4ed83c60ac44a07eec1ab4e6c5b","cc96d05fd5e74bcf6d1d0249a863954f",
            "1ff6c2f06db1c068ddd538db8d21cf33","047af93bb84fc9f01ae17f00368df1cf","c8ba5fbdf207172a52aa6acb77ae87ff",
            "3bc7a3dbe693711ce031fa5a626fd0b2","98696e4298e49480c2258438575a677f","fc8bffe75c5d6c0e63b37d75eba161fb",
            "490874499a276625a6377d0bedcdfa52","6ebdbbeb74f84b7f9f30513fca16d78f","1044c23b986c35e9b2912b0ff33f88ce",
            "842619cb95620de1bc3cae8b9ebd1fc9","67c1d756d854fe2caa5720537bf0012a","6472c5c570225ae3c9451055adc2d621",
            "1c97395d32a258cbad573f5fdfb9f182","bf363115a72a5924fd2289da4521b001","ea62e6d252836fc9e411b9a11a2b80e4",
            "9fef4d8b326b365ab7964edd80708126","0bed948acef3d323b9f3e86274d10ede","93ef373b66b2f8f26ffac3307a12318e",
            "a806daa359715c15720b6d855eee95a2","9c2978d69d444f1877a106030b326ac1","dec45081eeab1a670efa646bfe78987a",
            "fc6b6160dd43b01572180f1f68400d5a","39aa619eaaf9ac44e6dbf336b1a79c17","372a79df638cdad3f8fdce16d1dc9a8a",
            "1473ca42a663252dbcf7fd74545c40f9","c78a112ead4ae2edeb173be8f6698c1a","5cf1df3dcd47350bd401e3d9a74ac232",
            "5948bc733baaf44bf2dfb194164db54c","53cb7dfb98b6ad1623fbb53a560eaef2","e5f272edaa428939c9ae60abcd9406d9",
            "e90dfb84e30edf611e326eeb04d680de","76f7843c2bc545f9ef3a174e319ea4e2","416d977799a82898bb9bb50369eeeb6f",
            "0b214f8695e77e483d444cefbb91cc51","4182fb03a8bc2b45118e3774956f888d","61829f96db0c119084e326819e754cd1",
            "4570571597f088739f1265556613f6e0","710b49d1b3b2535d1fe8f05565f0ada7","b982d3bd69f08316fc096a06b037a2fd",
            "cc46f4690c5c0af118ebf5d750eeb544","4723c9500f1e5783619759601221c643","a9df475c99f9a2d06b712ea635d431ef",
            "de8b558ae0f8b91c718c84ae27e114ba","c1d53f32daaa2c0bc0ae1000f2be0834","be8afaf88654c8b682a0e490fd3400c2",
            "bfe6f5373eb9a6377ecf77ade2b46568","e75d28ef8f34e9eea99a4639aa4f570b","989f949849ace3a11bd4eeef35cd4bc7",
            "2a2542f9e61a9a1d3b83ae31889ac954","165eca536f27a447e7616424ba77f4de","3a659f0ddbbc3e453d9d3d638c1f8e2b",
            "d822352ddf17eb521a8583e9e1e6ea9f","b678d6b6cd959abf5dceb15e0082a388","b712916d8bfc1718a431c7b4fa280ae6",
            "46975e2587f7c3ba50717f8327fb2f1d","06ef4da00563c7616d09040f3b1be466","8957e6d74b61f0568422f2382e0bfd14",
            "1e2391485f9fb177b26c07bf7938ebd8","849858eaa9c847772b1da00e8f580f91","fe4c3cadb75731d29427b6eeb16c4339",
            "71a802cc08406a7470a2b6da0874155d","6df2f8a9b4b3bd65516b3bd423aafbd5","f2c1fdc8ac684e4d1e10a045a5da4b8a",
            "9d910c8bf395fce35216f0f4fa85432e","041d94b7259faca4e8facb1f79fec635","ed31037f65484480c5679fbea2b3ba62",
            "2bde79bff687ae45f1354cde4324ccdd","b78da085d4b9ee7cf0b5bf69ccf3a66d","88c7258d33a4093aa716a91ab1c96776",
            "06dc5066c94a0ab608d9ec79fa8abd99","4ddd2fdf3630818eefd15f544e356c60","2c93eb9092832fa98d27f04a650a0a22",
            "170bf81bdc4a8375ca82d23bb972dc14","570043f51ff10380390b369575f6b48e","0a7f1421e20ae7a35100b849d1dc8633",
            "43296a104b4ab392e3f2e3e9be66bb6c","ce8671b5681d78b5ed8728fb99c90b70","e184b6c5df5f8b3f3c06808072cc7386",
            "8bcf6c4fb36470b050b853278c00a3d3","88e8c1c91848a31ce9b811ce9fb56544","aec92010d80204800897c2275be8a874",
            "52d94e7dd5b2506c2cdaa55af7a892c1","ca503ac11482f3ec8d37c8e846f09d77","b87f119ff188afdc7d73b7bda2830fe8",
            "1e8f1f58f223f7c47f07ea9cce837188","604ead321b982250d1ec27090cfac725","87057dc90f659cbbbde2aebcf28e72fc",
            "6052be06053cab5bb8a843fb8a8848aa","b53deb8cd90fa6f09e71697ebdcfdfe2","516207824edb752de3e1c4282da4cd94",
            "c1c29ffbe6cc048f7beabe7abf08bf0b","20adc8da3d3b62bd2c2f534aa4165ba0","8621ffdbc5698829397d97767ac13db3",
            "01574404803ac4796041b647c11d3dfb","fb673e1fea55f1eb3d1e7fa64cea2055","bf1f5ece7d1dcbe5412192ac844749dc",
            "233175ec0b705d8b7a05c9bce0d85960","93d3bd8c6f37d61d0ff418d308204ef3","867c4235c7d5abbefd2b8abd92b57f8a",
            "ae5375df9d4b569f20a1a46270b282fb","baf8c970f80a0b8148536881fd469951","94d329f7523a40b1b46c7135c55d03ab",
            "3da94b2be3bfe2af590d980eb2f5530e","962012d09b8170d912f0669f6d7d9d07","6eae8378c629c4abf0ba4b2833a2d090",
            "3659006dac4bf1324f5da7b4da62e09d","f97de4a9986d216a6e0fea62b0450da9","166db3ce189da200e9d6c00f84f1d314",
            "d7270203d5e4f4c545bdb6319f089d8e","ee56f4fc196cf976d2562d758c86ba65","f4c3b280657711ba9c7c643bf485f19e",
            "2dde9b58295023512b83b5d952ece32a","e80cc21b30aa05848656a900f9773004","897d129f164607778cfe1d7faa868d30",
            "11c1d89ccc10b5e8fe1e4cbebf0065cc","c62e9b417787e53457bb4f012596f7b6",
            "1025e7bda98c6029b0c165834ff4bcea","0d81ee1dce715a7c50125d39addb726c",
            "fd429705f4aef3d2c92ffbf56ffe5699","fa4ff75fcb358364202258008b7703e7",
            "4785851a7b29ca4cf5f61c5c0104d3ad","50bae3461ad757177c07ffd3127ca9df",
            "7155b3c50e3d83eb6dcd4d890975955e","43d430f9c91c96c0af261ae92e733a6a",
            "e4dbef5d71f00d151d26e6077b3d798e","348de9092207931258d19822c67b3d59",
            "894f3c1ba146565bb607d110b513af69","76d16796b5fada9968ee2530ea43ddc3",
            "f63f5b741aec5b30f452c7edc1265146","b87716e7ea334920575a5d37ebca19e5",
            "84575b798ac34e99e0a6f129bfa85ff5","7c3b1d22ed969be2be2b6363cf09afa0",
            "d17357ba3041f7104414eb89527614b3","62c08c11984656eefe95d9b9296afce4",
            "fc96077be211ce692773099c053c66ef","4909876ba4e5d81c320e4ce3e6bb8cb8",
            "10c3833cbfdee09e85f4b853a5ffee28","1898ea6663f723fdaa429146321a883b",
            "55be51a80c6a10880f6644d7ab157f6d","d4afa5c63b684ebeabc176583c396182",
            "6e332d08443cb319b3d0793cfdef08ca","6fa9fdca5bbdb1efc9c9542babaaa3c2",
            "4072d7324f6315458081b5cbaca0e1e9","4c2ec1ab75d28ec152095750f794451d",
            "ef10b060f1caca7a2bffe9fa5b8a9484","b2f5ce26df15c9a7066d31a349074176",
            "73995fb90f0ebefe78b90731e67789cd","64489c85dc2fe0787b85cd87214b3810",
            "304246cb3520e1141afc75f283008c39","fb368d539e11a7941f8abeeb65f74b7c",
            "34f22884c4d3e544bfdfe6ca5d46098d","b4ffe84df9dada1391b7196bd2851c51",
            "d0c06a0df1ee1d97e869e447071af73b","db47ab79fb0c9f87e7491257892caf1b",
            "da3dfdc400ef0731b8100a5e37cb4a0d","f7aaa1b575cd36d43e811ab2045ed1ba",
            "01574404803ac4796041b647c11d3dfb","17420e4066d7991edf0af724fd643984",
            "3f61acbc2f7973c057bc283dfbbbd769","0393d37fbcd130e3ae0e29e6ff66fa07",
            "84bab8351ad8cc26c728e3c2cfc9a649","5d675f92221472a15ec2760007cf38c8",
            "233175ec0b705d8b7a05c9bce0d85960","26fcda1e0719629f5cdc99a07757d96f",
            "4fbf9586de6db9a96d21fc21e16cfbff","756322b68bfe27b9933a7f234d5329c8",
            "a6816c9ff4576cf87e5b44c6d00d3a90","0a72021644e07a7f0c0bdc1186a96864",
            "1f3241ebcf171eeeb11d844f606c1198","5d2a97252112fd32636c9d367a88f215",
            "d789a656ef17d0f0aedcd3633b0e2b6f","b44c1496d0045365f2a0dc3b7e1c9549",
            "8fc330d76e990ab6964af08ea1e47d2e","f9b6a775769cf7809ffe22cea0bdc3a2",
            "24b39dfd12a80cdf252e5042552203d9","a74853967ed2e2a4a0d5b0667953ffba",
            "21160b9db2f59f80ee428fbbbc46474f","8ebfa47f38c507cdda7c1ea888aa8d3c",
            "23038d501199cf119a34632da4511d76",
        };
        private static readonly List<string> name_temp = new List<string>()
        {
        };
        //经验效率
        private static string ExpLable = "";

		//
		private static string OPSLable = "";
		//胜率
		private static string PvpLable = "";
		private static string PvpRate = "";
		private static KeyCode StartKey = KeyCode.F9;


		private static QueueList<int> unlockMercID = new QueueList<int>();
		private static QueueList<int> unlockMapID = new QueueList<int>();

        
    }
}
