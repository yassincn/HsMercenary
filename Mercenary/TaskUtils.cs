﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Mercenary
{
    // Token: 0x0200000E RID: 14
    public static class TaskUtils
    {
        // Token: 0x06000059 RID: 89 RVA: 0x00005CDD File Offset: 0x00003EDD
        public static void UpdateTask()
        {
            TaskUtils.ClearTaskSpecialNode();
            TaskUtils.UpdateTaskInfo(HsGameUtils.GetTasks());
        }
        public static void UpdateMainLineTask()//5.7改动
        {
            TaskUtils.UpdateTaskInfo(HsGameUtils.GetMainLineTask());
        }
        // Token: 0x0600005A RID: 90 RVA: 0x00005CEC File Offset: 0x00003EEC
        public static long Current()
        {
            return new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
        }
        public static void ClearTaskSpecialNode()
        {
            HaveTaskDocter = false;
            HaveTaskTank = false;
            HaveTaskFighter = false;
            HaveTaskCaster = false;
        }
        // Token: 0x0600005B RID: 91 RVA: 0x00005D0C File Offset: 0x00003F0C
        private static void UpdateTaskInfo(List<Task> newTasks)
        {
            using (List<Task>.Enumerator enumerator = newTasks.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    Task task_new = enumerator.Current;
                    Task task_old = TaskUtils.tasks.Find((Task x) => x.Id == task_new.Id && x.ProgressMessage == task_new.ProgressMessage);
                    task_new.StartAt = ((task_old != null) ? task_old.StartAt : TaskUtils.Current());
                }
            }
            TaskUtils.tasks = newTasks;
            TaskUtils.dict_skillsTargetType = TaskUtils.GetTaskSkillAndTargetType(TaskUtils.tasks);
        }

        // Token: 0x0600005C RID: 92 RVA: 0x00005DA8 File Offset: 0x00003FA8
        public static int GetTaskMap()
        {
            List<Task> taskOrder = (from t in TaskUtils.tasks orderby t.Priority, t.water select t).ToList<Task>();
            foreach (Task task in taskOrder)
            {
                if (task.GetMapId() != -1)
                {
                    return task.GetMapId();
                }
            }
            return -1;
        }

        public static bool HasSkill(string skill)
        {
            return TaskUtils.dict_skillsTargetType.ContainsKey(skill);
        }


        public static HsMercenaryStrategy.TARGETTYPE FindSkillTargetType(string skill)
        {
            if (!TaskUtils.dict_skillsTargetType.ContainsKey(skill))
            {
                return HsMercenaryStrategy.TARGETTYPE.UNSPECIFIED;
            }
            else
            {
                return dict_skillsTargetType[skill].TargetType;
            }
        }

        public static int FindSkillSubskillIndex(string skill)
        {
            if (!TaskUtils.dict_skillsTargetType.ContainsKey(skill))
            {
                return -1;
            }
            else
            {
                return dict_skillsTargetType[skill].SubSkillIndex;
            }
        }

        // Token: 0x0600005E RID: 94 RVA: 0x00005E15 File Offset: 0x00004015
        public static List<Task> GetTasks()
        {
            List<Task> taskOrder = (from t in TaskUtils.tasks orderby t.Priority, t.water select t).ToList<Task>();
            return taskOrder;
        }

        // Token: 0x0600005F RID: 95 RVA: 0x00005E48 File Offset: 0x00004048
        private static List<MercenaryEntity> _GetTaskMercenaries(List<Task> allTasks)
        {
            List<MercenaryEntity> list = new List<MercenaryEntity>();
            foreach (Task task in allTasks)
            {   //5.7改动
                if (task.Mercenaries != null)
                    foreach (MercenaryEntity item in task.Mercenaries)
                    {
                        list.Add(item);
                    }
            }
            return list;
        }

        // Token: 0x06000060 RID: 96 RVA: 0x00005ED8 File Offset: 0x000040D8
        private static Dictionary<string, MercenaryEntity> GetTaskSkillAndTargetType(List<Task> allTasks)
        {
            Dictionary<string, MercenaryEntity> list = new Dictionary<string, MercenaryEntity>();
            foreach (MercenaryEntity mercenaryEntity in TaskUtils._GetTaskMercenaries(allTasks))
            {
                if (!string.IsNullOrEmpty(mercenaryEntity.Skill))
                {
                    if (!list.ContainsKey(mercenaryEntity.Skill))
                        list.Add(mercenaryEntity.Skill, mercenaryEntity);
                }
            }
            return list;
        }

        // Token: 0x0400003A RID: 58
        public static readonly Dictionary<string, int> CleanConf = new Dictionary<string, int>
        {
            {
                "不开启",
                -1
            },
            {
                "30分钟",
                1500
            },
            {
                "60分钟",
                3600
            },
            {
                "120分钟",
                7200
            },
            {
                "180分钟",
                10800
            }
        };
        //有对应赐福任务
        public static bool HaveTaskTank { get; set; }
        public static bool HaveTaskFighter { get; set; }
        public static bool HaveTaskCaster { get; set; }
        public static bool HaveTaskDocter { get; set; }
        // Token: 0x0400003B RID: 59
        private static List<Task> tasks = new List<Task>();

        // Token: 0x0400003C RID: 60
        private static Dictionary<string, MercenaryEntity> dict_skillsTargetType = new Dictionary<string, MercenaryEntity>();
    }
}
